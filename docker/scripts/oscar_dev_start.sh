#!/usr/bin/env bash

###############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

OSCAR_ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd -P )"
DEV_CONTAINER="apollo_dev_${USER}"

RED='\033[0;31m'
NC='\033[0m' # No Color

function show_help()
{
    cat <<EOF
Usage: bash docker/scripts/oscar_dev_start.sh [options] ...
OPTIONS:
    -h, --help    - Display this help and exit.
    -r, --rerun   - Remove current containers and run it again.
    stop          - Stop all running Apollo containers.
EOF
    exit 0
}


function stop_dev_containers() {

    local running_containers="$(docker ps -a --format '{{.Names}}')"

    for container in ${running_containers[*]}; do

        if [[ "${container}" =~ apollo_.*_${USER}$ ]]; then

            if docker stop "${container}" >/dev/null; then
                printf "${container} \033[32m[STOPPED]${NC}\n"
          else
                printf "\033[31m[FAILED TO STOP]${NC} ${container}\n"
            fi
        fi

    done
}


function pull_dev_containers() {

      printf "\n Please use default Apollo scripts.\n"

}


function run_dev_containers() {

    bash ${OSCAR_ROOT_DIR}/docker/scripts/dev_start.sh -f -l

    if [ $? -eq 0 ];then

      set -e
      printf "\n Install all OSCAR dependencies..\n\n"

      
      bash $OSCAR_ROOT_DIR/scripts/oscar/host/lucid/download_ArenaSDK.bash
      
      docker exec -it -u "${USER}" ${DEV_CONTAINER} bash -i '/apollo/docker/scripts/oscar_dev_setup.sh'

      # oscar_tools setup
      docker exec ${DEV_CONTAINER} sh -c 'echo "source /apollo/scripts/oscar/oscar_tools/setup.sh" >> /home/${USER}/.bashrc'

      printf "\n \033[32mTo login into the ${DEV_CONTAINER} container, please run the following command:${NC}"
      printf "\n\n    oscar docker enter"
      printf "\n\n OR"
      printf "\n\n    bash docker/scripts/dev_into.sh"
      printf "\n\n \033[32mEnjoy!${NC}\n\n"

    else
        return 1
    fi

}


function start_dev_containers() {

    local runned_apollo_containers
    any_runned_apollo_containers=true
    local runned_containers="$(docker ps -a --format '{{.Names}}')"

    # Get all apollo containers
    for container in ${runned_containers[*]}; do

        if [[ "${container}" =~ apollo_.*_${USER}$ ]]; then
            runned_apollo_containers="${runned_apollo_containers} ${container}"
        fi

    done

    # check if there are any
    if [[ -z "${runned_apollo_containers}" ]]; then
        any_runned_apollo_containers=false
        return 1
    fi

    for container in ${runned_apollo_containers[*]}; do

        docker start $container >/dev/null
        if [ $? -eq 0 ];then
            printf "${container} \033[32m[STARTED]${NC}\n"
        else
            printf "${container} \033[31m[FAILED TO START]${NC}\n"
            return 1
        fi

    done

    # setting network for Lucid Cameras
    bash $OSCAR_ROOT_DIR/scripts/oscar/host/lucid/network_setup.bash || echo -e "${RED}Failed to set network settings for Lucid Cameras ${NC}"

}

function main() {

    start_dev_containers

    if [ "$?" -eq 0 ]; then
      printf "\n\033[32mTo login into the newly created ${DEV_CONTAINER} container, please run the following command:${NC}"
      printf "\n    bash docker/scripts/dev_into.sh"
      printf "\n\033[32mEnjoy!${NC}\n\n"

    else
        if $any_runned_apollo_containers ; then
            printf "\n\033[31mFailed to start previously stopped Apollo containers. Trying to rerun..${NC}\n"
        fi
        run_dev_containers
    fi
}

while [ $# -gt 0 ]
do
    case "$1" in
    -r|--rerun)
        run_dev_containers
        exit 0
        ;;
    -h|--help)
        show_help
        exit 0
        ;;
    -p|--pull)
        pull_dev_containers
        exit 0
        ;;
    stop)
	    stop_dev_containers
	    exit 0
	    ;;
    *)
        echo -e "\033[93mWarning${NC}: Unknown option: $1"
        exit 2
        ;;
    esac
        shift
done

main
