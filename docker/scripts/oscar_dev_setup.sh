#!/usr/bin/env bash

#############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

set -e

BASHRC="/home/${USER}/.bashrc"

# change to US repositories as default Chinese ones are slow to download from them
sudo cp -f "/apollo/docker/build/rcfiles/sources.list.us.x86_64" /etc/apt/sources.list

# There is an invalid repo added in Apollo 8.0. In order to do apt update, we need to remove it from sources.list
sudo sed --in-place '/deb https:\/\/apollo-pkg-beta.bj.bcebos.com\/neo\/beta bionic main/d' /etc/apt/sources.list

sudo apt update
sudo apt install -y \
  qt5-default \
  mc \
  python3.8-dev \
  python3.8-venv \
  python3-tk \
  libeigen3-dev \
  libxt-dev libsm-dev \
  libfreetype6-dev \
  libdouble-conversion-dev \
  liblz4-dev \
  libblkid-dev \
  iputils-ping \
  lm-sensors \
  udev \
  libglew-dev \
  htop
#  mesa-utils \
#  dnsmasq \
#  alsa-base \
#  pulseaudio \
#  sox \
#  sudo \

# Adding env variables to ~/.bashrc --------------------------------------

echo 'export LC_ALL=C.UTF-8' >>${BASHRC}
echo 'export LANG=C.UTF-8' >>${BASHRC}

# Default Encoding to python IO.
# It is needed to show pretty Markdown files in Terminal
echo 'export COVERAGE_FILE=/tmp/last_pytest.coverage' >>${BASHRC}
echo 'export PYTHONIOENCODING=utf8' >>${BASHRC}

source ${BASHRC}

# Supervisor -------------------------------------------------------------

pip3 install tornado
pip3 install websockets

# HDMapCreator -------------------------------------------------------------

if [ -d mapcreator ]; then
  cd mapcreator && bash /apollo/mapcreator/mapcreator_install.sh
fi

# To connect to ALPHA with tty -------------------------------------------

sudo adduser ${USER} dialout

# records checking and visualization tools preparation --------------------------

echo 'export PATH="/home/$USER/.local/bin:$PATH"' >>${BASHRC}

echo 'export TRACY_PORT=15001' >>${BASHRC}

echo "Removing bazel dump files from build folders..."
for i in $(find /apollo -iname *.bazel_dump); do
  echo $i
  rm -f $i
done

# Check if this file is up-to-date when entering container -------------------------------------------

/apollo/scripts/oscar/container/hash_checker.bash update
echo '/apollo/scripts/oscar/container/hash_checker.bash check' >> ${BASHRC}
