load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")


def repo():
    http_archive(
        name="com_github_tracy",
        build_file="//third_party/tracy:tracy.BUILD",
        # sha256 = "a76017d928f3f2727540fb950edd3b736caa97b12dbb4e5edce66542cbea6600",
        # strip_prefix = "tracy-0.10",
        # urls = ["https://github.com/wolfpld/tracy/archive/refs/tags/v0.10.tar.gz"],
        sha256="93a91544e3d88f3bc4c405bad3dbc916ba951cdaadd5fcec1139af6fa56e6bfc",
        strip_prefix="tracy-0.9",
        urls=["https://github.com/wolfpld/tracy/archive/refs/tags/v0.9.tar.gz"],
    )
