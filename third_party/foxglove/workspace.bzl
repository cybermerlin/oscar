load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")


def repo():
    http_archive(
        name = "com_github_nlohmann_json_foxglove",
        build_file = "//third_party/foxglove:json.BUILD",
        sha256 = "d69f9deb6a75e2580465c6c4c5111b89c4dc2fa94e3a85fcd2ffcd9a143d9273",
        strip_prefix = "json-3.11.2",
        urls = ["https://github.com/nlohmann/json/archive/refs/tags/v3.11.2.tar.gz"],
    )

    http_archive(
        name = "websocketpp_repo",
        build_file = "//third_party/foxglove:websocketpp.BUILD",
        sha256 = "6ce889d85ecdc2d8fa07408d6787e7352510750daa66b5ad44aacb47bea76755",
        strip_prefix = "websocketpp-0.8.2",
        urls = ["https://github.com/zaphoyd/websocketpp/archive/refs/tags/0.8.2.tar.gz"]
    )

    http_archive(
        name = "foxglove_websocket_repo",
        build_file = "//third_party/foxglove:foxglove_websocket.BUILD",
        patches =["//third_party/foxglove:foxglove_websocket.patch"],
        patch_args = ["-p1"],
        sha256 = "c6106934a47d9d5b53ee3f6a0a6c8ec778ffdb52e9b462136bfc1bc5391ed227",
        urls = ["https://github.com/foxglove/ws-protocol/archive/refs/tags/releases/cpp/v1.0.0.tar.gz"],
        strip_prefix = "ws-protocol-releases-cpp-v1.0.0"
    )

    http_archive(
        name = "foxglove_schemas",
        build_file = "//third_party/foxglove:foxglove_schemas.BUILD",
        sha256 = "05432a456b721895e0edb335f13a1710ee982544203a6c0ca2e6ea85b7eb76b5",
        urls = ["https://github.com/foxglove/schemas/archive/refs/tags/ros-v2.2.0.tar.gz"],
        strip_prefix = "schemas-ros-v2.2.0/schemas/proto"
    )
