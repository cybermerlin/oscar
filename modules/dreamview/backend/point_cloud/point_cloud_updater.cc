/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/dreamview/backend/point_cloud/point_cloud_updater.h"

#include <utility>
#include <vector>

#include "nlohmann/json.hpp"
#include "pcl/filters/voxel_grid.h"
#include "yaml-cpp/yaml.h"

#include "modules/dreamview/proto/point_cloud.pb.h"

#include "cyber/common/file.h"
#include "cyber/common/log.h"
#include "cyber/time/clock.h"
#include "modules/common/adapters/adapter_gflags.h"
#include "modules/dreamview/backend/common/dreamview_gflags.h"
#include <pcl/filters/random_sample.h>


namespace apollo {
namespace dreamview {

using apollo::localization::LocalizationEstimate;
using Json = nlohmann::json;

float PointCloudUpdater::lidar_height_ = kDefaultLidarHeight;
boost::shared_mutex PointCloudUpdater::mutex_;

PointCloudUpdater::PointCloudUpdater(WebSocketHandler *websocket,
                                     SimulationWorldUpdater *simworld_updater)
    : node_(cyber::CreateNode("point_cloud")),
      websocket_(websocket),
      point_cloud_str_(""),
      future_ready_(true),
      simworld_updater_(simworld_updater) {
  RegisterMessageHandlers();
}

PointCloudUpdater::~PointCloudUpdater() { Stop(); }

void PointCloudUpdater::LoadLidarHeight(const std::string &file_path) {
  if (!cyber::common::PathExists(file_path)) {
    AWARN << "No such file: " << FLAGS_lidar_height_yaml
          << ". Using default lidar height:" << kDefaultLidarHeight;
    boost::unique_lock<boost::shared_mutex> writer_lock(mutex_);
    lidar_height_ = kDefaultLidarHeight;
    return;
  }

  YAML::Node config = YAML::LoadFile(file_path);
  if (config["vehicle"] && config["vehicle"]["parameters"]) {
    boost::unique_lock<boost::shared_mutex> writer_lock(mutex_);
    lidar_height_ = config["vehicle"]["parameters"]["height"].as<float>();
    AINFO << "Lidar height is updated to " << lidar_height_;
    return;
  }

  AWARN << "Fail to load the lidar height yaml file: "
        << FLAGS_lidar_height_yaml
        << ". Using default lidar height:" << kDefaultLidarHeight;
  boost::unique_lock<boost::shared_mutex> writer_lock(mutex_);
  lidar_height_ = kDefaultLidarHeight;
}

void PointCloudUpdater::RegisterMessageHandlers() {
  // Send current point_cloud status to the new client.
  websocket_->RegisterConnectionReadyHandler(
      [this](WebSocketHandler::Connection *conn) {
        Json response;
        response["type"] = "PointCloudStatus";
        response["enabled"] = enabled_;
        websocket_->SendData(conn, response.dump());
      });
  websocket_->RegisterMessageHandler(
      "RequestPointCloud",
      [this](const Json &json, WebSocketHandler::Connection *conn) {
        std::string to_send;
        // If there is no point_cloud data for more than 2 seconds, reset.
        if (point_cloud_str_ != "" &&
            std::fabs(last_localization_time_ - last_point_cloud_time_) > 2.0) {
          boost::unique_lock<boost::shared_mutex> writer_lock(mutex_);
          point_cloud_str_ = "";
        }
        {
          boost::shared_lock<boost::shared_mutex> reader_lock(mutex_);
          to_send = point_cloud_str_;
        }
        websocket_->SendBinaryData(conn, to_send, true);
      });
  websocket_->RegisterMessageHandler(
      "TogglePointCloud",
      [this](const Json &json, WebSocketHandler::Connection *conn) {
        auto enable = json.find("enable");
        if (enable != json.end() && enable->is_boolean()) {
          if (*enable) {
            enabled_ = true;
          } else {
            enabled_ = false;
          }
          if (websocket_) {
            Json response;
            response["type"] = "PointCloudStatus";
            response["enabled"] = enabled_;
            // Sync the point_cloud status across all the clients.
            websocket_->BroadcastData(response.dump());
          }
        }
      });
  websocket_->RegisterMessageHandler(
      "GetPointCloudChannel",
      [this](const Json &json, WebSocketHandler::Connection *conn) {
        std::vector<std::string> channels;
        GetChannelMsg(&channels);
        Json response({});
        response["data"]["name"] = "GetPointCloudChannelListSuccess";
        for (unsigned int i = 0; i < channels.size(); i++) {
          response["data"]["info"]["channel"][i] = channels[i];
        }
        websocket_->SendData(conn, response.dump());
      });
  websocket_->RegisterMessageHandler(
      "ChangePointCloudChannel",
      [this](const Json &json, WebSocketHandler::Connection *conn) {
        auto channel_info = json.find("data");
        Json response({});
        if (channel_info == json.end()) {
          AERROR << "Cannot  retrieve point cloud channel info with unknown "
                    "channel.";
          response["type"] = "ChangePointCloudChannelFail";
          websocket_->SendData(conn, response.dump());
          return;
        }
        std::string channel =
            channel_info->dump().substr(1, channel_info->dump().length() - 2);
        if (ChangeChannel(channel)) {
          Json response({});
          response["type"] = "ChangePointCloudChannelSuccess";
          websocket_->SendData(conn, response.dump());
        } else {
          response["type"] = "ChangePointCloudChannelFail";
          websocket_->SendData(conn, response.dump());
        }
      });
}

void PointCloudUpdater::GetChannelMsg(std::vector<std::string> *channels) {
  enabled_ = true;
  auto channelManager =
      apollo::cyber::service_discovery::TopologyManager::Instance()
          ->channel_manager();
  std::vector<apollo::cyber::proto::RoleAttributes> role_attr_vec;
  channelManager->GetWriters(&role_attr_vec);
  for (auto &role_attr : role_attr_vec) {
    std::string messageType;
    messageType = role_attr.message_type();
    int index = messageType.rfind("PointCloud");
    int index_sensor = role_attr.channel_name().rfind("sensor");
    if (index != -1 && index_sensor != -1) {
      channels->push_back(role_attr.channel_name());
    }
  }
  channels_.clear();
  channels_ = {channels->begin(), channels->end()};
}

bool PointCloudUpdater::ChangeChannel(const std::string &channel) {
  if (curr_channel_name != "") {
    if (!node_->DeleteReader(curr_channel_name)) {
      AERROR << "delete reader failed";
      return false;
    }
  }
  point_cloud_reader_.reset();
  point_cloud_reader_ = node_->CreateReader<drivers::PointCloud>(
      channel, [this](const std::shared_ptr<drivers::PointCloud> &msg) {
        UpdatePointCloud(msg);
      });

  if (point_cloud_reader_ == nullptr) {
    return false;
  }
  curr_channel_name = channel;
  bool update_res = false;
  update_res = callback_api_(channel);
  if (!update_res) {
    AERROR << "update current point cloud channel fail";
    return false;
  }
  return true;
}

void PointCloudUpdater::Start(DvCallback callback_api) {
  callback_api_ = callback_api;
  localization_reader_ = node_->CreateReader<LocalizationEstimate>(
      FLAGS_localization_topic,
      [this](const std::shared_ptr<LocalizationEstimate> &msg) {
        UpdateLocalizationTime(msg);
      });
  LoadLidarHeight(FLAGS_lidar_height_yaml);
}

void PointCloudUpdater::Stop() {
  if (enabled_) {
    async_future_.wait();
  }
}

pcl::PointCloud<pcl::PointXYZ>::Ptr PointCloudUpdater::ConvertPCLPointCloud(
    const std::shared_ptr<drivers::PointCloud> &point_cloud) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_ptr(
      new pcl::PointCloud<pcl::PointXYZ>);
      // std::make_shared<pcl::PointCloud<pcl::PointXYZ>>());

  pcl_ptr->width = point_cloud->width();
  pcl_ptr->height = point_cloud->height();
  pcl_ptr->is_dense = false;

  if (point_cloud->width() * point_cloud->height() !=
      static_cast<unsigned int>(point_cloud->point_size())) {
    pcl_ptr->width = 1;
    pcl_ptr->height = point_cloud->point_size();
  }
  pcl_ptr->points.resize(point_cloud->point_size());

  for (size_t i = 0; i < pcl_ptr->points.size(); ++i) {
    const auto &point = point_cloud->point(static_cast<int>(i));
    pcl_ptr->points[i].x = point.x();
    pcl_ptr->points[i].y = point.y();
    pcl_ptr->points[i].z = point.z();
  }
  return pcl_ptr;
}

void PointCloudUpdater::UpdatePointCloud(
    const std::shared_ptr<drivers::PointCloud> &point_cloud) {

  if (!enabled_) {
    transform_cached = false; // reset the flag to load actual transformation after next turning on visualziation
    return;
  }

  auto point_cloud_transformed = std::make_shared<drivers::PointCloud>();

  // find transformation to pointcloud from localization frame

  if(!transform_cached) {

    cyber::Time query_time = cyber::Time(point_cloud->header().timestamp_sec() * 1e9);
    try {
      apollo::transform::TransformStamped transf = tf2_buffer_ptr_->lookupTransform(
            "localization", point_cloud->header().frame_id(), query_time);
      transformation = transf.transform();

    } catch (tf2::TransformException& ex) {
      AERROR << ex.what();
      return;

    }

    transform_cached = true;

    Eigen::Vector3d translation(transformation.translation().x(), transformation.translation().y(), transformation.translation().z());
    Eigen::Quaterniond quat(transformation.rotation().qw(), transformation.rotation().qx(), transformation.rotation().qy(), transformation.rotation().qz());

    transform_matrix = Eigen::Affine3d::Identity();
    transform_matrix.translation() = translation;
    transform_matrix.linear() = quat.toRotationMatrix();

    // Workaround. Dreamview rotates point cloud by 90 degrees over Z axis (X - forward, Y - left). 
    // TODO: Fix this in frontend transformations.
    Eigen::Quaterniond dv_rot(sqrt(2) / 2, 0, 0, -sqrt(2) / 2);
    Eigen::Affine3d dv_transform_matrix = Eigen::Affine3d::Identity();
    dv_transform_matrix.linear() = dv_rot.toRotationMatrix();
    transform_matrix = dv_transform_matrix * transform_matrix;
  }

  TransformPointCloud(point_cloud, point_cloud_transformed, transform_matrix);

  last_point_cloud_time_ = last_localization_time_;

    if (last_localization_time_ == 0.0 ||
      last_localization_time_ - last_point_cloud_time_ > 0.1) 
      {
    AWARN << "skipping outdated point cloud data, got PC time:" << last_point_cloud_time_ << ", but last localization time is " << last_localization_time_;
    return;
  }
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_ptr;

  // Check if last filter process has finished before processing new data.
  if (enable_voxel_filter_) {
    if (future_ready_) {
      future_ready_ = false;
      // transform from drivers::PointCloud to pcl::PointCloud
      pcl_ptr = ConvertPCLPointCloud(point_cloud_transformed);
      std::future<void> f =
          cyber::Async(&PointCloudUpdater::FilterPointCloud, this, pcl_ptr);
      async_future_ = std::move(f);
    }
  } else {
    pcl_ptr = ConvertPCLPointCloud(point_cloud_transformed);
    this->FilterPointCloud(pcl_ptr);
  }
}

void PointCloudUpdater::FilterPointCloud(
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_ptr) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_filtered_ptr(new pcl::PointCloud<pcl::PointXYZ>);

  // auto pcl_filtered_ptr = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();

  /*
      By default, disable voxel filter since it's taking more than 500ms
      ideally the most efficient sampling method is to
      use per beam random sample for organized cloud(TODO)
  */

  auto begin = std::chrono::steady_clock::now();

  // pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_ptr(new pcl::PointCloud<pcl::PointXYZ>);

  if (enable_voxel_filter_) {
    pcl::VoxelGrid<pcl::PointXYZ> voxel_grid;
    voxel_grid.setInputCloud(pcl_ptr);
    voxel_grid.setLeafSize(static_cast<float>(FLAGS_voxel_filter_size),
                           static_cast<float>(FLAGS_voxel_filter_size),
                           static_cast<float>(FLAGS_voxel_filter_height));
    voxel_grid.filter(*pcl_filtered_ptr);
    // pcl_filtered_ptr = pcl_ptr;

    // tmp_ptr.reset();


  } else if (enable_random_sampling_) {
    pcl::RandomSample<pcl::PointXYZ> random_sampling;
    random_sampling.setInputCloud(pcl_ptr);
    random_sampling.setSample(5e4);
    random_sampling.filter(*pcl_filtered_ptr);
  }
  else {
    pcl_filtered_ptr = pcl_ptr;
  }

  auto end = std::chrono::steady_clock::now();

  AWARN << "Filtering points: " << pcl_ptr->size() << " -> " << pcl_filtered_ptr->size() << 
      " took " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " milliseconds"<< std::endl;
  // float z_offset;
  {
    boost::shared_lock<boost::shared_mutex> reader_lock(mutex_);
    // z_offset = lidar_height_;
  }
  apollo::dreamview::PointCloud point_cloud_pb;
  for (size_t idx = 0; idx < pcl_filtered_ptr->size(); ++idx) {
    pcl::PointXYZ &pt = pcl_filtered_ptr->points[idx];
    // std::cout << idx << " " << pcl_filtered_ptr->size() << pcl_filtered_ptr->points[idx] << std::endl;
    if (!std::isnan(pt.x) && !std::isnan(pt.y) && !std::isnan(pt.z)) {
      point_cloud_pb.add_num(pt.x);
      point_cloud_pb.add_num(pt.y);
      point_cloud_pb.add_num(pt.z);
    }
  }
  std::cout << "End" << std::endl;
  {
    boost::unique_lock<boost::shared_mutex> writer_lock(mutex_);
    point_cloud_pb.SerializeToString(&point_cloud_str_);
    future_ready_ = true;
  }
  
  std::cout << "Sent" << std::endl;

}

void PointCloudUpdater::UpdateLocalizationTime(
    const std::shared_ptr<LocalizationEstimate> &localization) {
  last_localization_time_ = localization->header().timestamp_sec();
}

void PointCloudUpdater::TransformPointCloud(
    const std::shared_ptr<const drivers::PointCloud>& msg,
    std::shared_ptr<drivers::PointCloud> msg_transformed, 
    const Eigen::Affine3d& transformation) {

  for (const auto& point: msg->point()){
    
    Eigen::Vector3d p(point.x(), point.y(), point.z());
    p = transformation * p;

    auto* point_new = msg_transformed->add_point();
    point_new->set_intensity(point.intensity());
    point_new->set_timestamp(point.timestamp());
    point_new->set_x(static_cast<float>(p.x()));
    point_new->set_y(static_cast<float>(p.y()));
    point_new->set_z(static_cast<float>(p.z()));
  }
}

}  // namespace dreamview
}  // namespace apollo
