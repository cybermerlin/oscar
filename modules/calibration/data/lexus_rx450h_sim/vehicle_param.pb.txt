vehicle_param {
  front_edge_to_center: 3.770
  back_edge_to_center: 1.000
  left_edge_to_center: 0.9425
  right_edge_to_center: 0.9425

  length: 4.770
  width: 1.885
  height: 1.700
  min_turn_radius: 5.8
  max_acceleration: 3.56
  max_deceleration: -4.0
  max_steer_angle: 8.73
  max_steer_angle_rate: 10.0
  steer_ratio: 14.8
  wheel_base: 2.740
  wheel_rolling_radius: 0.372
  max_abs_speed_when_stopped: 0.2
  brake_deadzone: 0.0
  throttle_deadzone: 0.0
}
