///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#include "modules/oscar/foxglove/record_converter/data_converter.h"

#include <condition_variable>
#include <future>
#include <map>
#include <memory>
#include <queue>
#include <thread>
#include <utility>
#include <vector>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

#include "cyber/base/thread_pool.h"
#include "modules/oscar/foxglove/record_converter/utils/conversion_result.h"
#include "modules/oscar/foxglove/utils/types_conversion.h"

#define MAX_QUEUE_SIZE 1e4

namespace apollo {
using apollo::cyber::record::RecordMessage;

Converter::Converter(const std::string& src_path, const std::string& dest_path,
                     const std::string& prefix,
                     const std::string& compression_method,
                     const bool compress_images, const uint32_t chunk_size,
                     uint32_t max_file_size_mb)
    : reader_manager_(src_path),
      writer_manager_(dest_path, prefix, compression_method, chunk_size,
                      max_file_size_mb),
      compress_images_(compress_images) {
  writer_manager_.set_channel2type_map(reader_manager_.get_channel2type_map());
  writer_manager_.update_writer();
}

RecordMessage convertMessageWrapper(const RecordMessage& in_msg,
                                    const std::string& type_name,
                                    const bool compress_images) {
  RecordMessage out_msg(in_msg);
  convertCyberMessageToFoxGlove(in_msg, type_name, out_msg, compress_images);
  return out_msg;
}

void Converter::run() {
  std::queue<ConversionResult> read_msgs_queue;

  auto total_pbar = reader_manager_.get_total_messages();
  uint32_t msg_counter = 0;
  std::mutex queue_lock;

  bool max_queue_size_overflow = false;
  std::mutex queue_max_size_lock;
  std::condition_variable cv_queue_max_size;

  apollo::cyber::base::ThreadPool thread_pool(std::thread::hardware_concurrency());

  auto read_thread = std::make_shared<std::thread>([&] {
    RecordMessage apollo_msg;

    while (this->reader_manager_.get_message(&apollo_msg)) {
      if (max_queue_size_overflow) {
        std::unique_lock<std::mutex> queue_size_locker(queue_max_size_lock);
        cv_queue_max_size.wait(queue_size_locker,
                               [&] { return !max_queue_size_overflow; });
      }
      auto type_name =
          this->reader_manager_.get_type_name(apollo_msg.channel_name);

      std::unique_lock<std::mutex> locker(queue_lock);
      if (convertTypeName(type_name).compare(type_name) == 0) {
        read_msgs_queue.push(apollo_msg);
      } else {
        auto ft = thread_pool.Enqueue(&convertMessageWrapper, apollo_msg,
                                     type_name, this->compress_images_);
        read_msgs_queue.push(std::move(ft));
      }
    }
  });

  while (true) {
    size_t queue_size;
    {
      std::unique_lock<std::mutex> lg(queue_lock);
      queue_size = read_msgs_queue.size();
    }
    if (queue_size == 0 && reader_manager_.is_finished()) break;

    if (queue_size == 0) {
      usleep(1e5);
      continue;
    }

    if (queue_size > MAX_QUEUE_SIZE) {
      max_queue_size_overflow = true;
    } else {
      std::unique_lock<std::mutex> queue_size_locker(queue_max_size_lock);
      max_queue_size_overflow = false;
      cv_queue_max_size.notify_one();
    }

    std::unique_ptr<ConversionResult> res;
    {
      std::unique_lock<std::mutex> locker(queue_lock);
      res = std::make_unique<ConversionResult>(
          std::move(read_msgs_queue.front()));
      read_msgs_queue.pop();
    }
    writer_manager_.write_message(unpack_result(*res));

    pbar_.progress(++msg_counter, total_pbar);
  }

  pbar_.finish();
  read_thread->join();

  std::cout << "Readed messages:    " << reader_manager_.get_readed_messages()
            << "/" << reader_manager_.get_total_messages()
            << "\nConverted messages: " << writer_manager_.get_message_number()
            << "/" << reader_manager_.get_total_convert_messages() << "\n";
}

}  // namespace apollo
