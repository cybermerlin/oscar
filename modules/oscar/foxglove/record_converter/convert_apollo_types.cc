///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#include <string>

#include <boost/format.hpp>

#include "modules/oscar/foxglove/record_converter/mcap_types_converter.h"

namespace {
const char help_str[] =
    "usage: convert_types <in_file> <out_file>\n\n"
    "in_file - str - directory with .record files;\n"
    "out_file - str - directory to save .mcap files.\n";

struct CLIArgs {
  std::string in_file;
  std::string out_file;

  CLIArgs(int argc, char** argv) {
    for (int i = 1; i < argc; i++) {
      auto a = std::string(argv[i]);
      if (a.compare("-h") == 0 || a.compare("--help") == 0) {
        std::cout << help_str;
        exit(1);
      }
    }
    
    if (argc != 3) throw std::runtime_error(boost::str(boost::format(R"(N arguments = %i)") % argc));

    in_file = argv[1];
    out_file = argv[2];

  }

  std::string show() const {
    return boost::str(boost::format(R"(in_file: %s out_file: %s )") %
                      in_file % out_file);
  }
};
}  // namespace

int main(int argc, char** argv) {
  CLIArgs args(argc, argv);
  std::cout << args.show() << "\n";

  apollo::McapTypesConverter converter(args.in_file, args.out_file);

  converter.run();
  return 0;
}
