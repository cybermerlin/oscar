///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/record_converter/record_reader_manager.h"

#include <limits.h>
#include <unistd.h>

#include <fstream>
#include <iomanip>
#include <variant>

#include <experimental/filesystem>

#include "cyber/common/file.h"
#include "cyber/record/record_reader.h"
#include "modules/oscar/foxglove/utils/utils.h"

namespace fs = std::experimental::filesystem;

namespace {
uint32_t get_total_reader_messages(const RecordReader& reader) {
  uint32_t count = 0;

  for (auto& ch : reader.GetChannelList()) {
    count += reader.GetMessageNumber(ch);
  }

  return count;
}
}  // namespace

namespace apollo {

RecordReaderManager::RecordReaderManager(const std::string& src_path) {
  std::ifstream restricted_types_fstream(restricted_list_types_file_);
  ACHECK(restricted_types_fstream.is_open());

  std::string cur_line;
  while (getline(restricted_types_fstream, cur_line)) {
    if (cur_line[0] == '#') continue;
    restricted_types_.push_back(cur_line);
  }

  finish_.store(false);

  const std::string current_path = apollo::cyber::common::GetCurrentPath();
  const std::string abs_path =
      apollo::cyber::common::GetAbsolutePath(current_path, src_path);

  AINFO << "Reading from directory: " << abs_path;

  std::map<std::string, uint32_t> restricted_types_messages;
  std::map<std::string, uint32_t> permitted_types_messages;

  std::vector<std::string> files;

  if (fs::is_directory(abs_path)) {
    for (auto& f : fs::directory_iterator(abs_path)) files.push_back(f.path());
  } else
    files.push_back(abs_path);

  std::sort(files.begin(), files.end());

  for (const auto& f : files) {
    in_files_.push(f);

    RecordReader reader(f);

    total_messages_ += get_total_reader_messages(reader);

    for (auto& ch : reader.GetChannelList()) {
      auto message_type = reader.GetMessageType(ch);

      bool check_restricted =
          std::find(restricted_types_.begin(), restricted_types_.end(),
                    message_type) != restricted_types_.end();

      if (check_restricted) {
        restricted_types_messages.try_emplace(message_type, 0);
        restricted_types_messages[message_type] += reader.GetMessageNumber(ch);
      } else {
        channel2type_map_[ch] = message_type;
        permitted_types_messages.try_emplace(message_type, 0);
        permitted_types_messages[message_type] += reader.GetMessageNumber(ch);
      }
    }
  }

  for (auto& [t, msg_count] : restricted_types_messages) {
    std::cout << "Restricted type: " << t << std::setw(50 - t.size())
              << " with "
              << std::string(10 - std::to_string(msg_count).size(), ' ')
              << msg_count << " messages\n";
    total_drop_messages_ += msg_count;
  }
  std::cout << "\nTotal messages to drop: " << total_drop_messages_ << "\n";

  if (restricted_types_messages.size() > 0)
    std::cout << "\nTo include restricted types add them in file "
              << restricted_list_types_file_ << "\n\n";

  for (auto& [t, msg_count] : permitted_types_messages) {
    std::cout << "Converting type: " << t << std::setw(50 - t.size())
              << " with "
              << std::string(10 - std::to_string(msg_count).size(), ' ')
              << msg_count << " messages\n";
    total_convert_messages_ += msg_count;
  }

  std::cout << "\nTotal messages to convert: " << total_convert_messages_
            << "\n";

  update_reader();
}

bool RecordReaderManager::update_reader() {
  if (in_files_.size() == 0) {
    finish_.store(true);
    return false;
  }

  std::string cur_f{in_files_.front()};
  in_files_.pop();
  reader_.reset(new RecordReader(cur_f));
  if (!reader_->IsValid()) {
    AERROR << "Reader is invalid. Skipping file: " << cur_f;
    return update_reader();
  }

  return true;
}

bool RecordReaderManager::get_message(
    apollo::cyber::record::RecordMessage* cur_msg) {
  if (reader_->ReadMessage(cur_msg) || update_reader() ||
      reader_->ReadMessage(cur_msg)) {
    current_reader_message_counter_++;
    return true;
  } else {
    finish_.store(true);
    return false;
  }
}
}  // namespace apollo
