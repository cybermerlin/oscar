///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <iostream>
#include <string>
#define MCAP_IMPLEMENTATION
#include <map>
#include <queue>

#include <mcap/writer.hpp>

#include "cyber/record/record_message.h"

using apollo::cyber::record::RecordMessage;

namespace apollo {
class McapWriterManager final {
 public:
  McapWriterManager(const std::string& dest_path,
                    const std::string& file_prefix = "",
                    const std::string& compression = "lz4",
                    const uint32_t chunksize = mcap::DefaultChunkSize,
                    uint32_t max_file_size_MB = 20 * (1 << 10));

  ~McapWriterManager();

  void set_channel2type_map(
      const std::map<std::string, std::string>& channel2type_map);

  void write_message(const RecordMessage& in_msg);
  void update_writer();

  uint32_t get_message_number() { return message_counter_; }

 private:
  void add_schemas_to_writer();

  mcap::McapWriter writer_;
  mcap::McapWriterOptions writer_opts_;

  std::map<std::string, uint32_t> channel2Id_;
  std::map<std::string, std::string> channel2type_map_;
  std::queue<mcap::Message> messages_queue_;

  bool save_in_one_file_;

  std::string dest_path_;
  std::string file_prefix_;

  uint32_t mcap_file_counter_ = 0;
  uint32_t message_counter_ = 0;
  std::string curr_out_filename_;

  double max_file_size_mb_;
};

}  // namespace apollo
