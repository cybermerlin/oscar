///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#include <string>

#include <boost/format.hpp>
#include <experimental/filesystem>

#include "modules/oscar/foxglove/record_converter/data_converter.h"

#define DEFAULT_OUT_DIR "/apollo/data/bag/mcaps"
#define DEFAULT_PREFIX ""
#define DEFAULT_COMPRESSION "lz4"
#define DEFAULT_CHUNK_SIZE 786432UL
#define DEFAULT_MAX_SIZE 20 * (1 << 10)

namespace fs = std::experimental::filesystem;

namespace {
const char help_str[] =
    "This script converts .record files to .mcap files to visualize in "
    "FoxGlove Studio.\n\n"
    "usage: convert_record <src_path> <dest_path> [--prefix PREFIX] "
    "[--compression COMRPESSION] [--max_size MAX_SIZE] [--chunk_size "
    "CHUNK_size]"
    "[--compress_images COMPRESS_IMAGE]\n\n"
    "src_path - str - file or directory with .record files;\n"
    "dest_path - str - file or directory to save .mcap files;\n"
    "--prefix - str - prefix to each output file;\n"
    "--compression - str - compression type [lz4]/zstd;\n"
    "--max_size - int - after exceeding this limit writing will "
    "continue to another file (in MBs);\n"
    "--chunk_size - int - size in bytes for eachc chunk in output files;\n"
    "--compress_images - bool - whether to perform jpeg compression "
    "[true]/false. Significantly speeds up writing.\n";

struct CLIArgs {
  std::string src_path;
  std::string dest_path = DEFAULT_OUT_DIR;
  std::string prefix = DEFAULT_PREFIX;
  std::string compression = DEFAULT_COMPRESSION;
  uint32_t max_file_size = DEFAULT_MAX_SIZE;
  uint32_t chunk_size = DEFAULT_CHUNK_SIZE;
  bool compress_images = true;

  CLIArgs(int argc, char** argv) {
    for (int i = 1; i < argc; i++) {
      auto a = std::string(argv[i]);
      if (a.compare("-h") == 0 || a.compare("--help") == 0) {
        std::cout << help_str;
        exit(1);
      }
    }

    ACHECK(argc > 1);

    src_path = argv[1];
    if ('/' == src_path.back()) src_path.pop_back();

    dest_path = argc <= 2 ? DEFAULT_OUT_DIR : argv[2];

    prefix = fs::path(src_path).filename();

    if (argc <= 3) {
      return;
    }

    std::vector<std::string> args;
    for (int i = 3; i < argc; i++) {
      args.push_back(argv[i]);
    }

    for (auto first_it = args.begin(), second_it = args.begin() + 1,
              end_it = args.end();
         second_it < end_it; first_it++, second_it++) {
      if (first_it->compare("--prefix") == 0) {
        prefix = *second_it;
      } else if (first_it->compare("--compression") == 0) {
        compression = *second_it;
      } else if (first_it->compare("--max_size") == 0) {
        max_file_size = std::stoi(*second_it);
      } else if (first_it->compare("--chunk_size") == 0) {
        chunk_size = std::stoi(*second_it);
      } else if (first_it->compare("--compress_images") == 0) {
        compress_images = *second_it == "true";
      }
    }
  }

  std::string show() const {
    return boost::str(boost::format(R"(src_path: %s
        dest_path: %s
        --prefix: %s
        --compression: %s
        --max_size: %d
        --chunk_size: %d
        --compress_images: %b )") %
                      src_path % dest_path % prefix % compression %
                      max_file_size % chunk_size % compress_images);
  }
};
}  // namespace

int main(int argc, char** argv) {
  CLIArgs args(argc, argv);
  AINFO << args.show();

  apollo::Converter converter(args.src_path, args.dest_path, args.prefix,
                              args.compression, args.compress_images,
                              args.chunk_size, args.max_file_size);

  converter.run();
  return 0;
}
