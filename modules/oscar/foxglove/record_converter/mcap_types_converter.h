///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <iostream>
#include <map>
#include <queue>
#include <string>

#define MCAP_IMPLEMENTATION
#include <mcap/reader.hpp>
#include <mcap/writer.hpp>

#include "modules/oscar/foxglove/record_converter/utils/tqdm.h"
#include "modules/oscar/foxglove/utils/types_conversion.h"

namespace apollo {
class McapTypesConverter final {
 public:
  McapTypesConverter(const std::string& input_file,
                     const std::string& output_file)
      : writer_opts_("protobuf"),
        input_file_(input_file),
        output_file_(output_file) {}

  void run() {
    const auto res = reader_.open(input_file_);
    if (!res.ok()) {
      std::cerr << "Failed to open " << input_file_
                << " for reading: " << res.message << std::endl;
      return;
    };

    auto status = writer_.open(output_file_, writer_opts_);
    if (status.code != mcap::StatusCode::Success) {
      std::cerr << status.message;
    }

    add_channels();

    auto total_messages = reader_.statistics()->messageCount;

    int msg_counter = 0;

    for (auto& msg : reader_.readMessages()) {
      std::string type = msg.schema->name;

      std::string in_data(msg.message.dataSize, '\0');
      memcpy(in_data.data(), msg.message.data, msg.message.dataSize);

      std::string out_data;
      message_transformers_map_.at(msg.channel->topic)
          .transform(in_data, out_data);

      mcap::Message out_msg;

      out_msg.channelId = channel2Id_[msg.channel->topic];
      out_msg.logTime = msg.message.logTime;
      out_msg.publishTime = msg.message.publishTime;
      out_msg.dataSize = out_data.size();
      out_msg.data = reinterpret_cast<const std::byte*>(out_data.data());

      auto status = writer_.write(out_msg);
      if (status.code != mcap::StatusCode::Success) std::cerr << status.message;
      pbar_.progress(++msg_counter, total_messages);
    }
    std::cout << "\n";
  }

  ~McapTypesConverter() {
    writer_.close();
    reader_.close();
  }

 private:
  void add_channels() {
    auto status = reader_.readSummary(mcap::ReadSummaryMethod::ForceScan);
    if (status.code != mcap::StatusCode::Success)
      throw std::runtime_error(status.message);

    for (const auto& it : reader_.channels()) {
      auto channel_name = it.second->topic;

      auto schema_name = reader_.schema(it.second->schemaId)->name;
      auto new_schema_name = convertTypeName(schema_name);

      auto conversion_type = get_conversion_type_by_channel(schema_name);

      message_transformers_map_.emplace(channel_name,
                                        MessageTransformer{conversion_type});

      mcap::Schema schema;

      if (new_schema_name.compare(schema_name) == 0) {
        schema = *reader_.schema(it.second->schemaId);
      } else {
        auto desc = message_transformers_map_.at(channel_name)
                        .get_out_descriptor(new_schema_name);

        if (desc == nullptr) {
          std::cerr << "Skipping channel " << channel_name << " of type "
                    << new_schema_name;
          continue;
        }

        schema = {new_schema_name, "protobuf",
                  BuildFileDescriptorSet(desc).SerializeAsString()};
        schema.id = it.second->schemaId;
      }

      writer_.addSchema(schema);
      mcap::Channel channel(channel_name, "protobuf", schema.id);
      writer_.addChannel(channel);
      channel2Id_[channel.topic] = channel.id;
    }
  }

  tqdm pbar_;

  mcap::McapWriter writer_;
  mcap::McapWriterOptions writer_opts_;

  mcap::McapReader reader_;

  std::map<std::string, uint32_t> channel2Id_;

  std::map<std::string, MessageTransformer> message_transformers_map_;

  std::string input_file_;
  std::string output_file_;
  std::string file_prefix_;

  int num_messages_;
};

}  // namespace apollo
