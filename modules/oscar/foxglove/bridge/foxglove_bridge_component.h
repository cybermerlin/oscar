///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <unistd.h>

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "modules/oscar/foxglove/bridge/proto/foxglove_bridge_config.pb.h"

#include "cyber/component/component.h"
#include "cyber/cyber.h"
#include "modules/oscar/foxglove/bridge/foxglove_bridge.h"

namespace apollo {

using apollo::cyber::Component;
using apollo::oscar::ParameterManager;

class FoxGloveBridgeComponent final : public cyber::Component<> {
 public:
  bool Init() override;
  bool Proc() { return true; }

 private:
  bool loadConfig();

  std::unique_ptr<apollo::oscar::FoxGloveBridge> bridge_ = nullptr;
  std::shared_ptr<apollo::oscar::FoxGloveBridgeConfig> config_ = nullptr;
};

CYBER_REGISTER_COMPONENT(FoxGloveBridgeComponent)

}  // namespace apollo
