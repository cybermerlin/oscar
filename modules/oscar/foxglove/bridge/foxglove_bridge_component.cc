///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/bridge/foxglove_bridge_component.h"

namespace apollo {

bool FoxGloveBridgeComponent::Init() {
  if (!loadConfig()) return false;

  bridge_ = std::make_unique<apollo::oscar::FoxGloveBridge>(*node_, config_);

  return true;
}

bool FoxGloveBridgeComponent::loadConfig() {
  config_ = std::make_shared<apollo::oscar::FoxGloveBridgeConfig>();
  if (!apollo::cyber::common::GetProtoFromFile(config_file_path_,
                                               config_.get())) {
    return false;
  }

  AERROR << "FoxGlove bridge config: " << config_->DebugString();
  return true;
}

}  // namespace apollo
