///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <foxglove/websocket/websocket_notls.hpp>
#include <foxglove/websocket/websocket_server.hpp>
#include <memory>
#include <string>

#include "cyber/cyber.h"
#include "modules/oscar/foxglove/schemas/CompressedImage.pb.h"
#include "modules/oscar/foxglove/schemas/FrameTransforms.pb.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"
#include "modules/oscar/foxglove/schemas/PointCloud.pb.h"
#include "modules/oscar/foxglove/schemas/RawImage.pb.h"
#include "modules/oscar/foxglove/utils/types_conversion.h"
#include "modules/oscar/foxglove/utils/utils.h"

using apollo::cyber::message::RawMessage;

class ChannelHandler {
 public:
  ChannelHandler() = delete;
  ChannelHandler(foxglove::Server<foxglove::WebSocketNoTls>& server,
                 apollo::cyber::Node& node, const std::string& channel_name,
                 const std::string& channel_type,
                 Conversion conversion_type = Conversion::NO_CONVERSION);

  ~ChannelHandler() { node_.DeleteReader(channel_name_); }

  [[nodiscard]] uint32_t get_channel_id() const noexcept { return channel_id_; }
  [[nodiscard]] const std::string& get_channel_name() const noexcept {
    return channel_name_;
  }

  void disable() {
    AINFO << "Disabling reader " << channel_name_;
    if (reader_ != nullptr && reader_->IsInit()) reader_->Shutdown();
  }

  void enable() {
    AINFO << "Enabling reader " << channel_name_;
    if (reader_ != nullptr && !reader_->IsInit()) reader_->Init();
    if (out_msg_.size() != 0) send_message();
  }

 private:
  void waitFirstMessage();
  void send_message();
  void callback(const std::shared_ptr<RawMessage>& msg);

  foxglove::Server<foxglove::WebSocketNoTls>& server_;
  apollo::cyber::Node& node_;

  std::shared_ptr<apollo::cyber::Reader<RawMessage>> reader_ = nullptr;
  const std::string channel_name_;
  uint32_t channel_id_;

  const MessageTransformer message_transformer_;

  std::string out_msg_ = "";

  double max_wait_time_ = 0.5;
};
