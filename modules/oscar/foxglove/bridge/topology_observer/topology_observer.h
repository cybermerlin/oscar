///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <unistd.h>

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <foxglove/websocket/websocket_notls.hpp>
#include <foxglove/websocket/websocket_server.hpp>

#include "cyber/cyber.h"
#include "modules/oscar/foxglove/bridge/topology_observer/channel_handler.h"

using apollo::cyber::proto::ChangeMsg;

// using HandlerMap = std::unordered_map<std::string, ChannelHandler>;
using HandlerMap =
    std::unordered_map<std::string, std::unique_ptr<ChannelHandler>>;

class TopologyObserver final {
 public:
  TopologyObserver(foxglove::Server<foxglove::WebSocketNoTls>& server,
                   apollo::cyber::Node& node);

  ~TopologyObserver();

  void subscribeCallback(foxglove::ChannelId chanId, foxglove::ConnHandle);
  void unsubscribeCallback(foxglove::ChannelId chanId, foxglove::ConnHandle);
  void subscribeConnectionGraphCallback(bool);

  void shutdown();

 private:
  void updateChannel(const ChangeMsg& change_msg);
  void tryAddChannel(const ChangeMsg& change_msg);
  void tryRemoveChannel(const ChangeMsg& change_msg);

  void updateConnectionGraph(const ChangeMsg& change_msg);
  void addTransformReader(const std::string& channel_name,
                          const std::string& msgTypeName);

  template <typename T_in>
  void addChannelReader(const std::string& channel_name,
                        const std::string& msgTypeName);

  template <typename T_in, typename T_out>
  void addChannelReader(const std::string& channel_name,
                        const std::string& msgTypeName);

 private:
  foxglove::Server<foxglove::WebSocketNoTls>& server_;
  apollo::cyber::Node& node_;

  std::shared_ptr<apollo::cyber::service_discovery::ChannelManager>
      channel_manager_ = nullptr;

  HandlerMap channelName2HandlerMap_;

  foxglove::MapOfSets publish_topics_;
  foxglove::MapOfSets subscribe_topics_;

  std::mutex update_mutex_;
};
