///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/bridge/topology_observer/topology_observer.h"

#include <algorithm>
#include <unordered_set>

#include <google/protobuf/descriptor.h>
#include <google/protobuf/descriptor.pb.h>

#include "modules/oscar/foxglove/utils/types_conversion.h"
#include "modules/oscar/foxglove/utils/utils.h"

using apollo::cyber::proto::ChangeType;
using apollo::cyber::proto::OperateType;
using apollo::cyber::proto::RoleType;

namespace {

inline auto getHandlerById(HandlerMap& handler_map, foxglove::ChannelId chanId)
    -> decltype(handler_map.begin()) {
  return std::find_if(
      handler_map.begin(), handler_map.end(),
      [chanId](auto&& el) { return el.second->get_channel_id() == chanId; });
}

constexpr std::array<std::string_view, 3> black_list_node_prefixes = {
    "logger",
    "MonitorReader",
    "TransformListener",
};
}  // namespace

TopologyObserver::~TopologyObserver() { channel_manager_->Shutdown(); }

TopologyObserver::TopologyObserver(
    foxglove::Server<foxglove::WebSocketNoTls>& server,
    apollo::cyber::Node& node)
    : server_(server),
      node_(node),
      channel_manager_(
          apollo::cyber::service_discovery::TopologyManager::Instance()
              ->channel_manager()) {
  channel_manager_->AddChangeListener([this](const ChangeMsg& change_msg) {
    updateConnectionGraph(change_msg);
    updateChannel(change_msg);
  });
}

void TopologyObserver::updateConnectionGraph(const ChangeMsg& change_msg) {
  auto role_t = change_msg.role_type();
  auto change_t = change_msg.change_type();
  auto op_t = change_msg.operate_type();

  if (change_t != ChangeType::CHANGE_CHANNEL) return;

  const std::string& channel_name = change_msg.role_attr().channel_name();
  const std::string& node_name = change_msg.role_attr().node_name();

  // filter cyber_monitor nodes, transform buffer nodes and monitor logging
  // nodes
  if (std::any_of(black_list_node_prefixes.begin(),
                  black_list_node_prefixes.end(), [&](auto&& prefix) {
                    return node_name.find(prefix) != std::string::npos;
                  }))
    return;

  foxglove::MapOfSets* active_map;
  switch (role_t) {
    case RoleType::ROLE_WRITER:
      active_map = &publish_topics_;
      break;
    case RoleType::ROLE_READER:
      active_map = &subscribe_topics_;
      break;
    case RoleType::ROLE_SERVER:
    case RoleType::ROLE_CLIENT:
    case RoleType::ROLE_NODE:
    case RoleType::ROLE_PARTICIPANT:
      break;
  }

  bool remove_empty_channel = false;
  switch (op_t) {
    case OperateType::OPT_JOIN:
      active_map->try_emplace(channel_name, std::unordered_set<std::string>());
      active_map->at(channel_name).insert(node_name);
      break;
    case OperateType::OPT_LEAVE:
      if (active_map->find(channel_name) == active_map->end()) break;
      active_map->at(channel_name).erase(node_name);
      if (active_map->at(channel_name).size() == 0) remove_empty_channel = true;
      break;
  }

  server_.updateConnectionGraph(publish_topics_, subscribe_topics_, {});

  // if we want to remove some topic from publishing / subscribing list,
  // we have to:
  // 1.send map with empty node list for this channel (done with command above)
  // 2. remove this channel from the map and send the map to the server.
  // Maybe this behavior will change after v1.0.0
  if (remove_empty_channel) {
    active_map->erase(channel_name);
    AINFO << "Connection graph: removing " << channel_name;
    server_.updateConnectionGraph(publish_topics_, subscribe_topics_, {});
  }
}

void TopologyObserver::updateChannel(const ChangeMsg& change_msg) {
  if (ChangeType::CHANGE_CHANNEL != change_msg.change_type() ||
      change_msg.role_attr().node_name() == node_.Name())
    return;

  std::lock_guard<std::mutex> lg(update_mutex_);
  switch (change_msg.operate_type()) {
    case OperateType::OPT_JOIN: {
      tryAddChannel(change_msg);
      break;
    }
    case OperateType::OPT_LEAVE:
      tryRemoveChannel(change_msg);
      break;
  }
}

void TopologyObserver::tryAddChannel(const ChangeMsg& change_msg) {
  if (RoleType::ROLE_WRITER != change_msg.role_type()) return;

  const std::string& channel_name = change_msg.role_attr().channel_name();
  const std::string& channel_type = change_msg.role_attr().message_type();

  // check if channel is already added
  if (channelName2HandlerMap_.find(channel_name) !=
      channelName2HandlerMap_.end()) {
    return;
  }

  channelName2HandlerMap_.emplace(
      channel_name,
      new ChannelHandler(server_, node_, channel_name, channel_type,
                         get_conversion_type_by_channel(channel_type)));
}

void TopologyObserver::tryRemoveChannel(const ChangeMsg& change_msg) {
  const std::string& channel_name = change_msg.role_attr().channel_name();

  if (channelName2HandlerMap_.find(channel_name) ==
      channelName2HandlerMap_.end())
    return;

  std::vector<apollo::cyber::proto::RoleAttributes> readers;
  channel_manager_->GetReadersOfChannel(channel_name, &readers);

  bool to_remove =
      !channel_manager_->HasWriter(channel_name) &&
      (!channel_manager_->HasReader(channel_name) ||
       (readers.size() == 1 &&
        readers[0].node_name().find(node_.Name()) != std::string::npos));

  if (!to_remove) {
    AINFO << "Channel not removed because it has readers/writers for channel: "
          << channel_name;
    return;
  }

  auto chanId = channelName2HandlerMap_.at(channel_name)->get_channel_id();
  AINFO << "Removing reader for channel: " << channel_name
        << " with Id: " << chanId;

  channelName2HandlerMap_.at(channel_name)->disable();

  // TODO: there somewhere a bug, because client sends Warning when we remove
  // channel (Client subscription id X did not exist; ignoring unsubscription)
  server_.removeChannels({chanId});
  channelName2HandlerMap_.erase(channel_name);
}

void TopologyObserver::subscribeCallback(foxglove::ChannelId chanId,
                                         foxglove::ConnHandle) {
  std::lock_guard<std::mutex> lg(update_mutex_);

  auto handlerIt = getHandlerById(channelName2HandlerMap_, chanId);

  if (handlerIt != channelName2HandlerMap_.end()) {
    handlerIt->second->enable();
    AERROR << "client subscribed to id " << chanId << " "
           << handlerIt->second->get_channel_name();
  } else {
    AERROR << "Client subscribed to unknown channel id: " << chanId;
  }
}

void TopologyObserver::unsubscribeCallback(foxglove::ChannelId chanId,
                                           foxglove::ConnHandle) {
  std::lock_guard<std::mutex> lg(update_mutex_);

  auto handlerIt = getHandlerById(channelName2HandlerMap_, chanId);

  if (handlerIt != channelName2HandlerMap_.end()) {
    handlerIt->second->disable();
    AERROR << "client unsubscribed from id " << chanId << " "
           << handlerIt->second->get_channel_name();
  } else {
    AERROR << "Client unsubscribed from unknown channel id: " << chanId;
  }
}

void TopologyObserver::subscribeConnectionGraphCallback(bool wtf) {
  AERROR << "Connection graph subscription " << wtf;
}
