///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/bridge/foxglove_bridge.h"

#include <iostream>
#include <regex>
#include <utility>

#include "modules/common/adapters/adapter_gflags.h"

#define SET_METHOD(method) \
  [&](auto&&... ts) { method(std::forward<decltype(ts)>(ts)...); }

// Mysterious mandatory template specialization
namespace foxglove {
template <>
void Server<WebSocketNoTls>::setupTlsHandler() {}
}  // namespace foxglove

namespace apollo {
namespace oscar {

const foxglove::ServerOptions defaultOptions = {
    .capabilities =
        {
            // foxglove::CAPABILITY_CLIENT_PUBLISH,
            foxglove::CAPABILITY_CONNECTION_GRAPH,
            foxglove::CAPABILITY_PARAMETERS_SUBSCRIBE,
            foxglove::CAPABILITY_PARAMETERS,
            // foxglove::CAPABILITY_SERVICES,
        },
    .supportedEncodings = {},
    .metadata = {},
    .sendBufferLimitBytes = 100 * 1024 * 1024,  // 100 MB
    .useTls = false,
    .certfile = "",
    .keyfile = "",
    .sessionId = "",
    .useCompression = false,
};

constexpr std::array debug_levels = {
    "Debug", "Info", "Warn", "Error", "Critical",
};

const auto logHandler = [](foxglove::WebSocketLogLevel level, char const* msg) {
  AINFO << "Message from FoxGlove Server: ("
        << debug_levels[static_cast<unsigned>(level)] << ") " << msg;
};

FoxGloveBridge::FoxGloveBridge(
    apollo::cyber::Node& node,
    std::shared_ptr<apollo::oscar::FoxGloveBridgeConfig> config)
    : server_("C++ Protobuf server", logHandler, defaultOptions),
      topology_observer_(server_, node),
      parameter_manager_(server_, config->parameters_file(),
                         config->car_urdf_file()) {
  setHandlers();
  server_.start("0.0.0.0", config->port());
  AINFO << "Init complete";
}

FoxGloveBridge::~FoxGloveBridge() { server_.stop(); }

void FoxGloveBridge::setHandlers() {
  server_.setHandlers({
      .subscribeHandler = SET_METHOD(topology_observer_.subscribeCallback),
      .unsubscribeHandler = SET_METHOD(topology_observer_.unsubscribeCallback),
      .clientAdvertiseHandler = {},
      .clientUnadvertiseHandler = {},
      .clientMessageHandler = {},
      .parameterRequestHandler = SET_METHOD(parameter_manager_.requestCallback),
      .parameterChangeHandler = SET_METHOD(parameter_manager_.changeCallback),
      .parameterSubscriptionHandler =
          SET_METHOD(parameter_manager_.subscriptionCallback),
      .serviceRequestHandler = {},
      .subscribeConnectionGraphHandler =
          SET_METHOD(topology_observer_.subscribeConnectionGraphCallback),
  });
}

}  // namespace oscar
}  // namespace apollo
