///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/bridge/parameter_manager/parameter_manager.h"

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <unordered_map>

#include <foxglove/websocket/websocket_server.hpp>

#include "yaml-cpp/yaml.h"

#include "cyber/common/log.h"

#if __GNUC__ < 8
#include <experimental/filesystem>
using std::experimental::filesystem::exists;
#else
#include <filesystem>
using std::filesystem::exists;
#endif

namespace {
enum YamlFieldTypes {
  BOOL,
  BOOL_LIST,
  INT,
  INT_LIST,
  FLOAT,
  FLOAT_LIST,
  STRING,
  STRING_LIST,
};

const std::unordered_map<std::string, YamlFieldTypes> type_str_to_enum{
    {"bool", BOOL},   {"bool_list", BOOL_LIST},
    {"int", INT},     {"int_list", INT_LIST},
    {"float", FLOAT}, {"float_list", FLOAT_LIST},
    {"str", STRING},  {"str_list", STRING_LIST},
};

template <typename T>
std::vector<T> extract_vector(const YAML::Node& it) {
  std::vector<T> ret_vec(it.size());
  std::transform(it.begin(), it.end(), ret_vec.begin(),
                 [](YAML::detail::iterator_value i) { return i.as<T>(); });

  return ret_vec;
}

template <typename T>
void print_array(std::vector<T> val_array) {
  std::copy(val_array.begin(), val_array.end(),
            std::ostream_iterator<T>(std::cout, " "));
}

}  // namespace
namespace apollo {
namespace oscar {

void ParameterManager::readFromFile(const std::string& yaml_file) {
  if (!exists(yaml_file)) {
    throw std::runtime_error("YAML file not found: " + yaml_file);
  }

  YAML::Node node = YAML::LoadFile(yaml_file);

  for (const auto& it : node) {
    auto name = it.first.as<std::string>();
    auto value = it.second["value"];

    auto t = it.second["type"].as<std::string>();

    if (type_str_to_enum.find(t) == type_str_to_enum.end()) {
      throw std::runtime_error("Invalid type field in YAML file: " + t);
    }
    switch (type_str_to_enum.at(t)) {
      case INT:
        params_.emplace_back(name, value.as<int>());
        break;
      case FLOAT:
        params_.emplace_back(name, value.as<double>());
        break;
      case STRING:
        params_.emplace_back(name, value.as<std::string>());
        break;
      case BOOL:
        params_.emplace_back(name, value.as<bool>());
        break;
      case INT_LIST: {
        params_.emplace_back(name, extract_vector<int>(value));
        break;
      }
      case BOOL_LIST:
        params_.emplace_back(name, extract_vector<bool>(value));
        break;
      case FLOAT_LIST:
        params_.emplace_back(name, extract_vector<double>(value));
        break;
      case STRING_LIST:
        params_.emplace_back(name, extract_vector<std::string>(value));
        break;
    }
  }
}

void ParameterManager::printParams() {
  std::cout << "Parameters: \n";
  for (auto& param : params_) {
    std::cout << param.getName() << " " << std::boolalpha;

    switch (param.getType()) {
      case foxglove::ParameterType::PARAMETER_NOT_SET:
        std::cout << "Not set";
        break;
      case foxglove::ParameterType::PARAMETER_BOOL:
        std::cout << " [Bool] " << param.getValue<bool>();
        break;
      case foxglove::ParameterType::PARAMETER_INTEGER:
        std::cout << " [Int] " << param.getValue<int64_t>();
        break;
      case foxglove::ParameterType::PARAMETER_DOUBLE:
        std::cout << " [Double] " << param.getValue<double>();
        break;
      case foxglove::ParameterType::PARAMETER_STRING:
        std::cout << " [String] " << param.getValue<std::string>();
        break;
      case foxglove::ParameterType::PARAMETER_BOOL_ARRAY:
        std::cout << " [Bool Array] ";
        print_array(param.getValue<std::vector<bool>>());
        break;
      case foxglove::ParameterType::PARAMETER_INTEGER_ARRAY:
        std::cout << " [Int Array] ";
        print_array(param.getValue<std::vector<int64_t>>());
        break;
      case foxglove::ParameterType::PARAMETER_DOUBLE_ARRAY:
        std::cout << " [Double Array] ";
        print_array(param.getValue<std::vector<double>>());
        break;
      case foxglove::ParameterType::PARAMETER_STRING_ARRAY:
        std::cout << " [String Array] ";
        print_array(param.getValue<std::vector<std::string>>());
        break;
    }

    std::cout << "\n";
  }
}

void ParameterManager::addRobotDescription(const std::string& urdf_file) {
  if (!exists(urdf_file)) {
    throw std::runtime_error("Car URDF file not found: " + urdf_file);
  }

  std::fstream ifs(urdf_file);

  std::string urdf_content((std::istreambuf_iterator<char>(ifs)),
                           (std::istreambuf_iterator<char>()));

  params_.emplace_back("/robot_description", urdf_content);
}

void ParameterManager::subscriptionCallback(
    const std::vector<std::string>& what,
    foxglove::ParameterSubscriptionOperation name, foxglove::ConnHandle ch) {
  AINFO << "Start parameters subscription";
  server_.publishParameterValues(ch, getParams());
}

void ParameterManager::requestCallback(const std::vector<std::string>& what,
                                       const std::optional<std::string>& name,
                                       foxglove::ConnHandle ch) {
  AINFO << "Parameter request";
  server_.publishParameterValues(ch, getParams());
}

void ParameterManager::changeCallback(
    const std::vector<foxglove::Parameter>& params,
    const std::optional<std::string>& wtf, foxglove::ConnHandle ch) {
  AERROR << " Changing parameters not implemented.";
}

}  // namespace oscar
}  // namespace apollo
