///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <math.h>

#include <chrono>
#include <codecvt>
#include <functional>
#include <iostream>
#include <locale>
#include <map>
#include <queue>
#include <string>
#include <utility>
#include <variant>
#include <vector>

#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/oscar/foxglove/schemas/CompressedImage.pb.h"
#include "modules/oscar/foxglove/schemas/FrameTransforms.pb.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"
#include "modules/oscar/foxglove/schemas/PointCloud.pb.h"
#include "modules/oscar/foxglove/schemas/RawImage.pb.h"

#include "cyber/message/raw_message.h"
#include "cyber/record/record_message.h"
#include "modules/oscar/foxglove/utils/utils.h"

using apollo::cyber::record::RecordMessage;

void convertTimestamp(double src_ts, google::protobuf::Timestamp* out_ts);

void convertCyberMessageToFoxGlove(
    const apollo::transform::TransformStampeds& src_msg, std::string& out_msg);

void convertCyberMessageToFoxGlove(
    const apollo::cyber::message::RawMessage& src_msg, std::string& out_msg);

void convertCyberMessageToFoxGlove(const apollo::drivers::Image& src_image,
                                   std::string& out_msg,
                                   const bool compress = false);

void convertCyberMessageToFoxGlove(const apollo::drivers::PointCloud& src_pc,
                                   std::string& out_serialized_msg,
                                   bool keep_intensity = true,
                                   bool keep_ts = false);

void convertCyberMessageToFoxGlove(
    const apollo::drivers::CompressedImage& src_image, std::string& out_msg);

void convertCyberMessageToFoxGlove(const std::string& src_msg,
                                   const std::string& type_name,
                                   std::string& out_msg,
                                   const bool compress_if_possible);

void convertCyberMessageToFoxGlove(const RecordMessage& src_msg,
                                   const std::string& type_name,
                                   RecordMessage& out_msg,
                                   const bool compress_if_possible = true);

const std::string convertTypeName(const std::string& type_name,
                                  const bool compress_images = true);

template <typename T>
T parse_pb_message(const std::string& msg) {
  T out_msg;
  out_msg.ParseFromString(msg);
  return out_msg;
}

template <typename T>
T parse_pb_message(const RecordMessage& msg) {
  T out_msg;
  out_msg.ParseFromString(msg.content);
  return out_msg;
}

google::protobuf::FileDescriptorSet BuildFileDescriptorSet(
    const google::protobuf::Descriptor* toplevelDescriptor);

enum class Conversion {
  NO_CONVERSION,
  POINTCLOUD_APOLLO2FOXGLOVE,
  RAWIMAGE_APOLLO2FOXGLOVE,
  COMPRESSEDIMAGE_APOLLO2FOXGLOVE,
};

using apollo::cyber::message::RawMessage;

struct MessageTransformerPass {
  constexpr MessageTransformerPass() {}
  static void transform(const std::string& src_msg, std::string& out_msg) {
    out_msg = src_msg;
  }

  static const google::protobuf::Descriptor* get_out_descriptor(
      const std::string& msg_type) {
    return get_apollo_descriptor(msg_type);
  }
};

template <typename ApolloType, typename FoxgloveType>
struct Apollo2FoxgloveTransformer {
  constexpr Apollo2FoxgloveTransformer() = default;
  static void transform(const std::string& src_msg, std::string& out_msg) {
    ApolloType apollo_pc;
    apollo_pc.ParseFromString(src_msg);
    convertCyberMessageToFoxGlove(apollo_pc, out_msg);
  }

  static const google::protobuf::Descriptor* get_out_descriptor(
      const std::string& msg_type) {
    return FoxgloveType::GetDescriptor();
  }
};

Conversion get_conversion_type_by_channel(const std::string& type_name);

#define SET_FUNCTION(method) \
  [](auto&... ts) { return method(std::forward<decltype(ts)>(ts)...); }

using MessageTransformerPointCloud2FoxGlove =
    Apollo2FoxgloveTransformer<apollo::drivers::PointCloud,
                               foxglove::PointCloud>;
using MessageTransformerRawImage2Foxglove =
    Apollo2FoxgloveTransformer<apollo::drivers::Image, foxglove::RawImage>;
using MessageTransformerCompressedImage2Foxglove =
    Apollo2FoxgloveTransformer<apollo::drivers::CompressedImage,
                               foxglove::CompressedImage>;

using transformer_variant =
    std::variant<MessageTransformerPass, MessageTransformerRawImage2Foxglove,
                 MessageTransformerCompressedImage2Foxglove,
                 MessageTransformerPointCloud2FoxGlove>;

const std::unordered_map<Conversion, transformer_variant> transformers_v_map = {
    {Conversion::NO_CONVERSION, MessageTransformerPass()},
    {Conversion::POINTCLOUD_APOLLO2FOXGLOVE,
     MessageTransformerPointCloud2FoxGlove()},
    {Conversion::RAWIMAGE_APOLLO2FOXGLOVE,
     MessageTransformerRawImage2Foxglove()},
    {Conversion::COMPRESSEDIMAGE_APOLLO2FOXGLOVE,
     MessageTransformerCompressedImage2Foxglove()},
};

struct MessageTransformer {
 public:
  MessageTransformer() = delete;

  explicit MessageTransformer(Conversion conversion_type)
      : transformer_(transformers_v_map.at(conversion_type)) {}

  void transform(const std::string& src_msg,
                 std::string& out_msg) const noexcept;

  [[nodiscard]] const google::protobuf::Descriptor* get_out_descriptor(
      const std::string& read_msg_type) const noexcept;

 private:
  const transformer_variant transformer_;
};
