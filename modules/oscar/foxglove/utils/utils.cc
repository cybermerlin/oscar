///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/utils/utils.h"

#include <unordered_map>

#include <google/protobuf/util/time_util.h>
#include "modules/common_msgs/chassis_msgs/chassis.pb.h"
#include "modules/common_msgs/control_msgs/control_cmd.pb.h"
#include "modules/common_msgs/dreamview_msgs/hmi_status.pb.h"
#include "modules/common_msgs/localization_msgs/gps.pb.h"
#include "modules/common_msgs/localization_msgs/imu.pb.h"
#include "modules/common_msgs/localization_msgs/localization.pb.h"
#include "modules/common_msgs/perception_msgs/perception_lane.pb.h"
#include "modules/common_msgs/perception_msgs/perception_obstacle.pb.h"
#include "modules/common_msgs/planning_msgs/planning.pb.h"
#include "modules/common_msgs/prediction_msgs/prediction_obstacle.pb.h"
#include "modules/common_msgs/sensor_msgs/gnss_best_pose.pb.h"
#include "modules/common_msgs/sensor_msgs/heading.pb.h"
#include "modules/common_msgs/sensor_msgs/imu.pb.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/common_msgs/monitor_msgs/monitor_log.pb.h"
#include "modules/common_msgs/monitor_msgs/system_status.pb.h"
#include "modules/common/latency_recorder/proto/latency_record.pb.h"

#include "modules/drivers/gnss/proto/gnss_status.pb.h"

#include "modules/oscar/foxglove/schemas/CompressedImage.pb.h"
#include "modules/oscar/foxglove/schemas/FrameTransforms.pb.h"
#include "modules/oscar/foxglove/schemas/ImageAnnotations.pb.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"
#include "modules/oscar/foxglove/schemas/PointCloud.pb.h"
#include "modules/oscar/foxglove/schemas/RawImage.pb.h"
#include "modules/oscar/foxglove/schemas/SceneUpdate.pb.h"
#include "modules/oscar/foxglove/schemas/CameraCalibration.pb.h"

#include "modules/common_msgs/chassis_msgs/chassis.pb.h"
#include "modules/common_msgs/control_msgs/control_cmd.pb.h"
#include "modules/common_msgs/dreamview_msgs/hmi_status.pb.h"
#include "modules/common_msgs/localization_msgs/gps.pb.h"
#include "modules/common_msgs/localization_msgs/imu.pb.h"
#include "modules/common_msgs/localization_msgs/localization.pb.h"
#include "modules/common_msgs/perception_msgs/perception_lane.pb.h"
#include "modules/common_msgs/perception_msgs/perception_obstacle.pb.h"
#include "modules/common_msgs/planning_msgs/planning.pb.h"
#include "modules/common_msgs/prediction_msgs/prediction_obstacle.pb.h"
#include "modules/common_msgs/sensor_msgs/gnss_best_pose.pb.h"
#include "modules/common_msgs/sensor_msgs/heading.pb.h"
#include "modules/common_msgs/sensor_msgs/imu.pb.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/drivers/gnss/proto/gnss_status.pb.h"
#include "modules/oscar/foxglove/schemas/ImageAnnotations.pb.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"
#include "modules/oscar/foxglove/schemas/SceneUpdate.pb.h"
#include "modules/oscar/monitor/proto/monitor_message.pb.h"

#include "cyber/cyber.h"

// Adapted from
// https://gist.github.com/tomykaira/f0fd86b6c73063283afe550bc5d77594
std::string Base64Encode(const std::string data) {
  static constexpr char sEncodingTable[] = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
      'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
      'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

  size_t in_len = data.size();
  size_t out_len = 4 * ((in_len + 2) / 3);
  std::string ret(out_len, '\0');
  size_t i;
  char* p = const_cast<char*>(ret.c_str());

  for (i = 0; i < in_len - 2; i += 3) {
    *p++ = sEncodingTable[(data[i] >> 2) & 0x3F];
    *p++ = sEncodingTable[((data[i] & 0x3) << 4) |
                          (static_cast<int>(data[i + 1] & 0xF0) >> 4)];
    *p++ = sEncodingTable[((data[i + 1] & 0xF) << 2) |
                          (static_cast<int>(data[i + 2] & 0xC0) >> 6)];
    *p++ = sEncodingTable[data[i + 2] & 0x3F];
  }
  if (i < in_len) {
    *p++ = sEncodingTable[(data[i] >> 2) & 0x3F];
    if (i == (in_len - 1)) {
      *p++ = sEncodingTable[((data[i] & 0x3) << 4)];
      *p++ = '=';
    } else {
      *p++ = sEncodingTable[((data[i] & 0x3) << 4) |
                            (static_cast<int>(data[i + 1] & 0xF0) >> 4)];
      *p++ = sEncodingTable[((data[i + 1] & 0xF) << 2)];
    }
    *p++ = '=';
  }

  return ret;
}

uint64_t nanosecondsSinceEpoch() {
  return uint64_t(std::chrono::duration_cast<std::chrono::nanoseconds>(
                      std::chrono::system_clock::now().time_since_epoch())
                      .count());
}

// taken from
// https://github.com/foxglove/ws-protocol/blob/66fdb6fbb4d2022f0d7040542376533d59e9a674/cpp/examples/example_server.cpp#L59
// Writes the FileDescriptor of this descriptor and all transitive dependencies
// to a string, for use as a channel schema.
std::string SerializeFdSet(
    const google::protobuf::Descriptor* toplevelDescriptor) {
  google::protobuf::FileDescriptorSet fdSet;
  std::queue<const google::protobuf::FileDescriptor*> toAdd;
  toAdd.push(toplevelDescriptor->file());
  std::unordered_set<std::string> seenDependencies;
  while (!toAdd.empty()) {
    const google::protobuf::FileDescriptor* next = toAdd.front();
    toAdd.pop();
    next->CopyTo(fdSet.add_file());
    for (int i = 0; i < next->dependency_count(); ++i) {
      const auto& dep = next->dependency(i);
      if (seenDependencies.find(dep->name()) == seenDependencies.end()) {
        seenDependencies.insert(dep->name());
        toAdd.push(dep);
      }
    }
  }
  return fdSet.SerializeAsString();
}

using DescriptorFn = std::function<const google::protobuf::Descriptor*(void)>;

const std::unordered_map<std::string, DescriptorFn> descriptors_map {
  {"apollo.oscar.monitor.MonitorMessage", DescriptorFn(&apollo::oscar::monitor::MonitorMessage::descriptor)},
  {"apollo.drivers.gnss.GnssBestPose", DescriptorFn(&apollo::drivers::gnss::GnssBestPose::descriptor)},
  {"apollo.drivers.gnss.InsStatus", DescriptorFn(&apollo::drivers::gnss::InsStatus::descriptor)},
  {"apollo.drivers.gnss.Imu", DescriptorFn(&apollo::drivers::gnss::Imu::descriptor)},
  {"apollo.drivers.Image", DescriptorFn(&apollo::drivers::Image::descriptor)},
  {"apollo.drivers.CompressedImage", DescriptorFn(&apollo::drivers::CompressedImage::descriptor)},
  {"apollo.drivers.gnss.GnssStatus", DescriptorFn(&apollo::drivers::gnss::GnssStatus::descriptor)},
  {"apollo.drivers.PointCloud", DescriptorFn(&apollo::drivers::PointCloud::descriptor)},

  {"apollo.localization.LocalizationEstimate", DescriptorFn(&apollo::localization::LocalizationEstimate::descriptor)},
  {"apollo.localization.LocalizationStatus", DescriptorFn(&apollo::localization::LocalizationStatus::descriptor)},
  {"apollo.localization.CorrectedImu", DescriptorFn(&apollo::localization::CorrectedImu::descriptor)},
  {"apollo.localization.Gps", DescriptorFn(&apollo::localization::Gps::descriptor)},

  {"apollo.common.monitor.MonitorMessage", DescriptorFn(&apollo::common::monitor::MonitorMessage::descriptor)},
  {"apollo.common.LatencyReport", DescriptorFn(&apollo::common::LatencyReport::descriptor)},
  {"apollo.common.LatencyRecordMap", DescriptorFn(&apollo::common::LatencyRecordMap::descriptor)},

  {"apollo.perception.PerceptionObstacles", DescriptorFn(&apollo::perception::PerceptionObstacles::descriptor)},
  {"apollo.perception.PerceptionLanes", DescriptorFn(&apollo::perception::PerceptionLanes::descriptor)},
  
  {"apollo.routing.RoutingRequest", DescriptorFn(&apollo::routing::RoutingRequest::descriptor)},
  {"apollo.routing.RoutingResponse", DescriptorFn(&apollo::routing::RoutingResponse::descriptor)},

  {"apollo.control.ControlCommand", DescriptorFn(&apollo::control::ControlCommand::descriptor)},
  {"apollo.control.PadMessage", DescriptorFn(&apollo::control::PadMessage::descriptor)},

  {"apollo.canbus.Chassis", DescriptorFn(&apollo::canbus::Chassis::descriptor)},
  {"apollo.dreamview.HMIStatus", DescriptorFn(&apollo::dreamview::HMIStatus::descriptor)},
  {"apollo.planning.ADCTrajectory", DescriptorFn(&apollo::planning::ADCTrajectory::descriptor)},
  {"apollo.prediction.PredictionObstacles", DescriptorFn(&apollo::prediction::PredictionObstacles::descriptor)},
  {"apollo.transform.TransformStampeds", DescriptorFn(&apollo::transform::TransformStampeds::descriptor)},
  {"apollo.monitor.SystemStatus", DescriptorFn(&apollo::monitor::SystemStatus::descriptor)},


  {"foxglove.CompressedImage", DescriptorFn(&foxglove::CompressedImage::descriptor)},
  {"foxglove.RawImage", DescriptorFn(&foxglove::RawImage::descriptor)},
  {"foxglove.PointCloud", DescriptorFn(&foxglove::PointCloud::descriptor)},
  {"foxglove.ImageAnnotations", DescriptorFn(&foxglove::ImageAnnotations::descriptor)},
  {"foxglove.SceneUpdate", DescriptorFn(&foxglove::SceneUpdate::descriptor)},
  {"foxglove.Log", DescriptorFn(&foxglove::Log::descriptor)},
  {"foxglove.CameraCalibration", DescriptorFn(&foxglove::CameraCalibration::descriptor)},
};

// for descriptors not found by ProtobufFactory, we can manually set them
// a workaround for the bug described in this issue:
// https://github.com/ApolloAuto/apollo/issues/14777
const google::protobuf::Descriptor* getDescriptorByName(const std::string& t) {
  auto it = descriptors_map.find(t);
  return it != descriptors_map.end() ? it->second() : nullptr;
}

const google::protobuf::Descriptor* get_apollo_descriptor(
    const std::string& channel_type) {
  const google::protobuf::Descriptor* descriptor_out = nullptr;
  descriptor_out = apollo::cyber::message::ProtobufFactory::Instance()
                       ->FindMessageTypeByName(channel_type);

  if (descriptor_out == nullptr) {
    descriptor_out = getDescriptorByName(channel_type);
  }
  return descriptor_out;
}
