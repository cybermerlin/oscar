///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <chrono>
#include <queue>
#include <string>
#include <unordered_set>

#include <google/protobuf/descriptor.pb.h>

// Adapted from
// https://gist.github.com/tomykaira/f0fd86b6c73063283afe550bc5d77594
std::string Base64Encode(const std::string data);

uint64_t nanosecondsSinceEpoch();

// taken from
// https://github.com/foxglove/ws-protocol/blob/66fdb6fbb4d2022f0d7040542376533d59e9a674/cpp/examples/example_server.cpp#L59
// Writes the FileDescriptor of this descriptor and all transitive dependencies
// to a string, for use as a channel schema.
std::string SerializeFdSet(
    const google::protobuf::Descriptor* toplevelDescriptor);

// for descriptors not found by ProtobufFactory, we can manually set them
const google::protobuf::Descriptor* getDescriptorByName(const std::string& t);

const google::protobuf::Descriptor* get_apollo_descriptor(
    const std::string& channel_type);
