#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
LOGS_FILE=/apollo/data/log/foxglove_logs.txt

cd $SCRIPT_DIR

! [ -f /.dockerenv ] && echo "Starting bridge must be used inside docker container" && exit 1

DAG_FILE=/apollo/modules/calibration/data/mb_actros/foxglove_conf/foxglove_bridge.dag

[ ! -f "$DAG_FILE" ] && DAG_FILE=/apollo/modules/oscar/foxglove/bridge/dag/foxglove_bridge.dag &&
    echo "No DAG file found in car config folder. Falling back to default DAG location"

RUNNING_PID=$(pgrep -f "mainboard -d $DAG_FILE")
if ! [[ $RUNNING_PID -eq "" ]]; then
    echo "Killing previously running bridge"
    kill -SIGINT $RUNNING_PID
    sleep 1
    RUNNING_PID=$(pgrep -f "mainboard -d $DAG_FILE")
    if ! [[ $RUNNING_PID -eq "" ]]; then
        echo "Error: running bridge not closed. PID: $RUNNING_PID"
        exit 1
    fi
fi

if [ "$1" = "--debug" ]; then
    export GLOG_alsologtostderr=1
    mainboard -d $DAG_FILE
else
    echo "starting FoxGlove bridge in background"
    nohup mainboard -d $DAG_FILE ${@:2} >$LOGS_FILE 2>&1 &

    sleep 1
    pgrep -f "mainboard -d $DAG_FILE" >/dev/null || cat $LOGS_FILE
fi
