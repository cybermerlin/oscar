#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

CONTAINER_NAME=${USER}_foxglove_studio

if [ ! -f /.dockerenv ]; then

    running_containers="$(docker container list --format '{{.Names}}' | grep $CONTAINER_NAME)"
    [ "$running_containers" != $CONTAINER_NAME ] && echo "FoxGlove Studio web does not have running container" && exit
    
    echo "Stopping container with FoxGlove web: $CONTAINER_NAME"
    docker stop $CONTAINER_NAME > /dev/null 2>&1
else
    echo "Stopping container with FoxGlove web should be used outside docker container"
fi