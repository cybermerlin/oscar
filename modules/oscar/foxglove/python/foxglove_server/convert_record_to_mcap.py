#!/apollo/bazel-bin/modules/oscar/foxglove/py/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from multiprocessing.pool import AsyncResult
import os
import sys
import time
from typing import Iterable

from mcap_protobuf.writer import Writer

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record

import multiprocessing as mp
from queue import Queue

from utils import channel_types_classes, convertMessage

import tqdm
import tqdm.asyncio


def prepareMessage(channelname, msg, datatype, timestamp):
    return channelname, convertMessage(msg, datatype), timestamp


class Converter:
    def __init__(self, args) -> None:
        self.args = args
        self.writer = None
        self.writer_idx = -1
        self.out_stream = None

        self.start_ts = 0
        self.time_limit = sys.maxsize if args.max_dur == 0 else args.max_dur

    def updateWriter(self):
        self.closeWriter()

        self.writer_idx += 1
        current_out_file = f'{self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap'
        self.out_stream = open(current_out_file, 'wb')
        self.writer = Writer(self.out_stream)

    def closeWriter(self):
        if self.writer is not None:
            self.writer.finish()
        if self.out_stream is not None:
            self.out_stream.close()

        if self.writer is not None and self.args.compress:
            os.system(
                f'mcap compress {self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap --compression lz4 -o {self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}_lz4.mcap')
            os.remove(
                f'{self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap')
            os.rename(f'{self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}_lz4.mcap',
                      f'{self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap')

    def run(self):
        os.makedirs(self.args.dst_folder, exist_ok=True)

        with mp.Pool(processes=mp.cpu_count() - 2) as pool:

            pbar = tqdm.tqdm(
                sorted(os.listdir(self.args.src_folder)), position=0)

            self.updateWriter()

            for f in pbar:
                total_size = os.path.getsize(
                    f'{self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap') / 1024**3

                pbar.set_description(
                    f'Size: [{total_size:.2f}/{self.args.max_size} GB] {self.args.dst_folder}/{self.args.prefix}_{self.writer_idx}.mcap')
                src_file = f'{self.args.src_folder}/{f}'

                # open new file when size of a current file reaches limit
                if total_size > self.args.max_size:
                    self.updateWriter()

                self.processFile(src_file, pool)

        self.closeWriter()

    def processFile(self, input_file, process_pool):
        cyberReader = record.RecordReader(input_file)
        out_queue = Queue()

        for channelname, msg, datatype, timestamp in cyberReader.read_messages():
            if datatype not in channel_types_classes:
                continue

            if channelname in self.args.black_list:
                continue
            if len(self.args.white_list) > 0:
                if channelname not in self.args.white_list:
                    continue

            if datatype == 'apollo.drivers.PointCloud':
                out_queue.put(process_pool.apply_async(
                    prepareMessage, (channelname, msg, datatype, timestamp)))
            else:
                out_queue.put(prepareMessage(
                    channelname, msg, datatype, timestamp))

        pbar = tqdm.tqdm(total=out_queue.qsize(), position=1, leave=False)

        while out_queue.qsize() > 0:
            el = out_queue.get()
            if isinstance(el, AsyncResult):
                el = el.get()

            channelname, msg, timestamp = el

            if self.start_ts == 0:
                self.start_ts = timestamp

            if (timestamp - self.start_ts) / 1e9 > self.time_limit:
                self.updateWriter()
                self.start_ts = timestamp

            if isinstance(msg, Iterable):
                for m in msg:
                    self.writer.write_message(
                        topic=channelname,
                        log_time=timestamp,
                        publish_time=timestamp,
                        message=m
                    )
            else:
                self.writer.write_message(
                    topic=channelname,
                    log_time=timestamp,
                    publish_time=timestamp,
                    message=msg
                )
            pbar.update()
        pbar.close()


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('src_folder', type=str, default=f'/apollo/data/bag/13.09/13.09_inf_ruby_r',
                        help='path to source .record files')

    parser.add_argument('dst_folder', type=str, default=f'/apollo/data/bag/mcap',
                        help='path to output .mcap files')
    parser.add_argument('-p', '--prefix', type=str, default=f'out',
                        help='prefix for output files. files are saved in {dst_folder}/{prefix}_{idx}.mcap')
    parser.add_argument('--max_size', type=float, default=20,
                        help='maximum size of one output_file, in GB')
    parser.add_argument('--max_dur', type=int, default=0,
                        help='maximum duration for one file, in seconds. 0 - without limits.')
    parser.add_argument('--compress', action='store_true',
                        help='change compression algorithm to lz4')

    parser.add_argument("-w", '--white_list', type=str,
                        nargs='*', default=[], help="channels to keep in MCAP")
    parser.add_argument("-b", '--black_list', type=str, nargs='*',
                        default=[], help="channels to remove from MCAP")

    args = parser.parse_args()
    print(args)

    st = time.time()
    converter = Converter(args)
    converter.run()

    dur = time.time() - st
    print(f'Convertion took {dur:.2f} seconds')


if __name__ == "__main__":
    main()
