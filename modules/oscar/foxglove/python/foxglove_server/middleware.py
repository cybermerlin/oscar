from multiprocessing.pool import AsyncResult
import threading
import time
import logging
from typing import Callable, FrozenSet, NewType
from foxglove_websocket.types import ChannelWithoutId
import sys
import multiprocessing as mp
from queue import Queue

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


# when launching FoxGlove server from Dreamview there is another folder under the path below.
# We remove it from the current sys.path to access /apollo/cyber package.

redundant_path = '/apollo/bazel-bin/cyber/tools/cyber_launch/cyber_launch.runfiles/apollo'
if redundant_path in sys.path:
    sys.path.remove(redundant_path)

try:
    from cyber.python.cyber_py3 import cyber
except ModuleNotFoundError as e:
    print("Cyber not found. Is it built?")

from utils import channel_types_classes, cyber_foxglove_types_mapping, convertRawImage, convertCompressedImage, convertPointCloud, convertTransforms, getFGSchema

logger = logging.getLogger("ExampleMiddlewareThread")
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setFormatter(
    logging.Formatter("%(asctime)s: [%(levelname)s] <%(name)s> %(message)s")
)
logger.propagate = False


class DuplicateFilter:
    def __init__(self, substrings=[]) -> None:
        self.msgs = set()
        self.substrings = substrings

    def filter(self, record):
        msg = record.msg
        if all([sub not in msg for sub in self.substrings]):
            return True
        elif msg in self.msgs:
            return False
        else:
            self.msgs.add(msg)
            return True


duplicateFilter = DuplicateFilter(substrings=[
    "is not in classes list",
    "in blacklist",
    "lidar data",
])
logger.addFilter(duplicateFilter)

MiddlewareChannelId = NewType("MiddlewareChannelId", int)


def convertPointCloudWrapped(ch_id, t, msg):
    return ch_id, t, convertPointCloud(msg)


class ExampleMiddlewareThread(threading.Thread):
    """
    This class simulates a pub/sub middleware that provides callbacks in a separate thread. The
    implementation details are not meant to be realistic, but just to simulate an environment where
    channels are appearing and disappearing and messages are arriving at random times.

    Calling code can provide callbacks which will be called from the middleware thread. To do so,
    set the `on_add_channel`, `on_remove_channel`, and `on_message` properties.

    This is a subclass of threading.Thread, so to launch the thread, use the `start()` method.
    """

    # The middleware will call these callbacks from the middleware thread.
    on_add_channel: Callable[[MiddlewareChannelId, ChannelWithoutId], None]
    on_remove_channel: Callable[[MiddlewareChannelId], None]
    on_message: Callable[[MiddlewareChannelId, int, bytes], None]

    # When the server subscribes to a channel, we'll get called in the server thread (the main
    # thread). This lock is used to manage the set of subscribed channels safely across multiple
    # threads.
    #
    # We use a frozenset to indicate that we won't mutate the data structure, we'll just replace it
    # when subscriptions change. This allows the thread's main loop to briefly acquire the lock,
    # grab a reference to the set of channels, and release the lock, knowing that the referenced set
    # is safe to use from the thread, even if another thread happens to replace it.
    _lock: threading.Lock
    _subscribed_channels: FrozenSet[MiddlewareChannelId]

    def __init__(self, blacklist=[], drop_lidars=False):
        super().__init__()
        self._stop_event = threading.Event()
        self._lock = threading.Lock()
        self._stopped = False
        self._subscribed_channels = frozenset()
        self.blacklist = blacklist
        self.drop_lidars = drop_lidars

        self.ch_types = {}

        cyber.init()

        self.node_name = "FoxGlove_bridge"
        self.node = cyber.Node(self.node_name)
        self.data = {}

        self.ids = 0
        self.scan_period = 10

        # types_cache = f'{os.path.dirname(__file__)}/cache_files/msg_types.json'
        # os.makedirs(os.path.dirname(types_cache), exist_ok=True)
        # self.channelTypeLoader = ChannelTypeLoader(types_cache)

        self.t = None
        self.scanning_active = True

        # original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        self.worker_pool = mp.Pool(processes=mp.cpu_count())
        # signal.signal(signal.SIGINT, original_sigint_handler)

        # self.workers_output_queue = mp.Queue()
        self.workers_output_queue = Queue()

    def handle_subscribe_threadsafe(self, chan: MiddlewareChannelId):
        """
        Handle an added subscription from a WebSocket client. This method is thread-safe because it
        uses a lock to access internal data structures. It will be called on the WebSocket server
        thread.
        """
        with self._lock:
            self._subscribed_channels = self._subscribed_channels | {chan}

    def handle_unsubscribe_threadsafe(self, chan: MiddlewareChannelId):
        """
        Handle a removed subscription from a WebSocket client. This method is thread-safe because it
        uses a lock to access internal data structures. It will be called on the WebSocket server
        thread.
        """
        with self._lock:
            self._subscribed_channels = self._subscribed_channels - {chan}

    def stop_threadsafe(self):
        """
        Inform the thread that it should finish any active work and stop running. This method is
        thread-safe because the threading.Event class is thread-safe.
        """
        self._stop_event.set()

    def scan_channels(self):

        for node in cyber.NodeUtils.get_nodes(sleep_s=0):

            # skip cyber_monitor nodes
            if 'MonitorReader' in node:
                continue

            for ch in cyber.NodeUtils.get_writersofnode(node, sleep_s=0):

                with self._lock:
                    if ch in self._subscribed_channels:
                        continue

                    if ch in self.blacklist:
                        logger.warning(f'{ch} is skipped, in blacklist')
                        continue

                    if self.drop_lidars and 'PointCloud' in ch:
                        logger.warning(f'{ch} is skipped, lidar data')
                        continue

                    ch_t = cyber.ChannelUtils.get_msgtype(
                        ch, sleep_s=0).decode('utf-8')

                    if ch_t not in channel_types_classes:
                        logger.warning(f'{ch_t} is not in classes list')
                        continue

                    if ch_t in cyber_foxglove_types_mapping:
                        target_class = cyber_foxglove_types_mapping[ch_t]
                    else:
                        target_class = channel_types_classes[ch_t]

                    self.node.create_rawdata_reader(
                        ch, self.raw_data_callback, (self.ids, ch_t))

                    self.on_add_channel(self.ids, {
                        "topic": ch,
                        "encoding": "protobuf",
                        "schemaName": target_class.DESCRIPTOR.full_name,
                        "schema": getFGSchema(target_class)
                    }
                    )
                    self.ids += 1
                    self._subscribed_channels = self._subscribed_channels | {
                        ch}

    def raw_data_callback(self, msg, args) -> None:
        ch_id, ch_t = args
        if ch_t == 'apollo.drivers.PointCloud':
            # If queue for messages is becoming big, we skip big pointcloud message
            if self.workers_output_queue.qsize() > 1000:
                logger.warning(
                    f'skipping pointcloud because of large queue size: {self.workers_output_queue.qsize()}')
                return
            apply_res = self.worker_pool.apply_async(
                convertPointCloudWrapped, (ch_id, time.time_ns(), msg))
            with self._lock:
                self.workers_output_queue.put(apply_res)
            return

        with self._lock:
            if ch_t == 'apollo.drivers.Image':
                self.workers_output_queue.put(
                    (ch_id, time.time_ns(), convertRawImage(msg)))
            elif ch_t == 'apollo.drivers.CompressedImage':
                self.workers_output_queue.put(
                    (ch_id, time.time_ns(), convertCompressedImage(msg)))
            elif ch_t == 'apollo.transform.TransformStampeds':
                msgs = convertTransforms(msg)
                [self.workers_output_queue.put(
                    (ch_id, time.time_ns(), msg)) for msg in msgs]
            else:
                self.workers_output_queue.put((ch_id, time.time_ns(), msg))

    def run_scanning(self) -> None:

        self.scan_channels()
        t = time.time()

        while self.scanning_active and cyber.ok():

            time.sleep(0.5)
            if time.time() - t < self.scan_period:
                continue

            t = time.time()
            self.scan_channels()

    def stop_scanning(self) -> None:
        self.scanning_active = False

    def run(self) -> None:
        """
        This function provides the main entry point which will be executed in a new thread. It
        periodically calls the on_add_channel, on_remove_channel, and on_message callbacks in the
        middleware thread, simulating an active pub/sub graph.
        """
        logger.info("Middleware thread started")

        scanning_thread = threading.Thread(target=self.run_scanning)
        scanning_thread.start()

        prev_len = 0

        prev_t = time.time()
        while cyber.ok():
            if self._stop_event.is_set():
                break

            queue_len = self.workers_output_queue.qsize()
            if queue_len == 0:
                continue

            if queue_len > prev_len and queue_len > 1000:
                prev_len = queue_len
                logger.warning(f"Queue is growing. Size: {queue_len}")

            msg = self.workers_output_queue.get()

            if isinstance(msg, AsyncResult):
                msg = msg.get()

            if msg[1] - prev_t < 0:
                diff = (prev_t - msg[1]) / 1e9
                logger.warning(
                    f'Received timestamp jump back in time for {diff:.3f} seconds')
            prev_t = msg[1]

            self.on_message(*msg)

        logger.info("Middleware thread finished")
        self.stop_scanning()
        self.worker_pool.terminate()
        cyber.shutdown()
