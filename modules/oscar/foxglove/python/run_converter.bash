#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


function resolve_relative_path() (
    # If the path is a directory, we just need to 'cd' into it and print the new path.
    if [ -d "$1" ]; then
        cd "$1" || return 1
        pwd
    # If the path points to anything else, like a file or FIFO
    elif [ -e "$1" ]; then
        # Strip '/file' from '/dir/file'
        # We only change the directory if the name doesn't match for the cases where
        # we were passed something like 'file' without './'
        if [ ! "${1%/*}" = "$1" ]; then
            cd "${1%/*}" || return 1
        fi
        # Strip all leading slashes upto the filename
        echo "$(pwd)/${1##*/}"
    else
        echo $@
    fi
)


! [ -f /.dockerenv ] && echo "Convertation must be used inside docker container" && exit

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )



######### convert relative paths to absolute

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
    cd $SCRIPT_DIR
    ./foxglove_server/convert_record_to_mcap.py --help
    exit 0
fi


# create dir for output files here to later expand it to absolute path.
mkdir $2

REPLACE_ARGS=()
i=0

for ARG in $@
do
    REPLACE_ARGS[i++]="$(resolve_relative_path "$ARG")"
done

############################################

cd $SCRIPT_DIR
# pipenv run python foxglove_server/convert_record_to_mcap.py ${REPLACE_ARGS[@]}
./foxglove_server/convert_record_to_mcap.py ${REPLACE_ARGS[@]}

