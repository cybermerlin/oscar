///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/logger/logger_accumulator.h"

#include <algorithm>

namespace apollo {
namespace cyber {
namespace logger {

void LoggerAccumulator::Write(bool force_flush, time_t timestamp,
                              const char* message, int message_len) {
  if (!is_alive_) return;
  for (auto& logger : loggers_)
    logger->Write(force_flush, timestamp, message, message_len);
}

void LoggerAccumulator::Flush() {
  if (!is_alive_) return;
  for (auto& logger : loggers_) logger->Flush();
}
uint32_t LoggerAccumulator::LogSize() {
  uint32_t sz = UINT32_MAX;
  for (auto& logger : loggers_) sz = std::min(sz, logger->LogSize());
  return sz;
}

}  // namespace logger
}  // namespace cyber
}  // namespace apollo
