set -e

ARENA_SDK_DIR=/apollo/modules/oscar/drivers/camera/lucid/ArenaSDK_Linux_x64

ERROR_MESSAGE="License of Arena SDK does not permit distribution.\n\
Arena SDK is availabe to download manually from https://thinklucid.com/arena-software-development-kit/\n
You should unpack the archive ArenaSDK_Linux_x64 to /apollo/modules/oscar/drivers/camera/lucid in order to use Lucid Camera."

if [ -d $ARENA_SDK_DIR ] ; then
  echo "Setup for camera" 
  cd $ARENA_SDK_DIR
  sudo bash ./Arena_SDK_Linux_x64.conf
else
  echo -e "$ERROR_MESSAGE"
fi
