///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/camera/lucid/lucid_camera.h"

#include <chrono>
#include <future>

#include "modules/oscar/drivers/camera/lucid/ArenaSystemWrapper.h"

using google::protobuf::FieldDescriptor;

#define SLEEP_TIME 2

void LucidCamera::shutdown() {
  pDevice_->StopStream();
  pDevice_->DeregisterImageCallback(image_callback_.get());
  pSystem_->DestroyDevice(pDevice_);

  pSystem_->DeregisterDeviceDisconnectCallback(
      device_disconnect_callback_.get());
}

bool LucidCamera::connectDevice() {
  if (!this->searchForDevices()) {
    return false;
  }

  pNodeMap_ = pDevice_->GetNodeMap();
  pTLStreamNodeMap_ = pDevice_->GetTLStreamNodeMap();

  param_setter_.reset(new ParameterSetter(pDevice_));

  GenApi::CFloatPtr pExposureTimeNode = pNodeMap_->GetNode("ExposureTime");
  get_image_timeout_ms_ =
      static_cast<int64_t>(pExposureTimeNode->GetMax() / 1000 * 2);

  acq_single_ = Arena::GetNodeValue<GenICam::gcstring>(
                    pNodeMap_, "AcquisitionMode") == "SingleFrame";

  if (acq_single_) {
    GenApi::CCommandPtr acquisitionStart =
        pNodeMap_->GetNode("AcquisitionStart");
    acquisitionStart->Execute();
  }

  return pDevice_->IsConnected();
}

bool LucidCamera::searchForDevices() {
  pSystem_->UpdateDevices(100);
  std::vector<Arena::DeviceInfo> deviceInfos = pSystem_->GetDevices();

  if (deviceInfos.size() == 0) {
    return false;
  }

  for (auto deviceInfo : deviceInfos) {
    // check if MAC address or IP matches with the desired one
    if (lucid_config_.has_mac()) {
      if (deviceInfo.MacAddressStr().c_str() != lucid_config_.mac()) {
        continue;
      }
    } else if (lucid_config_.has_ip()) {
      if (deviceInfo.IpAddressStr().c_str() != lucid_config_.ip()) {
        continue;
      }
    }

    if (pDevice_ != nullptr) pSystem_->DestroyDevice(pDevice_);
    pDevice_ = pSystem_->CreateDevice(deviceInfo);

    break;
  }
  return this->isConnected();
}

bool LucidCamera::init(
    const std::shared_ptr<Config>& config,
    std::function<void(const std::shared_ptr<Image>&)> callback_fn) {
  // inittialize system
  if (pSystem_ == nullptr) {
    pSystem_ = ArenaSystemWrapper::getSystem();
  }

  writer_callback_fn_ = callback_fn;
  lucid_config_.CopyFrom(config->lucid_config());

  if (!connectDevice()) {
    return false;
  }

  // By default OpenCV creates 16 threads to perform operations.
  // And doesn't kill them so the total number of threads is around 270.
  // the line below prevents creating additional threads. The performance
  // decreases in around 3x times
  // cv::setNumThreads(0);

  setupDevice();

  device_disconnect_callback_ =
      std::make_unique<DeviceDisconnectCallbackWrapper>([this]() {
        std::string info = lucid_config_.has_mac()
                               ? " MAC - " + lucid_config_.mac()
                               : " IP - " + lucid_config_.ip();

        std::cout << "Camera disconnected:" << info << " Reconnecting...\n";

        while (!connectDevice()) {
          sleep(SLEEP_TIME);
        }
        setupDevice();
        pDevice_->StartStream();

        std::cout << "Camera reconnected:" << info << "\n";
      });

  pSystem_->RegisterDeviceDisconnectCallback(pDevice_,
                                             device_disconnect_callback_.get());

  pDevice_->StartStream();

  return true;
}

void LucidCamera::setupDevice() {
  timestamp_offset_ = (std::chrono::duration_cast<std::chrono::nanoseconds>(
                           std::chrono::system_clock::now().time_since_epoch())
                           .count() -
                       static_cast<double>(Arena::GetNodeValue<int64_t>(
                           pNodeMap_, "Timestamp"))) /
                      1'000'000'000UL;

  start_camera_time_ = static_cast<double>(Arena::GetNodeValue<int64_t>(
                           pNodeMap_, "Timestamp")) /
                       1'000'000'000UL;

  setSettings(&lucid_config_);

  image_callback_ = std::make_unique<ImageCallbackWrapper>(
      std::bind(&LucidCamera::onImage, this, std::placeholders::_1));

  pDevice_->RegisterImageCallback(image_callback_.get());
}

void LucidCamera::onImage(Arena::IImage* pImage) {
  double measurement_time;
  if (use_receive_time_) {
    measurement_time = current_time();
  } else {
    measurement_time =
        timestamp_offset_ + (static_cast<double>(pImage->GetTimestamp()) / 1e9);
  }

  if (pImage->IsIncomplete()) {
    pDevice_->RequeueBuffer(pImage);
    throw std::runtime_error("Image is corrupted");
  }

  // Setting RGB pixel format inside Camera does not work so
  // a workaround is to obtain an image in BayerRG format and
  // convert to RGB/BGR
  // Also width and height settings inside camera just crop upper-left box.
  // So, OpenCV is used for these purposes.

  cv::Mat cv_img((int)pImage->GetHeight(), (int)pImage->GetWidth(), CV_8UC1,
                 (void*)pImage->GetData());

  processImage(cv_img, &cv_img, target_width_, target_height_,
               desired_average_channel_intensity_);

  if (driver_gain_ != 0) {
    cv_img *= driver_gain_;
  }

  auto imagePtr = std::make_shared<apollo::drivers::Image>();

  imagePtr->set_data(cv_img.data, cv_img.total() * cv_img.elemSize());
  imagePtr->set_width(cv_img.cols);
  imagePtr->set_height(cv_img.rows);
  imagePtr->set_measurement_time(measurement_time);
  imagePtr->mutable_header()->set_timestamp_sec(measurement_time);
  imagePtr->set_encoding("rgb8");

  writer_callback_fn_(imagePtr);
}

bool LucidCamera::capture(std::shared_ptr<Image> imagePtr) {
  // check if device is active
  if (pDevice_ == nullptr) {
    throw std::runtime_error("device not initialized");
  } else if (!pDevice_->IsConnected()) {
    throw std::runtime_error("device not connected");
  }

  if (acq_single_) {
    GenApi::CCommandPtr acquisitionStart =
        pNodeMap_->GetNode("AcquisitionStart");

    run_with_timeout(
        std::bind(&GenApi::ICommand::Execute, acquisitionStart, false),
        std::chrono::seconds(1));
  }

  // default function pDevice->GetImage gets stuck when the internet
  // connection is lost. And Arena SDK is propriate without chance for fixing
  // this behavior. So we use a wrapper function that will always return after
  // timeout.

  Arena::IImage* pImage = pDevice_->GetImage(get_image_timeout_ms_);
  onImage(pImage);
  pDevice_->RequeueBuffer(pImage);

  return true;
}

bool LucidCamera::isConnected() {
  return pDevice_ != nullptr ? pDevice_->IsConnected() : false;
}

void LucidCamera::setSettings(const LucidConfig* in_config) {
  std::cout << std::boolalpha;
  auto lucid_settings = in_config->settings();

  // Gain in camera is limited to value 42 (why?). The value below is an
  // additional gain.
  driver_gain_ = in_config->driver_gain();

  desired_average_channel_intensity_ = in_config->desired_channel_average();
  // Set pixel format does not work so conversion is being done on the
  // obtained image.
  target_pixel_format_ = lucid_settings.pixel_format();
  // a workaround to perform resizing in a driver side. We set maximum
  // resolution on image and resize it in capture() function.
  target_width_ = lucid_settings.width();
  target_height_ = lucid_settings.height();

  lucid_settings.set_width(2880);
  lucid_settings.set_height(1860);

  std::cout << "Uploading settings to camera:\n";
  std::cout << "[Setting name]: Current value -> New value\n\n";

  std::cout << "Resetting camera to default settings\n";
  param_setter_->setValue("UserSetSelector", "Default");
  param_setter_->execute("UserSetLoad");

  if (lucid_settings.has_exposure_time()) {
    if (lucid_settings.exposure_auto() == "Continuous") {
      std::cout << "exposure_auto changed from Continuous to Off due to "
                   "presence of exposure_time\n";
      lucid_settings.set_exposure_auto("Off");
    }
    param_setter_->setValue("ExposureAuto", "Off");
  }
  if (lucid_settings.has_gain()) {
    if (lucid_settings.gain_auto() == "Continuous") {
      std::cout << "gain_auto changed from 'Continuous' to 'Off' due to "
                   "presence of gain\n";
      lucid_settings.set_gain_auto("Off");
    }
    param_setter_->setValue("GainAuto", "Off");
  }

  // setup some default utility settings
  param_setter_->setValue("AcquisitionFrameRateEnable", true);
  param_setter_->setValue("StreamPacketResendEnable", true);
  param_setter_->setValue("StreamBufferHandlingMode", "NewestOnly");
  param_setter_->setValue("StreamAutoNegotiatePacketSize", true);
  param_setter_->setValue("StreamMaxNumResendRequestsPerImage", 5);
  param_setter_->setValue("DeviceStreamChannelPacketSize", 9000);

  // Stupid camera sometimes set amplification factor by zero, making
  // completely black image. Turning off White balance feature to prevent
  // this.
  param_setter_->setValue("BalanceWhiteEnable", false);

  std::cout << "\n";

  auto descriptor = lucid_settings.descriptor();
  auto config_reflection = lucid_settings.GetReflection();

  for (int i = 0, total = descriptor->field_count(); i < total; i++) {
    auto field_descriptor = descriptor->field(i);

    if (!config_reflection->HasField(lucid_settings, field_descriptor)) {
      continue;
    }

    std::string name = field_descriptor->camelcase_name();
    name[0] = std::toupper(name[0]);

    switch (field_descriptor->type()) {
      case FieldDescriptor::TYPE_UINT32: {
        int value =
            config_reflection->GetUInt32(lucid_settings, field_descriptor);
        param_setter_->setValue(name, value);
        break;
      }
      case FieldDescriptor::TYPE_INT32:
      case FieldDescriptor::TYPE_UINT64:
      case FieldDescriptor::TYPE_INT64: {
        int value =
            config_reflection->GetInt32(lucid_settings, field_descriptor);
        param_setter_->setValue(name, value);
        break;
      }
      case FieldDescriptor::TYPE_DOUBLE:
      case FieldDescriptor::TYPE_FLOAT:
      case FieldDescriptor::TYPE_FIXED32:
      case FieldDescriptor::TYPE_FIXED64: {
        double value =
            config_reflection->GetFloat(lucid_settings, field_descriptor);
        param_setter_->setValue(name, value);
        break;
      }
      case FieldDescriptor::TYPE_STRING: {
        auto value =
            config_reflection->GetString(lucid_settings, field_descriptor);
        param_setter_->setValue(name, value);

        break;
      }
      case FieldDescriptor::TYPE_BOOL: {
        bool value =
            config_reflection->GetBool(lucid_settings, field_descriptor);
        param_setter_->setValue(name, value);
        break;
      }
      default:
        break;
    }
  }
  return;
}
