#!/usr/bin/env bash

#############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

CARLA_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

BRIDGE_PATH="${CARLA_PATH}/carla_bridge"

VENV_PATH="${CARLA_PATH}/carla_venv"


# ------------------------------------------------------------------------ #

set -e

if [ ! -d ${BRIDGE_PATH} ]; then
  git clone https://gitlab.com/starline/oscar_utils/carla/carla_apollo_bridge.git ${BRIDGE_PATH}
  ln -s ${BRIDGE_PATH}/carla_bridge/map/* /apollo/modules/map/data/
fi


if [ ! -d ${VENV_PATH} ]; then

    python3.8 -m venv ${VENV_PATH}

    source ${VENV_PATH}/bin/activate

    # worklog from OSCAR-1042 | 28.04.2023
    pip install protobuf==3.20.0

    # for carla and opencv
    pip install --upgrade pip==23.3.1

    # for pyyaml
    pip install wheel==0.42.0

    pip install -r ${BRIDGE_PATH}/carla_bridge/requirements.txt

    pip install ${BRIDGE_PATH}/carla_bridge/carla_api/carla-0.9.14-cp38-cp38-manylinux_2_27_x86_64.whl

    # pip install -e ${BRIDGE_PATH}

fi
