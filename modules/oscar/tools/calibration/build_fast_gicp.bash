#!/usr/bin/env bash

set -e

function build_gicp()
{
    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -Wno-dev
    make -j$(nproc)
}

pip3 install pybind11[global]

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd ${SCRIPT_DIR}/third_party/fast_gicp

if [ -d "build" ]
then
    echo "Directory build already exists. Removing and rebuilding"
    rm -rf build
    build_gicp
else
    build_gicp
fi
