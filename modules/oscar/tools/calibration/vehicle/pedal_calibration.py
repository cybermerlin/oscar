#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


import argparse
import os
import sys
import time
import datetime

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from tqdm import tqdm

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.sensor_msgs import imu_pb2


import signal
import faulthandler

faulthandler.enable()

np.set_printoptions(precision=3)
plt.ion()


class Calib:

    def __init__(self, sim=False, vis=True):

        self.vis = vis

        signal.signal(signal.SIGINT, self.signal_handler)

        cyber.init()
        self.node = cyber.Node("data_collector")

        self.node.create_reader('/apollo/canbus/chassis', chassis_pb2.Chassis,
                                self.callback_canbus)

        if sim == True:
            self.node.create_reader('/apollo/sensor/gnss/imu', imu_pb2.Imu,
                                    self.callback_imu)
        else:
            self.node.create_reader('/apollo/localization/pose',
                                    localization_pb2.LocalizationEstimate,
                                    self.callback_localization)

        self.control_pub = self.node.create_writer(
            '/apollo/control', control_cmd_pb2.ControlCommand)
        self.controlcmd = control_cmd_pb2.ControlCommand()

        self.scatter_plot = None
        self.data = np.empty((0, 3))

        self.vehicle_speed = 0
        self.engine_rpm = 0
        self.throttle_percentage = 0
        self.brake_percentage = 0
        self.gear_location = 0
        self.driving_mode = 0

        self.canmsg_received = 0

        self.sequence_num = 0
        self.is_moving = False
        self.is_accelerating = True
        self.canmsg_received = False
        self.localization_received = False
        self.save_data = np.empty((0, 10))
        self.MAX_ACCEL = 100
        self.outer_bar = None

        self.step = 1
        self.script_dir = os.path.dirname(os.path.realpath(__file__))
        self.save_fname = f'{self.script_dir}/collected_data/{datetime.datetime.now().strftime("%d.%m.%Y/%H:%M")}.csv'

        os.makedirs('/'.join(self.save_fname.split('/')[:-1]), exist_ok=True)

        if self.vis:
            self.figure = plt.figure(figsize=(8, 10.8))
            self.ax = plt.axes(projection='3d')
            self.initialize_plots()

        print(f'saving data fo file: {self.save_fname}')

    def initialize_plots(self):
        print('Got first point, initializing figure')
        self.scatter_plot = self.ax.scatter3D([], [], [])
        self.ax.set_xlabel('Cmd')
        self.ax.set_ylabel('Speed')
        self.ax.set_zlabel('Accel')
        self.figure.canvas.draw()
        print('Figure init complete')

    def update_plot(self):

        if not self.vis:
            return

        if len(self.save_data) == 0:
            return
        plot_data = np.stack([self.save_data[:, 1] - self.save_data[:, 0],
                             self.save_data[:, 3], self.save_data[:, 9]])

        self.ax.set_xlim((plot_data[0].min() - 1, plot_data[0].max() + 1))
        self.ax.set_ylim((plot_data[1].min() - 1, plot_data[1].max() + 1))
        self.ax.set_zlim((plot_data[2].min() - 1, plot_data[2].max() + 1))

        self.scatter_plot._offsets3d = plot_data

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def signal_handler(self, signal, frame):
        self.is_moving = False
        print(signal, frame)
        cyber.shutdown()

    def spin(self):

        print('Waiting for quit')

        while True:
            self.figure.canvas.flush_events()

    def append_data(self, *data):
        self.data = np.vstack([self.data, np.array(data)])

    def proc(self, accel_cmd, speed_lim, deccel_cmd, static_cmd=True):

        self.accel_cmd = accel_cmd
        self.speed_lim = speed_lim
        self.deccel_cmd = deccel_cmd

        self.append_data(self.accel_cmd, self.speed_lim, self.deccel_cmd)

        # print('Send Reset Command.')
        self.controlcmd = control_cmd_pb2.ControlCommand()
        self.controlcmd.header.module_name = "control"
        self.controlcmd.header.sequence_num = self.sequence_num
        self.sequence_num = self.sequence_num + 1
        self.controlcmd.header.timestamp_sec = cyber_time.Time.now().to_sec()
        self.controlcmd.pad_msg.action = 2
        self.control_pub.write(self.controlcmd)

        time.sleep(0.2)
        # Set Default Message
        # print('Send Default Command.')
        self.controlcmd.pad_msg.action = 1
        self.controlcmd.throttle = 0
        self.controlcmd.brake = 0
        self.controlcmd.steering_rate = 100
        self.controlcmd.steering_target = 0
        self.controlcmd.gear_location = chassis_pb2.Chassis.GEAR_DRIVE

        self.canmsg_received = False
        time.sleep(0.5)

        self.is_moving = True

        while self.is_moving and cyber.ok():
            now = cyber_time.Time.now().to_sec()
            self.publish_control(static_cmd=static_cmd)
            sleep_time = 0.05 - (cyber_time.Time.now().to_sec() - now)

            if sleep_time > 0:
                time.sleep(sleep_time)
            if not cyber.ok():
                self.save_data_to_file()
                raise KeyboardInterrupt

        self.save_data_to_file()

    def save_data_to_file(self):
        np.savetxt(self.save_fname,
                   self.save_data,
                   fmt='%.3f',
                   delimiter=',',
                   header=','.join(['brake', 'throttle', 'gear_location', 'vehicle_speed', 'engine_rpm', 'driving_mode', 'throttle_percentage', 'brake_percentage', 'gear_locartion', 'acceleration']))

    def publish_control(self, static_cmd=True):
        """
        New Control Command
        """
        if not self.canmsg_received:
            # print('No CAN Message Yet')
            # exit()
            self.update_plot()
            return
        else:
            self.canmsg_received = False

        self.controlcmd.header.sequence_num = self.sequence_num
        self.sequence_num += 1

        if static_cmd:
            cmd = self.accel_cmd if self.is_accelerating else -self.deccel_cmd
            msg = f'Speed: {self.vehicle_speed:.2f}/{self.speed_lim}, Command: {cmd}'
            if self.is_accelerating:
                if self.accel_cmd > 0:
                    self.controlcmd.throttle = self.accel_cmd
                    self.controlcmd.brake = 0
                else:
                    self.controlcmd.throttle = 0
                    self.controlcmd.brake = -self.accel_cmd
                if self.vehicle_speed >= self.speed_lim:
                    self.is_accelerating = False

            else:

                if self.deccel_cmd > 0:
                    self.controlcmd.brake = self.deccel_cmd
                    self.controlcmd.throttle = 0
                else:
                    self.controlcmd.brake = 0
                    self.controlcmd.throttle = -self.deccel_cmd
        else:
            self.inner_accel_step = 4
            if self.is_accelerating:
                if self.vehicle_speed >= self.speed_lim:
                    self.is_accelerating = False
                if self.accel_cmd <= self.MAX_ACCEL - self.step:
                    self.accel_cmd += self.inner_accel_step
            else:
                if self.accel_cmd >= -(self.MAX_ACCEL - self.step):
                    self.accel_cmd -= self.inner_accel_step

            msg = f'Speed: {self.vehicle_speed:.2f}/{self.speed_lim:.2f}, Command: {self.accel_cmd:.2f}'
            if self.accel_cmd > 0:
                self.controlcmd.throttle = self.accel_cmd
                self.controlcmd.brake = 0
            else:
                self.controlcmd.throttle = 0
                self.controlcmd.brake = -self.accel_cmd

        if self.outer_bar is not None:
            self.outer_bar.set_description(msg)
        else:
            print(msg)

        self.controlcmd.header.timestamp_sec = cyber_time.Time.now().to_sec()

        self.control_pub.write(self.controlcmd)

        cur_data = np.array([
            self.controlcmd.brake,
            self.controlcmd.throttle,
            self.controlcmd.gear_location,
            self.vehicle_speed,
            self.engine_rpm,
            self.driving_mode,
            self.throttle_percentage,
            self.brake_percentage,
            self.gear_location,
            self.acceleration])

        self.save_data = np.vstack([self.save_data, cur_data[np.newaxis, ...]])

        self.update_plot()

        if (np.round(self.vehicle_speed, decimals=4) == 0) and (not self.is_accelerating):
            self.is_moving = False
            self.is_accelerating = True

    def callback_localization(self, data):
        """
        New Localization
        """
        self.acceleration = data.pose.linear_acceleration_vrf.y
        self.localization_received = True

    def callback_imu(self, data):
        """
        New imu (used only in svl simulator)
        """
        self.acceleration = data.linear_acceleration.x
        self.localization_received = True

    def callback_canbus(self, data):
        """
        New CANBUS
        """
        if not self.localization_received:
            print('No Localization Message Yet')
            return

        self.vehicle_speed = data.speed_mps
        self.engine_rpm = data.engine_rpm
        self.throttle_percentage = data.throttle_percentage
        self.brake_percentage = data.brake_percentage
        self.gear_location = data.gear_location
        self.driving_mode = data.driving_mode

        self.canmsg_received = True

    def collect_data(self, min_accel, max_accel, speed_lim, accel_step, static=True):
        self.MAX_ACCEL = max_accel
        self.step = accel_step

        if not static:
            self.outer_bar = tqdm(
                np.arange(min_accel, max_accel + 0.1, step=accel_step), position=0)

            for i in self.outer_bar:
                # print((max_accel, i), max_accel)
                self.proc(i, speed_lim * (max_accel - i + min_accel) /
                          max_accel, i, static_cmd=False)
                self.MAX_ACCEL -= accel_step
        else:
            self.outer_bar = tqdm(
                np.arange(min_accel, max_accel, step=accel_step)[::-1])
            for i in self.outer_bar:
                self.proc(i, speed_lim, i)


def check_if_max_accel_is_high(max_accel, thresh=50):
    if max_accel > thresh:
        print(
            f"Max acceleration: {max_accel} is above threshold: {thresh}. Are you sure you want to continue? ('y' or 'n')")
        inp = input()
        if inp == 'y':
            return
        else:
            print("Exiting program")
            exit()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--sim', action='store_true',
                        help='Use SVL simulation instead of real car. In Simulation acceleration is obtained from /Imu channel, but in real car - from localization/pose channel')

    args = parser.parse_args()
    print(args)

    calib = Calib(sim=args.sim)

    info = """
Commands:
1. q - quit
2. mode [params]
  - l {min_accel} {max_accel} {speed_lim} {accel_step} - l (linear) - change command linearly
    - min_accel - minimum acceleration cmd val, %
    - max_accel - maximum acceleration cmd val, % (max decceleration is the same)
    - speed_lim - speed limit, m/s
    - accel_step - step to send different acceleration values
  - s {min_accel} {max_accel} {speed_lim} {accel_step} - s (static) - send static commands
    - params are same as above
  - m {accel} {speed_lim} {deccel} - (manual) - manually choose static commands
    - accel - acceleration to be applied, %
    - speed_lim - speed limit, m/s
    - deccel - decceleration to be applied, %
    
    Example:
    'l 5 100 20 4'
    's 10 100 10 10'
    'm 100 20 100'
3. h - show this help

Enter command:"""
    print(info)

    try:
        while cyber.ok():
            cmd = input('Waiting command:')
            cmd = 's 5 100 20 10'

            if cmd == 'q':
                cyber.shutdown()
                break
            elif cmd == 'h':
                print(info)
                continue
            mode = cmd.split(' ')[0]

            if mode == 'l':
                print('Going into linear cmd value')
                min_accel, max_accel, speed_lim, accel_step = list(
                    map(float, cmd.split(' ')[1:]))
                check_if_max_accel_is_high(max_accel)
                calib.collect_data(min_accel, max_accel,
                                   speed_lim, accel_step, static=False)
            elif mode == 's':
                min_accel, max_accel, speed_lim, accel_step = list(
                    map(float, cmd.split(' ')[1:]))
                check_if_max_accel_is_high(max_accel)
                print('Going into static cmd value')
                calib.collect_data(min_accel, max_accel,
                                   speed_lim, accel_step, static=True)
            elif mode == 'm':
                print('Go into manual mode')
                accel, speed_lim, deccel = list(map(float, cmd.split(' ')[1:]))
                check_if_max_accel_is_high(accel)
                calib.proc(accel, speed_lim, deccel)
            else:
                print('Incorrect mode: ', mode)
                print("type 'h' to show help")
    except KeyboardInterrupt:
        print(f"Saving results to file: {calib.save_fname}")
        calib.save_data_to_file()
    finally:
        cyber.shutdown()
