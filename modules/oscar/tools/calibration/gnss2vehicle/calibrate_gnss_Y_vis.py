#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from math import degrees
import os
import sys
import time

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")
import numpy as np
import matplotlib

from matplotlib import pyplot as plt
from matplotlib.patches import Circle, Rectangle
from scipy.spatial.transform import Rotation
import yaml
import cv2 as cv

from cyber.python.cyber_py3 import cyber, record
from modules.common_msgs.localization_msgs import localization_pb2

from gnss2vehicle.utils import AngleDiffConverter, get_kde, EMA

np.set_printoptions(precision=3)
matplotlib.use("tkagg")
plt.ion()


COLORS_SET = np.random.uniform(0, 1, size=(50, 3))
MAX_POINTS = 2000000
def dist(p1, p2): return np.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)


class Calibrator:
    def __init__(self, rfolder=None, visualize=True, heading_offset_yaml=None, x_offset=0, z_offset=0):
        cyber.init()
        self.node = cyber.Node("calibrator")

        self.visualize = visualize

        self.rfolder = rfolder

        self.input_record = rfolder is not None

        if self.input_record:
            self.record_gen = self.read_record_msg()
        else:
            self.node.create_reader(
                "/apollo/localization/pose", localization_pb2.LocalizationEstimate, self.localization_reader_callback)

        self.poses = np.empty((0, 3), dtype=np.float32)  # x, y, heading angle

        self.figure, self.ax = plt.subplots()
        self.ax.set_aspect('equal', 'box')
        self.ax.grid()

        self.scatter_plot = None
        self.circles = []

        self.last_time = None
        self.message_received = False

        self.x_offset = x_offset
        self.z_offset = z_offset

        self.time_thresh = 0.1
        self.dist_thresh = 0.2

        self.car_width = 2.5
        self.car_length = 6.3

        self.angle_converter = AngleDiffConverter()

        self.radius_ema = EMA(init_val=0, alpha=0.5)

        if heading_offset_yaml is None:
            self.heading_offset_degrees = 0
        else:
            print(f'Got heading offset file: {heading_offset_yaml}')
            with open(heading_offset_yaml, 'r') as f:
                h = yaml.safe_load(f)
            self.heading_offset_degrees = Rotation.from_quat(
                [h['x'], h['y'], h['z'], h['w']]).as_euler('xyz', degrees=True)[-1]
            print(
                f'Heading correction: {self.heading_offset_degrees:.3f} degrees')
        self.last_pose = None

        self.last_circle_c = None

        self.calc_dists = []

        print('Init complete')

    def initialize_plots(self):
        print('Got first point, initializing figure')
        self.scatter_plot = self.ax.scatter(*self.poses[-1, :-1], s=1)

        self.figure.canvas.draw()
        print('Figure init complete')

    def add_point(self, last=False):

        self.poses = np.vstack([self.poses, self.last_pose])

        if len(self.poses) == 1 and self.visualize:
            self.initialize_plots()
        if len(self.poses) == 2:
            self.poses[0] = self.poses[1]

        points = self.poses[:, :-1]

        self.angle_converter.update(self.poses[-1, -1])
        # print(self.angle_converter.angles[-1] * 180 / np.pi)
        self.ax.set_xlim((points[:, 0].min() - 1, points[:, 0].max() + 1))
        self.ax.set_ylim((points[:, 1].min() - 1, points[:, 1].max() + 1))

        dense_diff_val = get_kde(self.angle_converter.angle_diffs[:, 0])
        inliers = abs(self.angle_converter.angle_diffs - dense_diff_val) < 0.1
        colors = [(0, 1, 0) if i else (1, 0, 0) for i in inliers]
        colors[-1] = (0, 0, 1)

        inliers = np.array(inliers)[:, 0]
        # print(points.shape, self.angle_converter.angles.shape)
        if self.angle_converter.periods.shape[0] > 1:
            if self.angle_converter.periods[-1] > self.angle_converter.periods[-2]:
                # self.circles = self.add_circles(points[inliers], period_labels=self.angle_converter.periods[inliers][:,0])
                self.circles = self.add_circles(
                    points, period_labels=self.angle_converter.periods[:, 0])
        if last:
            # self.circles = self.add_circles(points[inliers], period_labels=self.angle_converter.periods[inliers][:,0])
            self.circles = self.add_circles(
                points, period_labels=self.angle_converter.periods[:, 0])

        if self.visualize:
            self.draw_patches(self.circles + [self.add_car()])

            self.scatter_plot.set_offsets(points[-MAX_POINTS:])
            self.scatter_plot.set_facecolors(colors)

            self.figure.canvas.draw()
            self.figure.canvas.flush_events()

    def add_circles(self, points, period_labels=None):
        period_labels = period_labels if period_labels is not None else np.ones(
            (len(points)))

        circles = []
        rads = []

        # points = points[(points[:, 0] < np.quantile(points[:, 0], 0.95)) & (points[:, 0] > np.quantile(points[:, 0], 0.05))]

        for idx, period in enumerate(set(period_labels.tolist())):

            if sum(period_labels == period) <= 5:
                continue
            cur_points = points[period_labels == period]

            # print(len(cur_points))

            # find circle

            # print(cur_points.shape)
            (cX, cY), (w, h), _ = cv.fitEllipse(cur_points.astype(np.float32))
            radius = (w + h) / 4
            rads.append(radius)
            circles.append(
                Circle((cX, cY), radius, fill=False, color=COLORS_SET[idx]))

        # if len(points) <= 5:
        #     return
        # #find circle
        # (cX, cY), (w, h), _ = cv.fitEllipse(points.astype(np.float32))
        # radius = (w + h) / 4
        # rads.append(radius)
        # circles.append(Circle((cX, cY),radius, fill=False))

        rads = np.array(rads)
        print(
            f'after {len(set(period_labels[:-1]))} circles radius -  mean: {rads.mean():.3f}, std: {rads.std():.3f}')

        # find angle between circle radius and heading
        self.last_circle_c = (cX, cY)
        rad_angle = np.arctan2(self.poses[-1, 1] - cY, self.poses[-1, 0] - cX)
        # rad_angle = np.arctan2(self.poses[-2, 1] - cY, self.poses[-2, 0] - cX)

        # print(rad_angle)
        heading_angle = self.angle_converter.angles[-1, 0]

        # use all points from last circle
        points_idxs = period_labels == np.max(period_labels) - 1

        rad_angles = np.arctan2(
            points[points_idxs, 1] - cY, points[points_idxs, 0] - cX)

        heading_angles = self.angle_converter.angles[points_idxs, 0]
        angle_diffs = abs(heading_angles - rad_angles)
        angle_diffs[angle_diffs > 2 * np.pi] -= 2 * np.pi
        dists = rads[-1] * np.cos(angle_diffs)
        print(
            f'dists with all points: mean - {dists.mean():.3f}, std - {dists.std():.3f}')

        self.calc_dists.append(np.median(dists))

        print(
            f'Calculated length by heading and radius angles - mean: {np.mean(self.calc_dists):.3f}, std: {np.std(self.calc_dists):.3f}')

        shift = np.mean(self.calc_dists)
        shift = self.calc_dists[-1]

        offset = np.array([shift * np.cos(heading_angle),
                          shift * np.sin(heading_angle)])
        p = np.array(self.last_pose[:-1]) - offset
        rad_angle = np.arctan2(p[1] - cY, p[0] - cX)

        return circles

    def add_car(self):
        pos, angle = self.poses[-1, :-1].reshape((2, 1)), self.poses[-1, -1]

        transform = np.array([
            [np.cos(angle), -np.sin(angle)],
            [np.sin(angle), np.cos(angle)]
        ])
        init_offset = 3
        to_center = np.array(
            [5.25 - init_offset, +self.car_width / 2]).reshape((2, 1))

        center_pos = pos + transform @ to_center
        return Rectangle(center_pos, self.car_length, self.car_width, angle * 180 / np.pi + 180, fill=False)

    def draw_patches(self, patches):
        self.ax.patches.clear()
        [self.ax.add_patch(p) for p in patches if p is not None]

    def localization_reader_callback(self, msg):
        cur_time = msg.measurement_time
        pose = [msg.pose.position.x, msg.pose.position.y, msg.pose.heading]

        if not self.check_msg(pose, cur_time):
            return

        self.last_pose = pose
        self.message_received = True

    def read_record_msg(self):
        for idx, f in enumerate(sorted(os.listdir(self.rfolder))):
            rfile = self.rfolder + '/' + f
            freader = record.RecordReader(rfile)

            # st_time = time.time()

            for channelname, msg, datatype, timestamp in freader.read_messages():
                if channelname != '/apollo/localization/pose':
                    continue

                msg_struct = localization_pb2.LocalizationEstimate()
                msg_struct.ParseFromString(msg)

                pose = [msg_struct.pose.position.x, msg_struct.pose.position.y,
                        msg_struct.pose.heading + self.heading_offset_degrees * np.pi / 180]
                if not self.check_msg(pose, msg_struct.measurement_time):
                    continue

                # dur = time.time() - st_time
                st_time = time.time()
                # print(dur)
                self.last_pose = pose
                yield

    def check_msg(self, pose, cur_time):
        if self.last_time is None:
            self.last_time = cur_time
            return True
        elif (cur_time - self.last_time < self.time_thresh) or (dist(self.last_pose[:-1], pose[:-1]) < self.dist_thresh):
            return False

        return True

    def spin(self):
        while not cyber.is_shutdown():
            if self.input_record:
                next(self.record_gen)
            elif not self.message_received:
                continue
            else:
                self.message_received = False
            self.add_point()

    def save_to_file(self, save_file):
        print(self.calc_dists)
        offset = np.median(self.calc_dists[1:-1])
        offset = np.median(self.calc_dists)

        print(
            f'Calculated offset: median: {offset:.3f}, std: {np.std(self.calc_dists):.3f}')

        # save_file = 'res_transform.yaml'
        print(f'Saving results to file {save_file}')

        rot_q = Rotation.from_euler(
            'xyz', [0, 0, self.heading_offset_degrees], degrees=True).as_quat().tolist()
        res = {
            'translation':
                {'x': self.x_offset,
                 'y': float(np.round(offset, decimals=3)),
                 'z': self.z_offset},
            'rotation':
                {'x': rot_q[0],
                 'y': rot_q[1],
                 'z': rot_q[2],
                 'w': rot_q[3]}
        }
        print(res)
        with open(save_file, 'w') as f:
            yaml.dump(res, f, default_flow_style=False)


def main(path, off_save_file, x_off, z_off, save_fname):
    calib = Calibrator(rfolder=path,
                       visualize=True,
                       heading_offset_yaml=off_save_file,
                       x_offset=x_off,
                       z_offset=z_off)
    try:
        calib.spin()
    except StopIteration:
        calib.add_point(last=True)
        calib.save_to_file(save_fname)
        print("End of record file")
        if calib.visualize:
            while not cyber.is_shutdown():
                continue


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default='/apollo/data/bag/calib_10_circles/',
                        help='path to source folder with .record files')

    parser.add_argument('-o', '--offset', type=str, default='zero_quat.yaml',
                        help='save file name')

    parser.add_argument('-x', '--x_off', type=float, default=0,
                        help='x offset (cannot be calibrated')

    parser.add_argument('-z', '--z_off', type=float, default=1.85,
                        help='z offset (cannot be calibrated')

    parser.add_argument('-f', '--fname', type=str, default=f'{sys.path[0]}/res_transform.yaml',
                        help='name of a file to save extrinsics')

    args = parser.parse_args()

    main(args.path, args.offset, args.x_off, args.z_off, args.fname)
