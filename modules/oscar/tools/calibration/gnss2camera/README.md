# Калибровка GNSS -> Camera

## calibrate_camera_manual

Утилита для ручной первоначальной калибровки трансформации от gnss до камеры на основе проецирования карты на плоскость изображения. Для калибровки необходимо сопоставить изображение карты с линиями дороги. Также отображается линия горизонта.

Параметры калибровки указываются в конфиг файле ```cfg_calibrate.yaml```. Утилита читает канал изображения (требуется либо включенный драйвер камеры, либо воспроизведение записанного файла).

Модификация трансформации осуществляется клавишами клавиатуры при активном окне изображения. После  сопоставления при нажатии клавиши ```m``` (а также при закрытии утилиты) будет выведена текущая трансформация. 

##### Назначение клавиш
```
q   w   e
a   s   d
w/s - pitch angle
a/d - yaw angle
q/e - roll angle

u   i   o
j   k   l
i/k - forward/backward (Y axis)
j/l - left/right (X axis)
u/o - up/down (Z axis)

-/+ - divide/multiply step value by 2

f - stop/resume capturing messages
m - show current matrix and translation, quaternion
h - show help
Esc - quit
```


![](docs/calibration.png)

