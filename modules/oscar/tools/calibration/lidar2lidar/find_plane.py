#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys


from scipy.spatial.transform import Rotation
import open3d as o3d
import numpy as np
from tqdm.auto import tqdm
import matplotlib
from matplotlib import pyplot as plt

from common.common import load_transform_from_yaml

from pypatchworkpp import Parameters, patchworkpp

plt.rcParams['axes.grid'] = True
np.set_printoptions(suppress=True, precision=3)


# vis = o3d.visualization.VisualizerWithKeyCallback()
# vis.create_window(width=600, height=400)
# vis.run()

params = Parameters()
# params.verbose = True
params.min_range = 1
# params.uprightness_thr = 0.1
PatchworkPLUSPLUS = patchworkpp(params)


def find_plane(pc: np.ndarray):

    PatchworkPLUSPLUS.estimateGround(pc)

    ground = PatchworkPLUSPLUS.getGround()
    nonground = PatchworkPLUSPLUS.getNonground()
    # time_taken = PatchworkPLUSPLUS.getTimeTaken()

    # Get centers and normals for patches
    centers = PatchworkPLUSPLUS.getCenters()
    normals = PatchworkPLUSPLUS.getNormals()

    height = PatchworkPLUSPLUS.getHeight()

    # print("Origianl Points  #: ", pc.shape[0])
    # print("Ground Points    #: ", ground.shape[0])
    # print("Nonground Points #: ", nonground.shape[0])
    # print("Height #: ", height)
    # print("Time Taken : ", time_taken / 1000000, "(sec)")

    # mesh = o3d.geometry.TriangleMesh.create_coordinate_frame()

    # vis.clear_geometries()

    if len(ground) > 0:
        ground_o3d = o3d.geometry.PointCloud()
        ground_o3d.points = o3d.utility.Vector3dVector(ground)
        ground_o3d.colors = o3d.utility.Vector3dVector(
            np.array([[0.0, 1.0, 0.0]
                      for _ in range(ground.shape[0])], dtype=float)  # RGB
        )
        # vis.add_geometry(ground_o3d)

    nonground_o3d = o3d.geometry.PointCloud()
    nonground_o3d.points = o3d.utility.Vector3dVector(nonground)
    nonground_o3d.colors = o3d.utility.Vector3dVector(
        np.array([[1.0, 0.0, 0.0]
                 for _ in range(nonground.shape[0])], dtype=float)  # RGB
    )

    centers_o3d = o3d.geometry.PointCloud()
    centers_o3d.points = o3d.utility.Vector3dVector(centers)
    centers_o3d.normals = o3d.utility.Vector3dVector(normals)
    centers_o3d.colors = o3d.utility.Vector3dVector(
        np.array([[1.0, 1.0, 0.0]
                 for _ in range(centers.shape[0])], dtype=float)  # RGB
    )

    # vis.add_geometry(mesh)
    # vis.add_geometry(nonground_o3d)

    # vis.poll_events()
    # vis.update_renderer()

    return height


def read_and_transform(fname, tf):
    if "pcd" in fname:
        pc = o3d.io.read_point_cloud(fname)
        pc.transform(tf)
        return np.asarray(pc.points)

    elif "txt" in fname:
        pc = np.loadtxt(fname, dtype=np.float64).reshape((-1, 4))
        pc[:, :-1] = pc[:, :-1] @ tf[:-1, :-1] + tf[:-1, -1]
        return pc


def main(folder, init_transform):

    files = sorted(os.listdir(folder))

    files = [f for f in files if ".pcd" in f or ".txt" in f or ".npy" in f]

    step = 1

    files = files[::step]

    heights_main = []
    heights_sec = []
    pbar = tqdm(files, total=len(files))

    transform = load_transform_from_yaml(init_transform)

    for f in pbar:

        points = read_and_transform(f'{folder}/{f}', transform)
        height = find_plane(points)

        print(f, len(points), height)

        if "main" in f:
            heights_main.append(height)
        else:
            heights_sec.append(height)

    print(len(heights_main), len(heights_sec))

    l = min(len(heights_main), len(heights_sec))
    diffs = np.array(heights_main)[:l] - np.array(heights_sec)[:l]

    print("Mean:", diffs.mean())
    # plt.plot(np.arange(len(heights_main)), heights_main)
    # plt.plot(np.arange(len(heights_sec)), heights_sec)
    # plt.show()

    # plt.hist(diffs, bins="fd")
    # plt.hist(init_diffs, bins='fd')
    # plt.hist(heights, bins='fd')
    # plt.legend(['Init transform', 'Result transform'])
    # plt.xlabel('Отклонение, мм')
    # plt.show()


if __name__ == "__main__":  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default=f'{sys.path[0]}/RL_and_RR_pcds',
                        help='path to source folder with .pcd files')
    parser.add_argument('-i', '--init', type=str,
                        help='initial extrinsics file', default=f'{sys.path[0]}/init_transform_ruby_l_ruby_r.yaml')

    # parser.add_argument('-r', '--res', type=str,
    #                     help='result extrinsics file', default=f'{sys.path[0]}/res_transform_ruby_l_ruby_r.yaml')
    args = parser.parse_args()

    print(args)

    main(args.path, args.init)

    # vis.destroy_window()
