#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import numpy as np

import jax.numpy as jnp
from jax import grad, jit, vmap, value_and_grad
from jax.numpy.linalg import inv
from jax.scipy.linalg import expm
from scipy.spatial.transform import Rotation

from jaxopt import GaussNewton, LevenbergMarquardt
from jax import config
import yaml


config.update("jax_enable_x64", True)


@jit
def predict(x, W):
    # return jnp.linalg.inv(W) @ x @ W
    return (W) @ x @ jnp.linalg.inv(W)


@jit
def quat2mat(quat):
    n = jnp.linalg.norm(quat, ord=2)
    x, y, z, w = quat / n

    m = jnp.array([
        [1.0 - 2 * (y**2 + z**2), 2 * (x * y - z * w), 2 * (x * z + y * w)],
        [2 * (x * y + z * w), 1.0 - 2 * (x**2 + z**2), 2 * (y * z - x * w)],
        [2 * (x * z - y * w), 2 * (y * z + x * w), 1.0 - 2 * (x**2 + y**2)]
    ])
    return m


@jit
def tf_mat_to_quat_pos(m):
    qw = jnp.sqrt(1 + m[0, 0] + m[1, 1] + m[2, 2]) / 2
    qx = (m[2, 1] - m[1, 2]) / (4 * qw)
    qy = (m[0, 2] - m[2, 0]) / (4 * qw)
    qz = (m[1, 0] - m[0, 1]) / (4 * qw)
    return jnp.array([qx, qy, qz, qw, m[0, -1], m[1, -1], m[2, -1]])


@jit
def rot_mat_to_quat(m):
    qw = jnp.sqrt(1 + m[0, 0] + m[1, 1] + m[2, 2]) / 2
    qx = (m[2, 1] - m[1, 2]) / (4 * qw)
    qy = (m[0, 2] - m[2, 0]) / (4 * qw)
    qz = (m[1, 0] - m[0, 1]) / (4 * qw)
    return jnp.array([qx, qy, qz, qw])


v_tf_mat_to_quat_pos = jit(vmap(tf_mat_to_quat_pos))


@jit
def quat_pos_to_tf_mat(pose):

    M = jnp.hstack([quat2mat(pose[-4:]), pose[:-4].reshape((3, 1))])

    M = jnp.concatenate([M, np.array([0, 0, 0, 1]).reshape(1, 4)])
    return M


@jit
def loss(W, sensor_trs, localization_transform):
    target_tf = quat_pos_to_tf_mat(W)
# def loss(W, x, gt_transform, gt_z):

    # W = jnp.append(W[:2], gt_z, W[4:])
    # par = quat_pos_to_tf_mat(W)
    # print(v_tf_mat_to_quat_pos(gt_transform), v_tf_mat_to_quat_pos(predict(x, par)))
    residuals = v_tf_mat_to_quat_pos(
        localization_transform) - v_tf_mat_to_quat_pos(predict(sensor_trs, target_tf))
    # residuals = y - predict(x, par)
    # print(jnp.linalg.norm(residuals))

    return residuals.ravel()


@jit
def xy_loss(W, x, y, gt_z):
    par = quat_pos_to_tf_mat(jnp.append(W, gt_z))
    residuals = v_tf_mat_to_quat_pos(y) - v_tf_mat_to_quat_pos(predict(x, par))
    return residuals.ravel()


@jit
def wxy_loss(W, x, y, gt_z):
    par = quat_pos_to_tf_mat(jnp.array((0, 0, 1, *W, gt_z)))

    residuals = v_tf_mat_to_quat_pos(y) - v_tf_mat_to_quat_pos(predict(x, par))
    return residuals.ravel()


# @jit
# def quat_to_rotvec(quat):
#     theta = 2 * jnp.arctan2(jnp.linalg.norm(quat[:-1]), quat[-1])
#     vec = quat[:-1] / jnp.sin(theta / 2)
#     return vec


# def rotvec_to_quat(vec):

#     return expm()


def optimize(sensor_trs, localization_transform, init_pose, gt_z=1.63):

    lm = LevenbergMarquardt(
        residual_fun=loss,
        maxiter=5000,
        # verbose=True,
        tol=1e-12,
        xtol=1e-12,
        gtol=1e-12,
        solver='inv',
        jit=True,
    )

    res = lm.run(init_pose,
                 sensor_trs=sensor_trs,
                 localization_transform=localization_transform)

    quat = res.params[-4:] / jnp.linalg.norm(res.params[-4:], ord=2)
    if quat[0] < 0:
        quat *= -1

    # quat = res.params[:2] / jnp.linalg.norm(res.params[:2], ord=2)
    # quat = jnp.array([0, 0, *res.params[:2]])
    # result = jnp.concatenate([quat, res.params[:4], jnp.array([gt_z])])
    # result = res.params
    # result[-4:] = quat
    # result = jnp.concatenate([quat, res.params[4:], jnp.array([gt_z])])
    result = jnp.concatenate([res.params[:-4], quat])
    print('Error at the end of optimization: ', res.state.error)
    # print(jnp.linalg.norm(xy_loss(init_pose[:-1], p12s, trs, gt_z)))
    # print(jnp.linalg.norm(xy_loss(res.params, p12s, trs, gt_z)))
    print(jnp.linalg.norm(loss(init_pose, sensor_trs, localization_transform)))
    print(jnp.linalg.norm(loss(res.params, sensor_trs, localization_transform)))
    # print(result)
    return result


def find_outliers(transforms, ret_inliers=False):
    ret_inds = []
    # print(transforms)
    quat_poses = np.stack([tf_mat_to_quat_pos(t) for t in transforms])
    # print(quat_poses.shape)
    for col in quat_poses.T:
        Q1, Q3 = np.percentile(col, [25, 75])
        IQR = Q3 - Q1
        low_thresh = Q1 - IQR * 1.5
        high_thresh = Q3 + IQR * 1.5
        if ret_inliers:
            ret_inds.append(np.where((col > low_thresh)
                            & (col < high_thresh))[0])
            # print(len(np.where((col > low_thresh) & (col < high_thresh))[0]))
        else:
            ret_inds.append(np.where((col < low_thresh)
                            | (col > high_thresh))[0])

    ret_inds = set(np.concatenate(ret_inds))
    return np.array(sorted(list(ret_inds)))
