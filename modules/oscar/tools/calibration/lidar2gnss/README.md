# Калибровка GNSS -> Лидар

Калибровка осуществляется в 2 этапа:
1. Нахождение трансформаций между облаками точек в имеющейся записи алгоритмом VGICP;
2. Нахождение трансформации между лидаром и GNSS методом наименьших квадратов. 

Порядок обработки записей:
1. Извлечь облака точек из .record файла
2. Провести калибровку

## 1. Извлечение данных (Docker)

Воспользуйтесь скриптом [extract_pointclouds.py](../../extraction/README.md#extractpointcloudspy)

## 2. Калибровка (Anaconda environment)

Установка пакетов (требуется Anaconda) на хосте:

```
../conda_lidar_calib_env.sh
```
### Запуск:

```
python calibrate_lidar.py
```

Параметры:
 - ```-p``` - папка с облаками точек в pcd формате;
 - ```-s``` - шаг чтения файлов с облаками точек;
 - ```-r``` - папка, куда будут сохраняться результаты регистрации облаков точек;
 - ```-i``` - путь к начальному файлу трансформации;
 - ```-o``` - путь к оптимизированному файлу трансформации;
 - ```-v``` - флаг для визуализации результата.
