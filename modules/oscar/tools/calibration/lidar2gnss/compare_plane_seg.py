#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import os
import sys
from copy import deepcopy as dc


from scipy.spatial.transform import Rotation
import numpy as np
import open3d as o3d
from tqdm.auto import tqdm

import matplotlib
from matplotlib import pyplot as plt

from common.common import load_transform_from_yaml

np.set_printoptions(suppress=True, precision=3)


plt.ion()
plt.rcParams['axes.grid'] = True


class RTPlot:
    def __init__(self) -> None:
        self.figure, self.axs = plt.subplots(
            nrows=1, ncols=3, figsize=(19.2, 10.8))

        # self.ls = [ax.plot(0, 0)[0] for ax in self.axs]

        self.plane1 = np.empty((0, 4))
        self.plane2 = np.empty((0, 4))

        self.titles = ['X', 'Y', 'Z']
        plt.legend(['Init transformation', 'Result transformation'])

    def update(self, plane1, plane2):

        self.plane1 = np.vstack([self.plane1, plane1])
        self.plane2 = np.vstack([self.plane2, plane2])

        for idx, ax in enumerate(self.axs):

            ax.cla()
            ax.set_title(self.titles[idx])

            ax.plot(np.arange(len(self.plane1)), self.plane1[:, idx])
            ax.plot(np.arange(len(self.plane1)), self.plane2[:, idx])

            ax.legend(['Init transformation', 'Result transformation'])

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()


def main(folder, init_file, poses_file, res_file, step=1):

    files = sorted(os.listdir(folder))
    files = [f for f in files if 'pcd' in f]

    with open(poses_file, 'r') as f:
        poses = f.readlines()

    init_transformation = load_transform_from_yaml(init_file)
    files = files[::step]
    poses = poses[::step]

    res_transform = load_transform_from_yaml(res_file)

    plots = RTPlot()

    for idx, (f, pose) in enumerate(tqdm(zip(files, poses), total=len(files))):

        r1, t1 = pose[:-1].split(' ')[1:-3], pose[:-1].split(' ')[-3:]

        r1 = np.array(r1).reshape((3, 3)).astype(np.float64)

        pose = np.eye(4, dtype=np.float64)
        pose[:-1, :-1] = r1
        pose[:-1, -1] = np.array(t1).astype(np.float64)

        # .transform(p1 @ init_transformation)
        pc = o3d.io.read_point_cloud(folder + '/' + f)
        pc = pc.voxel_down_sample(0.1)

        THRESH = 1000
        HEIGHT = 10
        min_b = np.array([-THRESH, -THRESH, -HEIGHT],
                         dtype=np.float64).reshape((3, 1))
        max_b = np.array([THRESH, THRESH, 0], dtype=np.float64).reshape((3, 1))
        bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)

        plane_model_init, _ = dc(pc).transform((pose @ (init_transformation))).segment_plane(distance_threshold=0.1,
                                                                                             ransac_n=3,
                                                                                             num_iterations=2000)

        plane_model_res, _ = dc(pc).transform((pose @ (res_transform))).segment_plane(distance_threshold=0.1,
                                                                                      ransac_n=3,
                                                                                      num_iterations=2000)

        plots.update(plane_model_init, plane_model_res)


if __name__ == '__main__':  # pragma: no cover
    main('inf_bp_l',
         'init_transform_BL.yaml',
         'inf_bp_l/0_poses.txt',
         'res_transform_BL.yaml')
