#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest
import sys
import os


import pytest

import matplotlib
from matplotlib import pyplot as plt
matplotlib.use('agg')

from lidar2gnss.registration_pc import main as register_transformations
from lidar2gnss.optimize_tranform import main as optimize_transform
from lidar2gnss.compare_plane_seg import main as compare_plane_seg
from lidar2gnss.merge_all_pc import main as show_merged_pc


class TestLidar2Gnss(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self.pc_folder = '/apollo/modules/oscar/tools/test_data/lidar2gnss/inf_r_l'
        self.save_folder = '/apollo/modules/oscar/tools/test_data/res_lidar2gnss'
        self.init_extrinsics_file = '/apollo/modules/oscar/tools/test_data/lidar2gnss/init_transform_RL.yaml'
        self.res_transform_f = '/apollo/modules/oscar/tools/test_data/res_lidar2gnss/res_transform_RL.yaml'

    @pytest.mark.run(order=1)
    def test_registration(self):
        register_transformations(
            self.pc_folder, self.init_extrinsics_file, self.save_folder, visualize=False, step=10)

    @pytest.mark.run(order=2)
    def test_optimization(self):

        optimize_transform(
            self.save_folder, self.init_extrinsics_file, self.res_transform_f)

    @pytest.mark.run(order=3)
    def test_vis(self):
        show_merged_pc(self.pc_folder, self.init_extrinsics_file,
                       self.save_folder, self.res_transform_f)

    @pytest.mark.run(order=4)
    def test_metric(self):
        compare_plane_seg(self.pc_folder, self.init_extrinsics_file,
                          f'{self.pc_folder}/0_poses.txt', self.res_transform_f, step=10)
