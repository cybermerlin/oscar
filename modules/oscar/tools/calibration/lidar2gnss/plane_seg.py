#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import open3d as o3d
import os
from copy import deepcopy as dc
from scipy.spatial.transform import Rotation

import numpy as np

from tqdm.auto import tqdm

np.set_printoptions(suppress=True, precision=3)


def main():
    folder = '../lidar2lidar_calibration/RL_and_BL_pcds'

    files = sorted(os.listdir(folder))

    # files = [f for f in files if "main" in f]
    files = [f for f in files if "sec" in f]

    step = 1

    files = files[::step]

    results = []

    for idx, f in enumerate(tqdm(files, total=len(files))):

        pc = o3d.io.read_point_cloud(folder + '/' + f)

        pc = pc.voxel_down_sample(0.1)

        THRESH = 5
        HEIGHT = 10
        min_b = np.array([-THRESH, -THRESH, -HEIGHT],
                         dtype=np.float64).reshape((3, 1))
        max_b = np.array([THRESH, THRESH, HEIGHT],
                         dtype=np.float64).reshape((3, 1))
        bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)
        pc = pc.crop(bbox)

        plane_model, inliers = pc.segment_plane(distance_threshold=0.05,
                                                ransac_n=3,
                                                num_iterations=2000)

        print(plane_model)
        results.append(plane_model)

        inlier_cloud = pc.select_by_index(inliers)
        inlier_cloud.paint_uniform_color([1.0, 0, 0])

        outlier_cloud = pc.select_by_index(inliers, invert=True)

        # o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])

        # np.savetxt('res_plane_no_tr.txt', np.stack(results))
if __name__ == "__main__":  # pragma: no cover
    main()
