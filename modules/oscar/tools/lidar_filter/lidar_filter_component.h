///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include "pcl/PCLPointCloud2.h"
#include "pcl/conversions.h"
#include "pcl/filters/voxel_grid.h"
#include "pcl/point_cloud.h"
#include "pcl/point_types.h"

#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/tools/lidar_filter/proto/lidar_filter_conf.pb.h"

#include "cyber/component/component.h"
#include "cyber/cyber.h"

using apollo::cyber::Component;

using ApolloPointCloud = apollo::drivers::PointCloud;
using Point = pcl::PointXYZI;

namespace apollo {
namespace oscar {
class LidarFilterComponent : public Component<ApolloPointCloud> {
 public:
  bool Init() override;

  bool Proc(const std::shared_ptr<ApolloPointCloud>& msg) override;

 private:
  std::shared_ptr<apollo::cyber::Writer<ApolloPointCloud>> writer_ = nullptr;

  std::shared_ptr<ApolloPointCloud> out_msg_ = nullptr;
  pcl::VoxelGrid<pcl::PCLPointCloud2> voxel_filter_;

  apollo::oscar::lidar_filter::Conf config_;
};
CYBER_REGISTER_COMPONENT(LidarFilterComponent)

}  // namespace oscar
}  // namespace apollo
