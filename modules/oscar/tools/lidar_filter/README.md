# Lidar Filter

Модуль создает ноду, которая подписывается на канал с облаком точек, производит voxel downsampling и публикует отфильтрованное облако точек.

## Использование

```
mainboard -d apollo/modules/oscar/tools/lidar_filter/lidar_filter.dag
```

## Параметры

 - ```channel_postfix``` - постфикс, который будет добавлен к названию входного канала;
 - ```xy_voxel_size``` - размер вокселей по осям Х и Y;
 - ```z_voxel_size``` - размер вокселей по Z.
