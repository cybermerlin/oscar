///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/tools/lidar_filter/lidar_filter_component.h"

#include "pcl/PCLPointCloud2.h"
#include "pcl/conversions.h"
#include "pcl/filters/voxel_grid.h"
#include "pcl/point_cloud.h"
#include "pcl/point_types.h"

namespace {
void ConvertApollo2PCL(const std::shared_ptr<ApolloPointCloud>& point_cloud,
                       pcl::PointCloud<Point>& pcl_pc) {
  pcl_pc.width = point_cloud->width();
  pcl_pc.height = point_cloud->height();
  pcl_pc.is_dense = false;

  if (point_cloud->width() * point_cloud->height() !=
      static_cast<unsigned int>(point_cloud->point_size())) {
    pcl_pc.width = 1;
    pcl_pc.height = point_cloud->point_size();
  }
  pcl_pc.points.resize(point_cloud->point_size());

  for (size_t i = 0; i < pcl_pc.points.size(); ++i) {
    const auto& point = point_cloud->point(static_cast<int>(i));
    pcl_pc.points[i].x = point.x();
    pcl_pc.points[i].y = point.y();
    pcl_pc.points[i].z = point.z();
    pcl_pc.points[i].intensity = point.intensity();
  }
}

void ConvertPCL2Apollo(const pcl::PointCloud<Point>& point_cloud,
                       std::shared_ptr<ApolloPointCloud> out_msg) {
  out_msg->mutable_point()->erase(out_msg->point().begin(),
                                  out_msg->point().end());

  for (size_t i = 0; i < point_cloud.points.size(); ++i) {
    auto* point = out_msg->add_point();

    point->set_x(point_cloud.points[i].x);
    point->set_y(point_cloud.points[i].y);
    point->set_z(point_cloud.points[i].z);
    point->set_intensity(point_cloud.points[i].intensity);
  }
}
}  // namespace

namespace apollo {
namespace oscar {

bool LidarFilterComponent::Init() {
  if (!GetProtoConfig(&config_)) {
    AERROR << "Parse conf file failed, " << ConfigFilePath();
    return false;
  }

  out_msg_ = std::make_shared<ApolloPointCloud>();
  voxel_filter_.setLeafSize(config_.xy_voxel_size(), config_.xy_voxel_size(),
                            config_.z_voxel_size());

  return true;
}

bool LidarFilterComponent::Proc(const std::shared_ptr<ApolloPointCloud>& msg) {
  if (writer_ == nullptr) {
    auto channel = readers_[0]->GetChannelName();

    writer_ = node_->CreateWriter<ApolloPointCloud>(channel +
                                                    config_.channel_postfix());
  }

  pcl::PointCloud<Point>::Ptr src_pcl_pc_(new pcl::PointCloud<Point>);
  pcl::PointCloud<Point>::Ptr out_pcl_pc_(new pcl::PointCloud<Point>);

  // Because of bug with deallocating PointCloud<PointXYZI> after downsampling
  // We use PointCloud2 instead.
  // TODO: get rid of intermediate Apollo -> PointCLoud<PointXYZI> ->
  // PointCloud2 conversion
  pcl::PCLPointCloud2::Ptr cloud_src(new pcl::PCLPointCloud2());
  pcl::PCLPointCloud2::Ptr cloud_filtered(new pcl::PCLPointCloud2());

  auto st = apollo::cyber::Time::Now().ToSecond();

  ConvertApollo2PCL(msg, *src_pcl_pc_);
  AINFO << "Apollo Convertion: " << apollo::cyber::Time::Now().ToSecond() - st;
  st = apollo::cyber::Time::Now().ToSecond();
  pcl::toPCLPointCloud2(*src_pcl_pc_, *cloud_src);

  AINFO << "PCL Convertion: " << apollo::cyber::Time::Now().ToSecond() - st;
  st = apollo::cyber::Time::Now().ToSecond();

  voxel_filter_.setInputCloud(cloud_src);
  voxel_filter_.filter(*cloud_filtered);

  pcl::fromPCLPointCloud2(*cloud_filtered, *out_pcl_pc_);

  AINFO << "Filtering: " << apollo::cyber::Time::Now().ToSecond() - st;
  st = apollo::cyber::Time::Now().ToSecond();

  ConvertPCL2Apollo(*out_pcl_pc_, out_msg_);

  out_msg_->mutable_header()->MergeFrom(msg->header());
  out_msg_->set_frame_id(msg->frame_id());
  out_msg_->set_is_dense(msg->is_dense());
  out_msg_->set_measurement_time(msg->header().lidar_timestamp() / 1e9);

  out_msg_->set_width(out_msg_->point_size());
  out_msg_->set_height(1);

  // out_msg_->set_width(msg->width());
  // out_msg_->set_height(msg->height());

  AINFO << "Sending: " << apollo::cyber::Time::Now().ToSecond() - st;
  st = apollo::cyber::Time::Now().ToSecond();
  writer_->Write(out_msg_);

  return true;
}

}  // namespace oscar
}  // namespace apollo
