#!/usr/bin/env python

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

from cyber.python.cyber_py3 import record
from tqdm import tqdm
from extraction.utils import get_total_messages, TransformsManager
import os
import sys
import argparse

from multiprocessing import Pool, cpu_count
from multiprocessing.pool import AsyncResult

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


class PointcloudTransformer:
    def __init__(self, src_folder, save_file, pc_channel, tf_config_file) -> None:
        self.src_folder = src_folder
        self.pc_channel = pc_channel
        self.transform_manager = TransformsManager(
            tf_config_file)

        self.fwriter = record.RecordWriter()
        if not self.fwriter.open(save_file):
            print('Failed to open record writer!')
            return

    def add_channels(self):
        f = sorted(os.listdir(self.src_folder))[0]

        rfile = self.src_folder + '/' + f

        freader = record.RecordReader(rfile)

        protodesc = freader.get_protodesc(self.pc_channel)
        self.fwriter.write_channel(
            self.pc_channel, "apollo.drivers.PointCloud", protodesc)

        protodesc = freader.get_protodesc("/tf")
        self.fwriter.write_channel(
            "/apollo/localization/pose", "apollo.localization.LocalizationEstimate", protodesc)

    def write_async_results(self, results):
        for ch, r, t in tqdm(results, desc="Writing: ", position=1, leave=False):
            if isinstance(r, AsyncResult):
                r = r.get()
            self.fwriter.write_message(ch, r, t)

    def run(self):
        self.add_channels()

        target_frame_id = "localization"

        with Pool(processes=cpu_count() - 2) as ppool:

            for f in tqdm((sorted(os.listdir(self.src_folder))), desc="Total files: ", position=0):

                rfile = self.src_folder + '/' + f

                freader = record.RecordReader(rfile)
                results = []

                for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader), desc="Current file: ", position=1, leave=False):

                    if channelname == "/apollo/localization/pose":
                        results.append([channelname, msg, timestamp])
                        continue
                    if channelname != self.pc_channel:
                        continue

                    results.append((channelname,
                                    ppool.apply_async(
                                        TransformsManager.transform_message, (self.transform_manager, msg, target_frame_id)),
                                    timestamp))

                self.write_async_results(results)


def main(src_folder: str, save_file: str, pc_channel: str, tf_config_file: str):

    pc = PointcloudTransformer(
        src_folder, save_file, pc_channel, tf_config_file)
    pc.run()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2023/31.01/bpearls_and_cameras/')
    parser.add_argument('-d', '--dest', type=str, help='a path to result file',
                        default='/apollo/data/bag/pcs_in_world_frame/pointclouds.record')

    parser.add_argument("-c", "--channel", type=str, help="channel with pointclouds",
                        default="/apollo/sensor/lidar/fused/compensator/PointCloud2")

    parser.add_argument("--tf_config", type=str, help="path to file with static TF config file",
                        default="/apollo/modules/calibration/data/mb_actros/transform_conf/static_transform_conf.pb.txt")

    args = parser.parse_args()
    print(args)
    try:
        main(args.path, args.dest, args.channel, args.tf_config)
    except KeyboardInterrupt:
        sys.exit()
