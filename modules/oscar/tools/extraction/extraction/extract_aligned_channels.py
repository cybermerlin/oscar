#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import os
import argparse
import sys
from datetime import datetime
import cv2 as cv
import difflib
sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

import numpy as np
from tqdm.auto import tqdm
import open3d as o3d
from typing import overload

from scipy.spatial.distance import cdist

from cyber.python.cyber_py3 import record
from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2

from extraction.utils import get_total_messages

np.set_printoptions(precision=5, suppress=True)

pcd_dtype = o3d.core.float32


def extract_pc_and_ts(msg_struct):
    points = np.array(
        [[i.x, i.y, i.z, i.intensity] for i in msg_struct.point])

    pcd = o3d.t.geometry.PointCloud()
    pcd.point["positions"] = o3d.core.Tensor(
        points[:, :-1], pcd_dtype)

    pcd.point["intensity"] = o3d.core.Tensor(
        points[:, -1].reshape((-1, 1)), pcd_dtype)

    return pcd


def extract_compressed_image(image_struct):
    buf = np.frombuffer(image_struct.data, dtype=np.uint8)
    return cv.imdecode(buf, cv.IMREAD_COLOR)


def extract_image(image_struct):
    im = np.fromstring(image_struct.data, np.uint8).reshape(
        (image_struct.height, image_struct.width, 3))
    im = cv.cvtColor(im, cv.COLOR_BGR2RGB)
    return im


datatype2ClassMap = {
    "apollo.drivers.PointCloud": pointcloud_pb2.PointCloud,
    "apollo.drivers.Image": sensor_image_pb2.Image,
    "apollo.drivers.CompressedImage": sensor_image_pb2.CompressedImage,
}


def extract(msg, datatype):
    msg_struct = datatype2ClassMap[datatype]()
    msg_struct.ParseFromString(msg)

    data = extract_fns[datatype](msg_struct)

    # return msg_struct.measurement_time, data
    return msg_struct.header.timestamp_sec, data


extract_fns = {
    "apollo.drivers.PointCloud": extract_pc_and_ts,
    "apollo.drivers.Image": extract_image,
    "apollo.drivers.CompressedImage": extract_compressed_image,
}


def save_msg(msg, basename: str):
    save_msg_func_map[type(msg)](msg, basename)


def save_image(msg: np.ndarray, basename: str):
    cv.imwrite(f'{basename}.png', msg)


def save_pointcloud(msg: o3d.t.geometry.PointCloud, basename: str):
    # o3d.io.write_point_cloud(f"{basename}.pcd", msg)
    o3d.t.io.write_point_cloud(f"{basename}.pcd", msg)


save_msg_func_map = {
    np.ndarray: save_image,
    o3d.t.geometry.PointCloud: save_pointcloud
}


def main(src_folder, save_folder,
         main_channel='/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2',
         secondary_channel='/apollo/sensor/lidar/rs_bpearl_left/compensator/PointCloud2', period=5):

    os.makedirs(save_folder, exist_ok=True)

    for f in tqdm(sorted(os.listdir(src_folder)), position=0):

        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)
        time.sleep(.1)

        channel_list = [main_channel, secondary_channel]

        channels_dict = {a: [] for a in channel_list}

        # need_to_save_sec = False

        channel2type_map = {}

        # desired_period = 5

        last_main_ch_time = 0

        for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(),
                                                          total=get_total_messages(
                                                              freader),
                                                          position=1,
                                                          leave=False,
                                                          desc=f"reading {f}"):

            # print(channelname)
            if channelname == main_channel:
                if timestamp // 1e9 - last_main_ch_time >= period:
                    last_main_ch_time = timestamp // 1e9
                else:
                    continue

            # sometimes field datatype is empty for random messages so we save datatype from previous messages
            if datatype != "":
                channel2type_map[channelname] = datatype

            try:
                if channelname in channels_dict:
                    channels_dict[channelname].append(
                        extract(msg, channel2type_map[channelname]))
            except KeyError as e:
                print(e)
                # for k in channels_dict.keys():
                #     print("\n".join(difflib.ndiff([channelname], [k])))
                print(channels_dict.keys())

                print("/apollo/sensor/camera/lucid/left/compressed" == channelname)
                print("/apollo/sensor/lidar/rs_bpearl_left/PointCloud2" == channelname)
                exit()

        timestamps_main, list_main = list(
            map(list, zip(*channels_dict[main_channel])))

        timestamps_main = np.array(timestamps_main)

        aligned_list = []

        filter_idxs = set()
        diff_thresh = 0.03

        for channel in channels_dict:
            if channel == main_channel:
                aligned_list.append(list_main)
                continue

            timestamp_cur, list_current = list(
                map(list, zip(*channels_dict[channel])))
            timestamp_cur = np.array(timestamp_cur)
            dists = cdist(timestamps_main.reshape((-1, 1)),
                          timestamp_cur.reshape((-1, 1)))

            nearest_idxs = np.argmin(dists, axis=1)
            timestamp_cur = timestamp_cur[nearest_idxs]
            diffs = timestamps_main - timestamp_cur
            filter_idxs.update(set(np.where(np.abs(diffs) < diff_thresh)[0]))

            aligned_list.append([list_current[i] for i in nearest_idxs])

        # filter messages by period
        # desired_period = 5
        # init_period = (timestamps_main[-1] - timestamps_main[0]) / len(timestamps_main)
        # factor = desired_period / init_period

        # filter messages that exceed time difference thresold
        filter_idxs = list(filter_idxs)
        for idx, l in enumerate(aligned_list):
            aligned_list[idx] = [l[keep_idx] for keep_idx in filter_idxs]
        timestamps_main = [timestamps_main[i] for i in filter_idxs]

        strip_prefix_len = len(os.path.commonprefix(channel_list))
        save_name_channels = [i[strip_prefix_len:].replace(
            "/", "_") for i in channel_list]

        str_times = [datetime.fromtimestamp(t).strftime(
            "%Y-%m-%d-%H-%M-%S.%f")[:-3] for t in timestamps_main]

        for save_channel_name, save_list in zip(save_name_channels, aligned_list):
            for str_timestamp, msg in tqdm(zip(str_times, save_list),
                                           total=len(str_times),
                                           position=1,
                                           leave=False,
                                           desc=f"saving channel {save_channel_name.replace('_', '/')}"):
                save_msg(
                    msg, f"{save_folder}/{str_timestamp}_{save_channel_name}")


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2023/31.01')
    parser.add_argument('-d', '--dest', type=str, help='a path to destination folder',
                        default='bpearl_l_lucid_left_aligned')
    parser.add_argument('-m', '--main', type=str, help='main channel name',
                        default='/apollo/sensor/lidar/rs_bpearl_left/PointCloud2')
    parser.add_argument('-s', '--sec', type=str, help='secondary channel name',
                        default='/apollo/sensor/camera/lucid/left/compressed')
    parser.add_argument('--period', type=float, help='period between messages in main channel',
                        default=0.1)

    args = parser.parse_args()
    print(args)
    try:
        main(args.path,
             args.dest,
             main_channel=args.main,
             secondary_channel=args.sec,
             period=args.period
             )
    except KeyboardInterrupt:
        exit()
