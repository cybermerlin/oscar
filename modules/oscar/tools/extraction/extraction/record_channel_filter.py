#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import os
import yaml
import argparse
import sys

from tqdm import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record
from extraction.utils import get_total_messages


def main(src_folder, dest_folder, black_list_channels=[], white_list_channels=[]):

    start = time.time()
    os.makedirs(dest_folder, exist_ok=True)

    for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):

        rfile = src_folder + '/' + f
        wfile = dest_folder + '/' + f

        freader = record.RecordReader(rfile)
        fwriter = record.RecordWriter()

        if not fwriter.open(wfile):
            print('Failed to open record writer!')
            return

        added_channels = []

        for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader)):

            if channelname in black_list_channels:
                continue
            if len(white_list_channels) > 0:
                assert isinstance(white_list_channels, (list, tuple))
                if channelname not in white_list_channels:
                    continue

            if channelname not in added_channels:
                added_channels.append(channelname)
                protodesc = freader.get_protodesc(channelname)

                fwriter.write_channel(channelname, datatype, protodesc)

            fwriter.write_message(channelname, msg, timestamp)

        fwriter.close()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-cfg', type=str, default=f'{sys.path[0]}/record_channel_filter_cfg.yaml',
                        help='path to json with configs')

    args = parser.parse_args()

    print(args)
    config_file = args.cfg
    with open(config_file, 'r') as f:
        params = yaml.safe_load(f)

    assert bool(params["black_list_channels"]) ^ bool(params["white_list_channels"]), \
        f"Either black or white channels must be specified, got {params['black_list_channels']} for black list, and {params['white_list_channels']} for white list"

    try:
        print('Arguments:')
        print(params)
        main(**params)
        print(f'Save folder: {params["dest_folder"]}')
    except KeyboardInterrupt:
        exit()
