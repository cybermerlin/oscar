#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import argparse

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

import time
import os
import numpy as np

from cyber.python.cyber_py3 import record
from modules.common_msgs.localization_msgs import localization_pb2

from tqdm import tqdm


def main(src_folder, save_file):

    start = time.time()
    ar = np.empty((3,), dtype=np.float32)

    for idx, f in enumerate(tqdm((sorted(os.listdir(src_folder))))):

        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        time.sleep(.1)

        for channelname, msg, datatype, timestamp in freader.read_messages():
            if channelname != '/apollo/localization/pose':
                continue

            msg_struct = localization_pb2.LocalizationEstimate()
            msg_struct.ParseFromString(msg)
            ar = np.vstack([ar, np.array([msg_struct.pose.position.x,
                                          msg_struct.pose.position.y, msg_struct.header.timestamp_sec])])

        dur = time.time() - start
        # print(f'{idx + 1} / {len(os.listdir(src_folder))} files extracted, took {(dur // 60)} m {int(dur) % 60} s')

    np.savetxt(save_file, ar)


if __name__ == '__main__':  # pragma: no cover


    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/infinity_compensator/')
    parser.add_argument('-d', '--dest', type=str, help='a path to result file',
                        default='coordinates.txt')


    args = parser.parse_args()
    print(args)
    try:
        main(args.path, args.dest)
    except KeyboardInterrupt:
        exit()
