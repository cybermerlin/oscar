###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np
import open3d as o3d
import sys
import cv2 as cv

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2


class NumpyPCProcessor:

    @staticmethod
    def parseMessage(msg):
        msg_struct = pointcloud_pb2.PointCloud.FromString(msg)

        ts = msg_struct.measurement_time

        points = np.array(
            [[i.x, i.y, i.z, i.intensity] for i in msg_struct.point],
            dtype=np.float64
        )

        return ts, points

    @staticmethod
    def save(array, file_basename):

        if isinstance(array, str) or isinstance(array, bytes):
            _, array = NumpyPCProcessor.parseMessage(array)


        if not file_basename.endswith(".npy"):
            file_basename = file_basename + ".npy"

        np.save(file_basename, array)


o3d_device = o3d.core.Device("CPU:0")
o3d_float = o3d.core.float32

class O3dPCDProcessor:

    @staticmethod
    def parseMessage(msg):
        ts, points = NumpyPCProcessor.parseMessage(msg)
        return ts, points

    @staticmethod
    def save(array, file_basename):

        if isinstance(array, str):
            _, array = O3dPCDProcessor.parseMessage(array)

        pcd = o3d.t.geometry.PointCloud(o3d_device)
        pcd.point['positions'] = o3d.core.Tensor(array[:, :3], o3d_float, o3d_device)
        pcd.point['intensity'] = o3d.core.Tensor(array[:, 3][..., np.newaxis], o3d_float, o3d_device)
        
        o3d.t.io.write_point_cloud(f"{file_basename}.pcd", pcd)
        # o3d.t.io.write_point_cloud(f"{file_basename}.ply", pcd)

class RawImageProcessor:
    @staticmethod
    def parseMessage(msg):
        msg_struct = sensor_image_pb2.Image()
        msg_struct.ParseFromString(msg)

        im = np.fromstring(msg_struct.data, np.uint8).reshape(
            (msg_struct.height, msg_struct.width, 3))

        return msg_struct.header.timestamp_sec, im

    @staticmethod
    def save(array, file_basename):

        if isinstance(array, str):
            _, array = RawImageProcessor.parseMessage(array)

        if not file_basename.endswith(".txt"):
            file_basename = file_basename + ".txt"
        np.savetxt(file_basename, array)

class CompressedImageProcessor:
    @staticmethod
    def parseMessage(msg):
        msg_struct = sensor_image_pb2.CompressedImage.FromString(msg)

        buf = np.frombuffer(msg_struct.data, dtype=np.uint8)
        im = cv.imdecode(buf, cv.IMREAD_COLOR)

        return msg_struct.header.timestamp_sec, im

    @staticmethod
    def save(array, file_basename):

        if isinstance(array, str):
            _, array = CompressedImageProcessor.parseMessage(array)

        if not file_basename.endswith(".txt"):
            file_basename = file_basename + ".txt"
        np.savetxt(file_basename, array)
