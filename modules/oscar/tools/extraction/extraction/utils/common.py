#!/usr/bin/env python

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
import time
import sys
from queue import Queue
from typing import Iterable
from copy import deepcopy as dc

import yaml
import pandas as pd
import numpy as np
from scipy.spatial.transform import Rotation as R
import google.protobuf.text_format as text_format
import open3d as o3d

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record

from modules.transform.proto import static_transform_conf_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2


np.set_printoptions(precision=4, suppress=True)


def get_total_messages(freader):
    return sum(freader.get_messagenumber(channel) for channel in freader.get_channellist())


class Frame:
    def __init__(self, name) -> None:
        self.name = name
        self.connected_tfs = {}

    def add_connection(self, name, tf):
        self.connected_tfs[name] = tf

    def get_tf(self, name):
        return self.connected_tfs[name]


class TransformTree:

    def __init__(self, static_tf_conf) -> None:
        self.static_proto_f = static_tf_conf

        self.frames = {}

        tf_conf = static_transform_conf_pb2.Conf()

        try:
            with open(static_tf_conf, 'r') as f:
                text_format.Merge(f.read(), tf_conf)
        except FileNotFoundError:
            print(
                f"Static transform config file does not exist: {static_tf_conf}")

        for extrinsic in tf_conf.extrinsic_file:
            try:
                self.add_transform(extrinsic.file_path)
            except FileNotFoundError:
                print(
                    f'Extrinsic file {extrinsic.file_path} does not exist. It is readed from static config file: {static_tf_conf}. Check it!')
                exit(0)

    def add_transform(self, extrinsic_file: str):
        with open(extrinsic_file, 'r') as f:
            data = yaml.safe_load(f)

            parent_frame = data['header']['frame_id']
            child_frame = data['child_frame_id']

            tl = data['transform']['translation']
            rot = data['transform']['rotation']
            transform = np.eye(4)
            transform[:-1, -1] = [tl['x'], tl['y'], tl['z']]
            transform[:-1, :-1] = R.from_quat([rot['x'],
                                              rot['y'], rot['z'], rot['w']]).as_matrix()

            if parent_frame not in self.frames:
                self.frames[parent_frame] = Frame(parent_frame)
            if child_frame not in self.frames:
                self.frames[child_frame] = Frame(child_frame)

            self.frames[parent_frame].add_connection(child_frame, transform)
            self.frames[child_frame].add_connection(
                parent_frame, np.linalg.inv(transform))

    def find_chain(self, tf1, tf2):
        assert tf1 in self.frames, f'{self.frames.keys()}'
        assert tf2 in self.frames, f'{self.frames.keys()}'

        # Dijkstra algorithm (or something similar)
        paths = {name: None for name in self.frames.keys()}
        visited = {name: False for name in self.frames.keys()}

        paths[tf1] = [tf1]
        current_node = tf1

        visit_queue = Queue()

        while current_node != tf2:
            neighbors = list(self.frames[current_node].connected_tfs.keys())

            for n in neighbors:
                if not visited[n]:
                    visit_queue.put(n)

                if paths[n] is None:
                    paths[n] = paths[current_node] + [n]

                if n == tf2:
                    break

            visited[current_node] = True
            current_node = visit_queue.get()

        return paths[tf2]

    def get_transform(self, tf1, tf2):
        chain = self.find_chain(tf1, tf2)

        if len(chain) == 1:
            return np.eye(4)

        cumulative_transform = np.eye(4)
        for parent, child in zip(chain[:-1], chain[1:]):

            current_tf = self.frames[parent].get_tf(child)
            cumulative_transform = cumulative_transform @ current_tf

        return cumulative_transform


class TransformsManager:

    def __init__(self, static_tf_conf) -> None:
        self.static_proto_f = static_tf_conf
        self.transform_tree = TransformTree(static_tf_conf)
        self.last_localization_tf = None
        self.saved_static_tf = None

    def update_loc_tf(self, msg):
        msg_struct = transform_pb2.TransformStampeds.FromString(msg)

        tl = msg_struct.transforms[0].transform.translation
        rot = msg_struct.transforms[0].transform.rotation
        self.last_localization_tf = np.eye(4)
        self.last_localization_tf[:-1, -1] = [tl.x, tl.y, tl.z]
        self.last_localization_tf[:-1, :-1] = R.from_quat([rot.qx,
                                                           rot.qy, rot.qz, rot.qw]).as_matrix()

        self.last_loc_ts = msg_struct.transforms[0].header.timestamp_sec

    def transform_message(self, msg: str, target_frame_id):
        msg_struct = pointcloud_pb2.PointCloud.FromString(msg)

        points = np.array(
            [[i.x, i.y, i.z] for i in msg_struct.point],
            dtype=np.float64)
        expanded_points = np.concatenate(
            [points, np.ones(len(points), dtype=np.float64).reshape((-1, 1))], axis=1)

        converted_points = (self.get_static_tf(
            msg_struct, target_frame_id) @ expanded_points.T).T

        for orig_point, converted_point in zip(msg_struct.point, converted_points):
            orig_point.x = converted_point[0]
            orig_point.y = converted_point[1]
            orig_point.z = converted_point[2]

        msg_struct.header.frame_id = target_frame_id

        return msg_struct.SerializeToString()

    def get_static_tf(self, msg, target_frame_id):
        if self.saved_static_tf is None:
            if isinstance(msg, str):
                msg = pointcloud_pb2.PointCloud.FromString(msg)
            frame_id = msg.header.frame_id
            self.saved_static_tf = self.transform_tree.get_transform(
                target_frame_id, frame_id)
        return self.saved_static_tf

    def get_tf(self, frame1, frame2):
        return self.transform_tree.get_transform(
            frame1, frame2)


class ReaderWrapper:

    def __init__(self, path, *args, **kwargs) -> None:

        if not isinstance(path, list):
            # assert os.path.exists(path), f"Path does not exist: {path}"

            self.files = []
            if os.path.isdir(path):
                for (root, _, files) in os.walk(path):
                    [self.files.append(f'{root}/{f}')
                        for f in files if ".record" in f]
            else:
                self.files = [path]

        else:
            self.files = dc(path)

        self.files = sorted(self.files)

        self.args = args
        self.kwargs = kwargs

    def __iter__(self):
        return self.read_messages()

    def read_messages(self):
        for f in self.files:
            freader = record.RecordReader(f)

            for data in freader.read_messages(*self.args, **self.kwargs):
                yield data

    def __len__(self):
        return self.get_total_messages()

    def get_total_messages(self):
        return sum([get_total_messages(record.RecordReader(f)) for f in self.files])


class TimeMeasure:

    def __init__(self) -> None:
        self.start = None

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(time.time() - self.start)


def voxel_downsample(pointcloud: np.ndarray, voxel_size):
    # Downsampling was written instead of using open3d version to make use attributes of pointcloud like intensity and colors
    # pointcloud: np.ndarray[N, M], where M axis stands for [x, y, z, [attrs]]
    # voxel_size: size in metres

    pc_rounded = pointcloud[:, :3] - (pointcloud[:, :3] % voxel_size)

    df = pd.DataFrame(pc_rounded)
    inds = list(range(3, 3 + pointcloud.shape[1]))

    df[inds] = pointcloud

    means = df.groupby([0, 1, 2]).mean()

    points = means.values
    return points


class MsgCache:

    def __init__(self, folder) -> None:

        self.folder = folder

        if not os.path.isdir(folder):
            print("Creating folder", folder)
            os.makedirs(folder, exist_ok=True)

        self.files = os.listdir(folder)

    def save(self, msg, filename):

        fname = os.path.basename(filename)

        with open(f'{self.folder}/{fname}', 'wb') as f:
            f.write(msg.SerializeToString())

        self.files.append(filename)

    def load(self, filename):

        fname = os.path.basename(filename)

        if not os.path.exists(f'{self.folder}/{fname}'):
            return None

        with open(f'{self.folder}/{fname}', 'rb') as f:
            return f.read()


if __name__ == "__main__":

    ar = np.random.random((1000000, 4)) * 10 + 10

    voxel_downsample(ar, 0.3)
