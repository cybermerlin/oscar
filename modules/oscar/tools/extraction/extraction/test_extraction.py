#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest

import yaml
import logging
import shutil
import os
import sys

import numpy as np

from extraction.extract_coordinates import main as extract_coordinates
from extraction.extract_pointclouds import main as extract_pointclouds
from extraction.extract_aligned_pointclouds import main as extract_aligned_pointclouds
from extraction.record_channel_filter import main as filter_record

sys.path.append('/apollo')

from cyber.python.cyber_py3 import record



class TestExtraction(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.script_dir = os.path.dirname(os.path.realpath(__file__))

        test_data_folder = "../../test_data"
        self.src_folder_localization = f'{self.script_dir}/{test_data_folder}/loc_control_record'

        self.lidars_src_folder = f'{self.script_dir}/{test_data_folder}/aligned_lidars_records'

        self.save_coords_file = f'{self.script_dir}/{test_data_folder}/out/coords_dump.txt'

        self.aligned_pcds_dst_folder = f'{self.script_dir}/{test_data_folder}/out/out_pcds_aligned'
        self.pcds_dst_folder = f'{self.script_dir}/{test_data_folder}/out/out_pcds'

        self.dest_filter_folder_black = f'{self.script_dir}/{test_data_folder}/out/filtered_record_black'
        self.dest_filter_folder_white = f'{self.script_dir}/{test_data_folder}/out/filtered_record_white'

        for i in [self.src_folder_localization, self.lidars_src_folder]:
            assert os.path.exists(i), f'{i} is missing. Maybe test data is not downloaded'

    def testCoordinates(self):

        if os.path.exists(self.save_coords_file):
            os.remove(self.save_coords_file)

        extract_coordinates(self.src_folder_localization,
                            self.save_coords_file)

        self.assertTrue(os.path.exists(self.save_coords_file))

        array = np.loadtxt(self.save_coords_file)

        self.assertEqual(
            len(array.shape), 2, f"Output file has not 2-dimensional array, - {array.shape}")
        self.assertEqual(
            array.shape[1], 3, f"Last dimension of the array is not equal to 3 (x, y, ts)")

    def testPointCloud(self):

        if os.path.exists(self.pcds_dst_folder):
            shutil.rmtree(self.pcds_dst_folder)

        pc_channel = '/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2'
        pose_fname = '0_poses.txt'

        extract_pointclouds(self.lidars_src_folder,
                            self.pcds_dst_folder,
                            pc_channel,
                            poses_fname=pose_fname)

        # check for presence of poses file
        self.assertTrue(
            any([pose_fname in f for f in os.listdir(self.pcds_dst_folder)]), "Output folder does not contain poses file")

        self.assertTrue(len(os.listdir(self.pcds_dst_folder)) >
                        1, "Output folder contains only poses file")

        # check for presence of all pcd files in output dir except of one poses file
        self.assertEqual(sum(['pcd' in f for f in os.listdir(
            self.pcds_dst_folder)]), len(os.listdir(self.pcds_dst_folder)) - 1, "Output folder has wrong files")

    def testAlignedPointclouds(self):

        if os.path.exists(self.aligned_pcds_dst_folder):
            shutil.rmtree(self.aligned_pcds_dst_folder)

        extract_aligned_pointclouds(self.lidars_src_folder,
                                    self.aligned_pcds_dst_folder,
                                    main_channel='/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2',
                                    secondary_channel='/apollo/sensor/lidar/rs_bpearl_left/compensator/PointCloud2'
                                    )

        self.assertTrue(len(os.listdir(self.aligned_pcds_dst_folder))
                        > 0, "Result folder is empty")

    def testRecordFilteringBlackList(self):

        save_folder = self.dest_filter_folder_black

        if os.path.exists(save_folder):
            shutil.rmtree(save_folder)

        black_list = [
            "/apollo/canbus/chassis",
            "/apollo/sensor/gnss/raw_data"
        ]

        filter_record(self.src_folder_localization, save_folder,
                      black_list_channels=black_list)

        self.assertTrue(len(os.listdir(save_folder))
                        > 0, "Output folder is empty")

        test_freader = record.RecordReader(
            f'{save_folder}/{os.listdir(save_folder)[0]}')

        channels = test_freader.get_channellist()

        self.assertFalse(any([ch in black_list for ch in channels]),
                         f'Blacklisted channels are in output folder: {channels}, black list: {black_list}')

    def testRecordFilteringWhiteList(self):

        save_folder = self.dest_filter_folder_white

        if os.path.exists(save_folder):
            shutil.rmtree(save_folder)

        white_list = [
            "/apollo/canbus/chassis",
            "/apollo/sensor/gnss/raw_data"
        ]

        filter_record(self.src_folder_localization, save_folder,
                      white_list_channels=white_list)

        self.assertTrue(len(os.listdir(save_folder))
                        > 0, "Output folder is empty")

        test_freader = record.RecordReader(
            f'{save_folder}/{os.listdir(save_folder)[0]}')

        channels = test_freader.get_channellist()

        self.assertTrue(set(white_list) == set(channels),
                        f'Whitelisted channels are not in output folder: {channels}, white list: {white_list}')


if __name__ == '__main__': # pragma: no cover
    unittest.main()
