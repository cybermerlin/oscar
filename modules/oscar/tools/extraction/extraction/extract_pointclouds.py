#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import sys
import time
import os
from datetime import datetime
from multiprocessing import Pool

import open3d as o3d
import numpy as np
from tqdm.auto import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from cyber.python.cyber_py3 import record
from modules.common_msgs.localization_msgs import localization_pb2

from extraction.utils import get_total_messages
from extraction.utils import NumpyPCProcessor, O3dPCDProcessor


def main(src_path, save_folder, pc_channel, poses_fname='0_poses.txt', save_numpy=True):

    files = []

    if os.path.isdir(src_path):
        files = [f'{src_path}/{f}' for f in os.listdir(src_path)]
    else:
        files = [src_path]

    assert os.path.exists(src_path), src_path

    os.makedirs(save_folder, exist_ok=True)
    poses_file = f'{save_folder}/{poses_fname}'
    with open(poses_file, 'w') as f:
        f.write('')

    assert len(files) > 0

    MessageProcessor = NumpyPCProcessor if save_numpy else O3dPCDProcessor

    total_idx = 0

    total_poses_data = np.empty((0, 9), dtype=np.float64)

    pool = Pool(processes=8)

    for idx, f in enumerate(sorted(files)):

        rfile = f

        freader = record.RecordReader(rfile)
        if pc_channel == 'auto':
            pc_channel = [a for a in freader.get_channellist()
                          if 'lidar' in a][0]

        poses_list = []
        pcs_list = []

        for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader)):

            if channelname == pc_channel:

                pcs_list.append(pool.apply_async(
                    MessageProcessor.parseMessage, args=(msg,)))
                # pcs_list.append((ts, pcd))

            elif channelname == '/apollo/localization/pose':
                msg_struct = localization_pb2.LocalizationEstimate()
                msg_struct.ParseFromString(msg)

                cur_time = datetime.fromtimestamp(
                    msg_struct.header.timestamp_sec)
                msecs = int((msg_struct.header.timestamp_sec % 1) * 1000)
                str_time = cur_time.strftime("%Y-%m-%d-%H-%M-%S")
                str_time = f'{str_time}-{msecs:03d}'

                pos = msg_struct.pose.position
                orient = msg_struct.pose.orientation

                poses_list.append(np.array(
                    [msg_struct.header.timestamp_sec, pos.x, pos.y, pos.z, orient.qx, orient.qy, orient.qz, orient.qw]))

        # match saved localizations and point clouds
        poses = np.stack(poses_list)

        poses_folder = f"{save_folder}/poses"
        os.makedirs(poses_folder, exist_ok=True)

        pcds_folder = f"{save_folder}/pointclouds"
        os.makedirs(pcds_folder, exist_ok=True)

        poses_data = np.empty((len(pcs_list), 9), dtype=np.float64)
        poses_file = f'{poses_folder}/gt_poses.txt'

        # for idx, (ts, pcd) in enumerate(tqdm(pcs_list)):
        for idx, pc_future in enumerate(tqdm(pcs_list)):

            ts, pcd = pc_future.get()
            closest_idx = (np.abs(poses[:, 0] - ts)).argmin()

            pos = poses[closest_idx, 1:4]
            orient = poses[closest_idx, 4:]

            poses_data[idx, 0] = total_idx
            poses_data[idx, 1] = ts
            poses_data[idx, 2:] = poses[closest_idx, 1:]

            MessageProcessor.save(pcd, f"{pcds_folder}/{total_idx:05}")

            total_idx += 1

        total_poses_data = np.concatenate([total_poses_data, poses_data])
        np.savetxt(poses_file, total_poses_data)

    pool.close()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/24_08_nissan/')
    parser.add_argument('-d', '--dest', type=str, help='a path to destination folder',
                        default='/apollo/scripts/oscar/cyber_scripts/extraction/pcds_infinity')
    parser.add_argument('-c', '--channel', type=str,
                        help='name of channel with pointclouds', default='auto')

    args = parser.parse_args()
    print(args)
    try:
        main(args.path, args.dest, args.channel)
    except KeyboardInterrupt:
        exit()
