#!/usr/bin/env python

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys

from multiprocessing import Pool

import numpy as np
from tqdm.auto import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from cyber.python.cyber_py3 import record
from modules.common_msgs.sensor_msgs import pointcloud_pb2

from extraction.utils import get_total_messages, voxel_downsample


def filter_msg(msg, voxel_size):

    points = np.array([[p.x, p.y, p.z, p.intensity, p.timestamp]
                      for p in msg.point])

    points_filtered = voxel_downsample(points, voxel_size)

    del msg.point[:]
    for p in points_filtered:
        msg.point.add(
            x=p[0], y=p[1], z=p[2], intensity=int(p[3]), timestamp=int(p[4]))


def process_file(read_file, save_file, channels, voxel_size, idx):

    freader = record.RecordReader(read_file)
    fwriter = record.RecordWriter()

    if not fwriter.open(save_file):
        print('Failed to open record writer!')
        return

    added_channels = []

    for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader), desc=os.path.basename(read_file), position=idx):

        if channelname not in added_channels:
            added_channels.append(channelname)
            protodesc = freader.get_protodesc(channelname)

            fwriter.write_channel(channelname, datatype, protodesc)

        if datatype == "apollo.drivers.PointCloud" and (len(channels) == 0 or channelname not in channels):
            msg_struct = pointcloud_pb2.PointCloud.FromString(msg)
            filter_msg(msg_struct, voxel_size)
            msg = msg_struct.SerializeToString()

        fwriter.write_message(
            channelname, msg, timestamp)

    fwriter.close()


def main(read_folder, save_folder, channels, voxel_size):

    results = []
    os.makedirs(save_folder, exist_ok=True)
    with Pool(processes=8) as p:
        for idx, f in enumerate(sorted(os.listdir(read_folder))):

            results.append(p.apply_async(
                process_file, args=(f"{read_folder}/{f}", f"{save_folder}/{f}", channels, voxel_size, idx)))

        [r.get() for r in results]


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/24_08_nissan/')
    parser.add_argument('-d', '--dest', type=str, help='a path to destination folder',
                        default='/apollo/modules/oscar/tools/data')
    parser.add_argument('-c', '--channels', nargs="*", type=str,
                        help='name of channels with pointclouds. Empty for all channels', default=[])
    parser.add_argument('-s', "--size", type=int,
                        help='voxel downsampling size', default=0.1)

    args = parser.parse_args()
    print(args)
    try:
        main(args.path, args.dest, args.channels, args.size)
    except KeyboardInterrupt:
        exit()
