#!/usr/bin/env python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

from glob import glob
import sys
import time
import argparse
import yaml

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber
from modules.oscar.foxglove.schemas.CameraCalibration_pb2 import CameraCalibration

def parse_yaml(filename) -> CameraCalibration:
    calibration = CameraCalibration()
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)

        calibration.frame_id = f_yaml['header']['frame_id']
        calibration.width = f_yaml['width']
        calibration.height = f_yaml['height']

        calibration.distortion_model = f_yaml['distortion_model']

        calibration.D.extend(f_yaml['D'])
        calibration.K.extend(f_yaml['K'])
        calibration.R.extend(f_yaml['R'])
        calibration.P.extend(f_yaml['P'])

    return calibration


class CameraCalibrationPublisher:

    def __init__(self, period, camera_info_files):

        cyber.init()

        self.period = period
        self.node = cyber.Node("CameraCalibrationPublisher")

        self.writers = []

        self.objects = []

        for f in camera_info_files:

            camera_info = parse_yaml(f)

            write_channel = f"/apollo/sensor/camera/{camera_info.frame_id}/CameraCalibration"

            print(
                f"Parsed file {f} for camera {camera_info.frame_id}. Pub channel: {write_channel}")

            self.objects.append(camera_info)
            self.writers.append(self.node.create_writer(
                write_channel, CameraCalibration))
    
    def __del__(self):
        cyber.shutdown()
    
    def spin(self):
        while cyber.ok():

            for writer, obj in zip(self.writers, self.objects):
                writer.write(obj)

            time.sleep(self.period)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--period', type=float, help='period between publishing obstacles',
                        default=10)
    parser.add_argument('-f', '--calibration_files', type=str, help='a path to static obstacles protobuf file', nargs="+",
                        default=glob("/apollo/modules/calibration/data/mb_actros/perception_conf/sensor_params/camera_*intrinsics.yaml"))

    args = parser.parse_args()
    print(args)

    tr = CameraCalibrationPublisher(args.period, args.calibration_files)
    tr.spin()
