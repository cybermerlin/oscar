#!/usr/bin/env python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import argparse
import google.protobuf.text_format as text_format

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from modules.common_msgs.perception_msgs import perception_obstacle_pb2


class FakePerception:

    def __init__(self, period, write_channel, static_obstacles_file):

        print(f"Reading from {static_obstacles_file}")

        cyber.init()

        self.period = period
        self.node = cyber.Node("FakeObstacleDetection")

        self.writer = self.node.create_writer(write_channel, perception_obstacle_pb2.PerceptionObstacles)
        self.obstacles = perception_obstacle_pb2.PerceptionObstacles()

        try:
            with open(static_obstacles_file, 'r') as f:
                text_format.Merge(f.read(), self.obstacles)

        except FileNotFoundError:
            print(
                f"Static perception file does not exist: {static_obstacles_file}")
        print(self.obstacles)


    def spin(self):
        while cyber.ok():
            
            current_time = time.time()
            for obstacle in self.obstacles.perception_obstacle:
                obstacle.timestamp = current_time
            
            self.obstacles.header.timestamp_sec = current_time

            self.writer.write(self.obstacles)
            time.sleep(1 / self.period)

    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--period', type=float, help='period between publishing obstacles',
                        default=1)
    parser.add_argument('-c', '--channel', type=str,
                        help='name of channel to publish', default='/apollo/perception/obstacles')
    parser.add_argument('-o', '--obstacles_file', type=str, help='a path to static obstacles protobuf file',
                        default="/apollo/modules/tools/oscar_tools/fake_perception/static_obstacles.pb.txt")

    args = parser.parse_args()
    print(args)
    tr = FakePerception(args.period, args.channel, args.obstacles_file)
    tr.spin()
