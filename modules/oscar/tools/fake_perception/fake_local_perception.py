#!/usr/bin/env python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
from threading import Lock
import time
import argparse
from copy import deepcopy
import numpy as np
from scipy.spatial.transform.rotation import Rotation as R
import google.protobuf.text_format as text_format

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from modules.common_msgs.perception_msgs import perception_obstacle_pb2
from modules.common_msgs.localization_msgs import localization_pb2


def parse_translation(localization_msg):
    return np.array([
        localization_msg.pose.position.x,
        localization_msg.pose.position.y,
        localization_msg.pose.position.z
        ])
    
def parse_rotation_matrix(localization_msg):
    return R.from_quat([
        localization_msg.pose.orientation.qx,
        localization_msg.pose.orientation.qy,
        localization_msg.pose.orientation.qz,
        localization_msg.pose.orientation.qw
    ]).as_matrix()
    
def parse_theta(localization_msg):
    return R.from_quat([
        localization_msg.pose.orientation.qx,
        localization_msg.pose.orientation.qy,
        localization_msg.pose.orientation.qz,
        localization_msg.pose.orientation.qw
    ]).as_euler("XYZ")[2]

def apply_transform(pos, theta, translation):
    return np.array([
        pos[0] + translation[0] * np.cos(theta) -translation[1] * np.sin(theta),
        pos[1] + translation[0] * np.sin(theta) + translation[1] * np.cos(theta),
        pos[2] + translation[2]
        ])

class FakePerception:

    def __init__(self, period, write_channel, static_obstacles_file):

        cyber.init()
        
        self.start_time = None
        self.obstacles_written = True

        self.obstacles = perception_obstacle_pb2.PerceptionObstacles()
    
        print(f"Reading from {static_obstacles_file}")
        try:
            with open(static_obstacles_file, 'r') as f:
                text_format.Merge(f.read(), self.obstacles)

        except FileNotFoundError:
            print(
                f"Static perception file does not exist: {static_obstacles_file}")
            
            exit(1)
            
        self.init_obstacles = deepcopy(self.obstacles)

        self.period = period
        self.node = cyber.Node("FakePredictionLocal")

        self.writer = self.node.create_writer(write_channel, perception_obstacle_pb2.PerceptionObstacles)
        
        
        self.localization_lock = Lock()
        self.localization = None
        self.localization_reader = self.node.create_reader("/apollo/localization/pose", localization_pb2.LocalizationEstimate, self.localization_callback)

        print(self.obstacles)
        
        


    def localization_callback(self, msg):
        with self.localization_lock:
            self.localization = deepcopy(msg)
            self.obstacles_written = False
            
            
    def spin(self):
        while cyber.ok():
            time.sleep(1 / self.period)
            
            if self.localization is None:
                continue
            
            if self.obstacles_written:
                continue
            
            with self.localization_lock:
                current_localization = deepcopy(self.localization)
            
            translation = parse_translation(current_localization)
            rotation_matrix = parse_rotation_matrix(current_localization)
            
            theta = parse_theta(current_localization)
            
            if self.start_time is None:
                self.start_time = time.time()
                
            current_time = time.time()
            tracking_time = current_time - self.start_time
            
            for init_obstacle, obstacle in zip(self.init_obstacles.perception_obstacle, self.obstacles.perception_obstacle):
                obstacle.timestamp = current_time
                obstacle.tracking_time = tracking_time
                
                local_position = np.array([
                    init_obstacle.position.x,
                    init_obstacle.position.y,
                    init_obstacle.position.z,
                ])
                
                new_position = rotation_matrix @ local_position + translation
                
                obstacle.position.x, obstacle.position.y, obstacle.position.z = new_position
                
                obstacle.theta = theta
                obstacle.velocity.x = current_localization.pose.linear_velocity.x
                obstacle.velocity.y = current_localization.pose.linear_velocity.y
                obstacle.velocity.z = current_localization.pose.linear_velocity.z
                
                apply_local_transform = lambda x, y, z: [
                    obstacle.position.x + x * np.cos(theta) - y * np.sin(theta),
                    obstacle.position.y + x * np.sin(theta) + y * np.cos(theta),
                    obstacle.position.z + z,
                ]
                
                corners = [
                    [-obstacle.width / 2, obstacle.length / 2, 0],
                    [-obstacle.width / 2, -obstacle.length / 2, 0],
                    [obstacle.width / 2, -obstacle.length / 2, 0],
                    [obstacle.width / 2, obstacle.length / 2, 0],
                ]
                
                del obstacle.polygon_point[:]
                for corner in corners:
                    point = obstacle.polygon_point.add()
                    point.x, point.y, point.z = apply_local_transform(*corner)
                
            
            self.obstacles.header.timestamp_sec = current_time
            
            self.writer.write(self.obstacles)
            self.obstacles_written = True

    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--period', type=float, help='period between publishing obstacles',
                        default=10)
    parser.add_argument('-c', '--channel', type=str,
                        help='name of channel to publish', default='/apollo/perception/obstacles')
    parser.add_argument('-o', '--obstacles_file', type=str, help='a path to static obstacles protobuf file',
                        default="/apollo/modules/oscar/tools/fake_perception/static_local_obstacles.pb.txt")

    args = parser.parse_args()
    print(args)
    tr = FakePerception(args.period, args.channel, args.obstacles_file)
    tr.spin()
