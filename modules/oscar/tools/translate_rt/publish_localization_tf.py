import sys

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.transform_msgs import transform_pb2


class TranslatorManager:

    def __init__(self):

        cyber.init()
        self.node = cyber.Node("tf_translator")

        self.write_channel = "/tf"

        for node in cyber.NodeUtils.get_nodes(sleep_s=1):
            if self.write_channel in cyber.NodeUtils.get_writersofnode(node, sleep_s=0):
                raise RuntimeError(
                    f"Channel /tf is already being published by node {node}")

        self.writer = self.node.create_writer(
            self.write_channel, transform_pb2.TransformStampeds)

        self.reader = self.node.create_reader(
            "/apollo/localization/pose", localization_pb2.LocalizationEstimate, self.cb)

        print("Init complete")

    def cb(self, msg):

        out_msg = transform_pb2.TransformStampeds()

        tf = out_msg.transforms.add()
        tf.header.timestamp_sec = cyber_time.Time.now().to_sec()
        tf.header.frame_id = "world"
        tf.child_frame_id = "localization"
        tf.transform.translation.x = msg.pose.position.x
        tf.transform.translation.y = msg.pose.position.y
        tf.transform.translation.z = msg.pose.position.z

        tf.transform.rotation.qx = msg.pose.orientation.qx
        tf.transform.rotation.qy = msg.pose.orientation.qy
        tf.transform.rotation.qz = msg.pose.orientation.qz
        tf.transform.rotation.qw = msg.pose.orientation.qw

        self.writer.write(out_msg)


if __name__ == "__main__":
    tr = TranslatorManager()
    tr.node.spin()
