
import os
import pickle
import signal
import sys
import time

import yaml

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from dacite import from_dict


from modules.drivers.gnss.proto import gnss_status_pb2
from modules.common_msgs.planning_msgs import sl_boundary_pb2
from modules.common_msgs.planning_msgs import planning_internal_pb2
from modules.common_msgs.planning_msgs import planning_pb2
from modules.common_msgs.planning_msgs import navigation_pb2
from modules.common_msgs.planning_msgs import pad_msg_pb2
from modules.common_msgs.planning_msgs import scenario_type_pb2
from modules.common_msgs.planning_msgs import decision_pb2
from modules.common_msgs.sensor_msgs import ultrasonic_radar_pb2
from modules.common_msgs.sensor_msgs import delphi_esr_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2
from modules.common_msgs.sensor_msgs import gnss_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.sensor_msgs import imu_pb2
from modules.common_msgs.sensor_msgs import mobileye_pb2
from modules.common_msgs.sensor_msgs import racobit_radar_pb2
from modules.common_msgs.sensor_msgs import conti_radar_pb2
from modules.common_msgs.sensor_msgs import smartereye_pb2
from modules.common_msgs.sensor_msgs import gnss_best_pose_pb2
from modules.common_msgs.sensor_msgs import ins_pb2
from modules.common_msgs.sensor_msgs import radar_pb2
from modules.common_msgs.sensor_msgs import heading_pb2
from modules.common_msgs.sensor_msgs import gnss_raw_observation_pb2
from modules.common_msgs.storytelling_msgs import story_pb2
from modules.common_msgs.drivers_msgs import can_card_parameter_pb2
from modules.common_msgs.guardian_msgs import guardian_pb2
from modules.common_msgs.localization_msgs import imu_pb2 as l_imu_pb2
from modules.common_msgs.localization_msgs import localization_status_pb2
from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.localization_msgs import gps_pb2
from modules.common_msgs.localization_msgs import pose_pb2
from modules.common_msgs.v2x_msgs import v2x_traffic_light_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2
from modules.common_msgs.control_msgs import pad_msg_pb2
from modules.common_msgs.control_msgs import input_debug_pb2
from modules.common_msgs.prediction_msgs import prediction_point_pb2
from modules.common_msgs.prediction_msgs import feature_pb2
from modules.common_msgs.prediction_msgs import scenario_pb2
from modules.common_msgs.prediction_msgs import prediction_obstacle_pb2
from modules.common_msgs.prediction_msgs import lane_graph_pb2
from modules.common_msgs.simulation_msgs import grading_metric_pb2
from modules.common_msgs.simulation_msgs import scenario_pb2
from modules.common_msgs.simulation_msgs import grading_condition_pb2
from modules.common_msgs.simulation_msgs import agent_pb2
from modules.common_msgs.dreamview_msgs import hmi_status_pb2
from modules.common_msgs.dreamview_msgs import chart_pb2
from modules.common_msgs.task_manager_msgs import task_manager_pb2
from modules.common_msgs.map_msgs import map_clear_area_pb2
from modules.common_msgs.map_msgs import map_signal_pb2
from modules.common_msgs.map_msgs import map_crosswalk_pb2
from modules.common_msgs.map_msgs import map_geometry_pb2
from modules.common_msgs.map_msgs import map_pnc_junction_pb2
from modules.common_msgs.map_msgs import map_id_pb2
from modules.common_msgs.map_msgs import map_overlap_pb2
from modules.common_msgs.map_msgs import map_road_pb2
from modules.common_msgs.map_msgs import map_stop_sign_pb2
from modules.common_msgs.map_msgs import map_pb2
from modules.common_msgs.map_msgs import map_speed_control_pb2
from modules.common_msgs.map_msgs import map_rsu_pb2
from modules.common_msgs.map_msgs import map_yield_sign_pb2
from modules.common_msgs.map_msgs import map_parking_space_pb2
from modules.common_msgs.map_msgs import map_junction_pb2
from modules.common_msgs.map_msgs import map_speed_bump_pb2
from modules.common_msgs.map_msgs import map_lane_pb2
from modules.common_msgs.perception_msgs import perception_obstacle_pb2
from modules.common_msgs.perception_msgs import perception_camera_pb2
from modules.common_msgs.perception_msgs import perception_lane_pb2
from modules.common_msgs.perception_msgs import traffic_light_detection_pb2
from modules.common_msgs.routing_msgs import poi_pb2
from modules.common_msgs.routing_msgs import routing_pb2
from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.monitor_msgs import monitor_log_pb2
from modules.common_msgs.monitor_msgs import smart_recorder_status_pb2
from modules.common_msgs.monitor_msgs import system_status_pb2
from modules.common_msgs.audio_msgs import audio_event_pb2
from modules.common_msgs.audio_msgs import audio_common_pb2
from modules.common_msgs.audio_msgs import audio_pb2
from modules.common_msgs.config_msgs import vehicle_config_pb2
from modules.common_msgs.basic_msgs import drive_event_pb2
from modules.common_msgs.basic_msgs import pnc_point_pb2
from modules.common_msgs.basic_msgs import vehicle_id_pb2
from modules.common_msgs.basic_msgs import direction_pb2
from modules.common_msgs.basic_msgs import header_pb2
from modules.common_msgs.basic_msgs import vehicle_signal_pb2
from modules.common_msgs.basic_msgs import drive_state_pb2
from modules.common_msgs.basic_msgs import error_code_pb2
from modules.common_msgs.basic_msgs import geometry_pb2
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.chassis_msgs import chassis_detail_pb2
from modules.common_msgs.chassis_msgs import chassis_detail_pb2

from modules.oscar.foxglove.schemas import ArrowPrimitive_pb2
from modules.oscar.foxglove.schemas import CameraCalibration_pb2
from modules.oscar.foxglove.schemas import CircleAnnotation_pb2
from modules.oscar.foxglove.schemas import Color_pb2
from modules.oscar.foxglove.schemas import CompressedImage_pb2
from modules.oscar.foxglove.schemas import CubePrimitive_pb2
from modules.oscar.foxglove.schemas import CylinderPrimitive_pb2
from modules.oscar.foxglove.schemas import FrameTransform_pb2
from modules.oscar.foxglove.schemas import FrameTransforms_pb2
from modules.oscar.foxglove.schemas import GeoJSON_pb2
from modules.oscar.foxglove.schemas import Grid_pb2
from modules.oscar.foxglove.schemas import ImageAnnotations_pb2
from modules.oscar.foxglove.schemas import KeyValuePair_pb2
from modules.oscar.foxglove.schemas import LaserScan_pb2
from modules.oscar.foxglove.schemas import LinePrimitive_pb2
from modules.oscar.foxglove.schemas import LocationFix_pb2
from modules.oscar.foxglove.schemas import Log_pb2
from modules.oscar.foxglove.schemas import ModelPrimitive_pb2
from modules.oscar.foxglove.schemas import PackedElementField_pb2
from modules.oscar.foxglove.schemas import Point2_pb2
from modules.oscar.foxglove.schemas import Point3_pb2
from modules.oscar.foxglove.schemas import PointCloud_pb2
from modules.oscar.foxglove.schemas import PointsAnnotation_pb2
from modules.oscar.foxglove.schemas import PoseInFrame_pb2
from modules.oscar.foxglove.schemas import Pose_pb2
from modules.oscar.foxglove.schemas import PosesInFrame_pb2
from modules.oscar.foxglove.schemas import Quaternion_pb2
from modules.oscar.foxglove.schemas import RawImage_pb2
from modules.oscar.foxglove.schemas import SceneEntityDeletion_pb2
from modules.oscar.foxglove.schemas import SceneEntity_pb2
from modules.oscar.foxglove.schemas import SceneUpdate_pb2
from modules.oscar.foxglove.schemas import SpherePrimitive_pb2
from modules.oscar.foxglove.schemas import TextAnnotation_pb2
from modules.oscar.foxglove.schemas import TextPrimitive_pb2
from modules.oscar.foxglove.schemas import TriangleListPrimitive_pb2
from modules.oscar.foxglove.schemas import Vector2_pb2
from modules.oscar.foxglove.schemas import Vector3_pb2


def find_message_type_by_str(str_type):
    basename = str_type.split('.')[-1]

    for m in sys.modules.values():
        if basename in dir(m):
            return getattr(m, basename)


def read_config(fname, t):
    with open(fname, 'r') as f:
        yaml_f = yaml.safe_load(f)

        return from_dict(t, yaml_f)


class ProcessChecker:
    def __init__(self, channel, nodename) -> None:

        self.channel = channel
        self.nodename = nodename

    def is_initialized(self):
        active_nodes = cyber.NodeUtils.get_nodes(sleep_s=0)
        if not self.nodename in active_nodes:
            return False
        writers = cyber.NodeUtils.get_writersofnode(
            self.nodename, sleep_s=0)
        return self.channel in writers


def sigint_handler(signum, frame):
    cyber.shutdown()
    raise KeyboardInterrupt


def init_cyber():
    cyber.init()
    signal.signal(signal.SIGINT, sigint_handler)


def play_data(data_provider, process_manager, data_saver, main_src_channel, result_channel, ):

    prev_msg_ts = 0
    sleep_t = 0
    main_msg_counter = 0
    prev_publish_ts = time.time()

    pbar = tqdm(total=self.record_duration,
                bar_format="{l_bar}{bar}{n:.2f}/{total:.2f} sec [{elapsed}<{remaining}, {rate_fmt}{postfix}]")

    for channelname, msg, datatype, timestamp in data_provider.get_message_pool():

        if not process_manager.check_processes():
            return

        cur_msg_ts = timestamp / 1e9
        msg_ts_offset = cur_msg_ts - prev_msg_ts if prev_msg_ts > 0 else 0

        publish_ts_offset = time.time() - prev_publish_ts

        prev_msg_ts = cur_msg_ts
        prev_publish_ts = time.time()

        sleep_t += msg_ts_offset - publish_ts_offset
        time.sleep(max(0, sleep_t))

        if channelname == self.main_src_channel:
            main_msg_counter += 1

        data_provider.publish_msg(channelname, msg)

        data_saver.save(
            msg, channelname, datatype=datatype, timestamp=timestamp)
        pbar.set_description(
            f'Main msgs: {main_msg_counter}, output msgs: {data_saver.collected_msgs[self.result_channel]}')
        pbar.update(msg_ts_offset)

    pbar.close()


def load_cache(cache_file, cache_meta, cfg):

    if not os.path.exists(cache_file) or not os.path.exists(cache_meta):
        return None

    with open(cache_meta, 'rb') as f:
        meta_data = pickle.load(f)

    if meta_data != cfg:
        # print("no match in cache")
        return None

    with open(cache_file, 'rb') as f:
        return pickle.load(f)


def save_cache(cache_file, cache_meta, cfg, data):

    basedir = os.path.dirname(cache_file)
    if not basedir == "":
        os.makedirs(basedir, exist_ok=True)

    with open(cache_meta, 'wb') as f:
        pickle.dump(cfg, f)

    with open(cache_file, 'wb') as f:
        pickle.dump(data, f)
