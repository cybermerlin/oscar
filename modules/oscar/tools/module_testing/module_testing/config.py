from dataclasses import dataclass
import sys
from typing import List


@dataclass
class SourceDataProviderConfig:
    source_folder: str
    channels: List[str]
    start_time_offset: int = 0
    end_time_offset: int = sys.maxsize


@dataclass
class ResultSaverConfig:
    folder: str
    filename: str
    additional_save_channels: List[str]


@dataclass
class ProcessInfo:
    name: str
    command: str


@dataclass
class ProcessesManagerConfig:
    output_node: str
    processes: List[ProcessInfo]
    log_folder: str


@dataclass
class TestingConfig:
    source_data_provider: SourceDataProviderConfig
    processes_manager: ProcessesManagerConfig
    result_saver: ResultSaverConfig
    save_src_channels: bool

    result_channel: str
    main_src_channel: str
