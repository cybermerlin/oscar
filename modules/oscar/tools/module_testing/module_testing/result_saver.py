#!/apollo/modules/oscar/tools/module_testing/.venv/bin/python3

import asyncio
import copy
import logging
import os
import sys
import time

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time

from mcap_protobuf.writer import Writer

from utils import find_message_type_by_str, read_config

from threading import Lock
from config import ResultSaverConfig


class ProxyRawMessage():
    def __init__(self, raw_msg) -> None:
        self.raw_msg = raw_msg

    def SerializeToString(self):
        return self.raw_msg


def find_channel_type(channel):

    for n in cyber.NodeUtils.get_nodes(sleep_s=0):
        for ch in cyber.NodeUtils.get_writersofnode(n, sleep_s=0):
            if ch == channel:
                return find_message_type_by_str(cyber.ChannelUtils.get_msgtype(channel, sleep_s=0).decode())


async def scan_channel_type_async(channel):

    while cyber.ok():
        t = find_channel_type(channel)
        if t is not None:
            return t
        await asyncio.sleep(0.2)

        print("Waiting channel", channel)


def scan_channel_type(channel):
    while cyber.ok():
        t = find_channel_type(channel)
        if t is not None:
            return t
        time.sleep(0.2)

        print("waiting channel", channel)


class ResultSaver:
    def __init__(self, node: cyber.Node, cfg: ResultSaverConfig, first_record_ts=cyber_time.Time.now().to_nsec()):

        self.cfg = copy.deepcopy(cfg)

        # self.node = node
        self.node = cyber.Node("ResultSaver")

        self.channels = self.cfg.additional_save_channels

        self.logger = logging.getLogger("test")

        os.makedirs(self.cfg.folder, exist_ok=True)

        self.writer = Writer(
            f'{self.cfg.folder}/{self.cfg.filename}', compression=None)

        self.readers = []

        self.channel_types = {}

        self.channel_types_proxy = {}

        self.last_history_ts = first_record_ts
        self.last_rt_ts = time.time_ns()
        self.save_lock = Lock()
        self.collected_msgs = {ch: 0 for ch in self.channels}

        # asyncio.run(self.init_readers_async())
        # self.init_readers()

    def init_readers(self):
        for channel in self.channels:
            self.logger.debug(f"Waiting reader: {channel}")
            channel_type = scan_channel_type(channel)

            self.channel_types_proxy[channel] = type(f"Proxy{channel_type.__name__}", (ProxyRawMessage,), {
                                                     "DESCRIPTOR": channel_type.DESCRIPTOR})

            self.readers.append(self.node.create_reader(
                channel, channel_type, self.callback, args=channel))

    async def init_readers_async(self):
        for channel in self.channels:
            self.logger.debug(f"Waiting reader: {channel}")
            channel_type = await scan_channel_type_async(channel)

            self.channel_types[channel] = channel_type
            self.channel_types_proxy[channel] = type(f"Proxy{channel_type.__name__}", (ProxyRawMessage,), {
                                                     "DESCRIPTOR": channel_type.DESCRIPTOR})


            self.readers.append(self.node.create_rawdata_reader(
                channel, self.callback, args=channel))

    def __del__(self):
        self.writer.finish()
        self.logger.debug(f"{self.__class__.__name__} destroyed")

    def add_channel(self, channel):
        if channel not in self.channels:

            self.channel_types[channel] = find_channel_type(
                channel)

            self.channel_types_proxy[channel] = type(f"Proxy{self.channel_types[channel].__name__}", (ProxyRawMessage,), {
                "DESCRIPTOR": self.channel_types[channel].DESCRIPTOR})

            self.channels.append(channel)
            self.collected_msgs[channel] = 0

    def callback(self, msg, channel):
        with self.save_lock:

            if channel not in self.channel_types:
                self.add_channel(channel)

            timestamp = self.last_history_ts + time.time_ns() - self.last_rt_ts

            self.last_history_ts = timestamp
            self.last_rt_ts = time.time_ns()

            if "tf" in channel:
                ts_sec = (self.last_history_ts +
                          time.time_ns() - self.last_rt_ts) / 1e9

                msg = self.channel_types[channel].FromString(msg)

                if hasattr(msg, "header") and hasattr(msg.header, "timestamp_sec"):
                    msg.header.timestamp_sec = ts_sec
                if hasattr(msg, "timestamp_sec"):
                    msg.timestamp_sec = ts_sec
                if hasattr(msg, "transform"):
                    for transform in msg.transform:
                        transform.header.timestamp_sec = ts_sec

                self.writer.write_message(topic=channel,
                                          message=msg,
                                          log_time=timestamp,
                                          publish_time=timestamp
                                          )
            else:

                self.writer.write_message(topic=channel,
                                          #   message=msg,
                                          message=self.channel_types_proxy[channel](
                                              msg),
                                          log_time=timestamp,
                                          publish_time=timestamp
                                          )

            self.collected_msgs[channel] += 1


    def save(self, msg, channel, datatype=None, timestamp=None):

        with self.save_lock:
            self.add_channel(channel)

            proxy_msg = self.channel_types_proxy[channel](msg)

            self.last_history_ts = timestamp
            self.last_rt_ts = time.time_ns()

            self.writer.write_message(topic=channel,
                                      message=proxy_msg,
                                      # log_time=time.time_ns() - self.ts_offset,
                                      # publish_time=time.time_ns() - self.ts_offset
                                      log_time=timestamp,
                                      publish_time=timestamp
                                      )

            self.collected_msgs[channel] += 1


if __name__ == "__main__":

    cfg = read_config(
        "/apollo/modules/oscar/tools/module_testing/saver_config.yaml", ResultSaverConfig)

    cyber.init()
    node = cyber.Node("ResultSaver")
    saver = ResultSaver(node, cfg)

    node.spin()

    saver.logger.debug(saver.collected_msgs)

    raise KeyboardInterrupt
