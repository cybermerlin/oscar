#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import os
from datetime import datetime
import argparse

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from cyber.python.cyber_py3 import record
from modules.common_msgs.control_msgs import control_cmd_pb2


ESTOP_BRAKE_VALS = [20, 30, 50]


class ControlChecker:

    def __init__(self) -> None:
        self.last_brake = 0
        self.start_estop_time = 0
        self.first_time = None

    def update(self, msg_struct=None, timestamp=None):

        if msg_struct is not None:
            cur_time = msg_struct.header.timestamp_sec
            cur_brake = msg_struct.brake
        else:
            cur_brake = 0
        if cur_brake in ESTOP_BRAKE_VALS and self.last_brake not in ESTOP_BRAKE_VALS:
            time_diff = (datetime.fromtimestamp(cur_time) -
                         self.first_time).total_seconds()
            print()
            print(
                f'Emergency stop at {datetime.fromtimestamp(cur_time)}, brake value: {cur_brake} ({time_diff:.1f} sec)')
            print(msg_struct.header.status)

        self.last_brake = cur_brake


def main(src_folder):

    print(f'Checking folder: {src_folder}')
    checker = ControlChecker()

    start_time = None

    for idx, f in enumerate(sorted(os.listdir(src_folder))):
        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        time.sleep(.1)

        for channelname, msg, datatype, timestamp in freader.read_messages():
            if start_time is None:
                start_time = datetime.fromtimestamp(timestamp / 10**9)
                checker.first_time = start_time
            if channelname == '/apollo/control':

                msg_struct = control_cmd_pb2.ControlCommand()
                msg_struct.ParseFromString(msg)

                checker.update(msg_struct, timestamp)

    checker.update()

    print(f'Record started {start_time}')
    print(f'Record ended   {datetime.fromtimestamp(timestamp / 10**9)}')


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2022-08-11-13-16-59')
    parser.add_argument('-v', '--value', nargs='+', type=float,
                        help='list of possible values for emergency stop brake', default=[20, 30])
    args = parser.parse_args()

    ESTOP_BRAKE_VALS = args.value

    try:
        main(args.path)
    except KeyboardInterrupt:
        exit()
