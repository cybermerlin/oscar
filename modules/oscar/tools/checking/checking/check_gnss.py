#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import os
from datetime import datetime
import argparse

from tqdm import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record
from modules.common_msgs.sensor_msgs import gnss_best_pose_pb2

# Full list of statuses but only few of them were observed
# https://docs.novatel.com/OEM7/Content/Logs/BESTPOS.htm?tocpath=Commands%20%2526%20Logs%7CLogs%7CGNSS%20Logs%7C_____20#Position_VelocityType

status_desc_dict = {
    "SINGLE": "No corrections from server received",
    'PSRDIFF': "Solution with differential corrections (worse than RTK but with corrections)",
    'NARROW_FLOAT': "Big deviations in solution",
    "NONE": "No signal from satellites",
    "INS_RTKFIXED": "The best status, where RTK solution is the most precise and INS fused"
}

GOOD_SOLUTION = ["NARROW_INT", "INS_RTKFIXED"]


class GnssChecker:

    def __init__(self) -> None:
        self.last_type = GOOD_SOLUTION[0]
        self.start_incorrect_time = 0
        self.last_time = 0

    def update(self, msg_struct=None, timestamp=None):

        if msg_struct is not None:
            sol_type = gnss_best_pose_pb2._SOLUTIONTYPE.values_by_number[msg_struct.sol_type].name
            cur_time = msg_struct.measurement_time
            cur_time = timestamp / 10**9

            # if cur_time - self.last_time > 0.15:
            #     d = cur_time - self.last_time
            #     print(
            #         f"Big time diff between messages. {datetime.fromtimestamp(cur_time)} - {d:.3f} s")
        else:
            sol_type = GOOD_SOLUTION[0]
            cur_time = self.last_time

        if sol_type not in GOOD_SOLUTION and self.last_type in GOOD_SOLUTION:
            print()
            print(sol_type, ' - ', status_desc_dict[sol_type])
            print('Start: ', sol_type, datetime.fromtimestamp(cur_time))
            self.start_incorrect_timestart_incorrect_time = cur_time

        elif sol_type not in GOOD_SOLUTION and self.last_type not in GOOD_SOLUTION and sol_type != self.last_type:
            print(
                f'       Status changed: {self.last_type} -> {sol_type} at {datetime.fromtimestamp(cur_time)}')

        elif (sol_type in GOOD_SOLUTION and self.last_type not in GOOD_SOLUTION):
            dur = cur_time - self.start_incorrect_timestart_incorrect_time
            print('End: ', datetime.fromtimestamp(cur_time))
            print(f'Duration: {dur:.2f} sec')

        self.last_type = sol_type
        self.last_time = cur_time


def main(src_folder):

    print(f'Checking folder: {src_folder}')
    gnss_checker = GnssChecker()

    start_time = None

    for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):
        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        time.sleep(.1)

        for channelname, msg, datatype, timestamp in freader.read_messages():
            if start_time is None:
                start_time = datetime.fromtimestamp(timestamp / 10**9)
            if channelname == '/apollo/sensor/gnss/best_pose':

                msg_struct = gnss_best_pose_pb2.GnssBestPose()
                msg_struct.ParseFromString(msg)

                gnss_checker.update(msg_struct, timestamp)

    gnss_checker.update()

    print(f'Record started {start_time}')
    print(f'Record ended   {datetime.fromtimestamp(timestamp / 10**9)}')


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path',
                        type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2022-08-11-13-16-59')
    args = parser.parse_args()

    print(args)
    try:
        # main('data/bag/2022-07-29-11-21-07/')
        # main('data/bag/ilya_first/')
        # main('data/bag/ilya_second/')
        # main('data/bag/2022-07-29-14-53-53')
        main(args.path)
    except KeyboardInterrupt:
        exit()
