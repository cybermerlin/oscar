#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys

sys.path.append("/apollo/bazel-bin")

from modules.common_msgs.localization_msgs import localization_pb2, imu_pb2 as l_imu_pb2, gps_pb2
from modules.common_msgs.sensor_msgs import gnss_best_pose_pb2
from modules.drivers.gnss.proto import gnss_status_pb2
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.sensor_msgs import imu_pb2
from modules.common_msgs.planning_msgs import planning_pb2
from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.dreamview_msgs import hmi_status_pb2


channel_types = {
    'apollo.drivers.gnss.GnssBestPose': gnss_best_pose_pb2.GnssBestPose,
    'apollo.drivers.gnss.GnssStatus': gnss_status_pb2.GnssStatus,
    'apollo.drivers.gnss.InsStatus': gnss_status_pb2.InsStatus,
    'apollo.localization.LocalizationEstimate': localization_pb2.LocalizationEstimate,
    'apollo.localization.LocalizationStatus': localization_pb2.LocalizationStatus,
    'apollo.canbus.Chassis': chassis_pb2.Chassis,
    'apollo.control.ControlCommand': control_cmd_pb2.ControlCommand,
    'apollo.drivers.PointCloud': pointcloud_pb2.PointCloud,
    'apollo.drivers.gnss.Imu': imu_pb2.Imu,
    'apollo.localization.CorrectedImu': l_imu_pb2.CorrectedImu,
    'apollo.planning.ADCTrajectory': planning_pb2.ADCTrajectory,
    'apollo.transform.TransformStampeds': transform_pb2.TransformStampeds,
    'apollo.localization.Gps': gps_pb2.Gps,
    'apollo.dreamview.HMIStatus': hmi_status_pb2.HMIStatus
}


def get_total_messages(freader):
    return sum(freader.get_messagenumber(channel) for channel in freader.get_channellist())
