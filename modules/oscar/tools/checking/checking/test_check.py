#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest
import os
import logging

from checking.check_estop import main as estopMain
from checking.check_gnss import main as gnssMain

logging.basicConfig(level=logging.INFO)


class TestChecking(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.src_folder = f'{script_dir}/../../test_data/loc_control_records'
        assert os.path.exists(self.src_folder), f'{self.src_folder} is missing. Maybe test data is not downloaded'

    def testEStop(self):
        estopMain(self.src_folder)

    def testGnssStatus(self):
        gnssMain(self.src_folder)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
