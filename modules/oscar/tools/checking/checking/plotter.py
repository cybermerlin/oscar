#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import json
import sys
import time
import os
from datetime import datetime, timedelta
import logging


from tqdm import tqdm
import numpy as np
import matplotlib
from humps import camelize, decamelize
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
import matplotlib.style as mplstyle
from google.protobuf.json_format import MessageToDict


sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, record

from checking.utils import channel_types


matplotlib.use("tkagg")
plt.rcParams['axes.grid'] = True
plt.rcParams["path.simplify"] = True
plt.rcParams['path.simplify_threshold'] = 1.0
mplstyle.use(['fast'])


vfrom_ts = np.vectorize(datetime.utcfromtimestamp)


def get_msg_struct(msg, datatype):
    msg_struct = channel_types[datatype]()
    msg_struct.ParseFromString(msg)
    return msg_struct


def flatten_gen(pyobj, keystring=''):
    if type(pyobj) == dict:
        keystring = keystring + '.' if keystring else keystring
        for k in pyobj:
            yield from flatten_gen(pyobj[k], keystring + str(k))
    else:
        yield keystring, pyobj


def flatten_dict(d):
    return {k: v for k, v in flatten_gen(d)}


def remove_shared_axis(axs):
    axs.get_shared_x_axes().remove(axs)
    axs.xaxis.major = matplotlib.axis.Ticker()
    axs.xaxis.set_major_locator(matplotlib.ticker.AutoLocator())
    axs.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())


class ChannelPlotter:

    # def __init__(self, channels=['/apollo/sensor/gnss/best_pose'],
    def __init__(self, channels=['/apollo/sensor/gnss/best_pose'],
                 ts_field=None,
                 src_folder=None,
                 use_cached=False,
                 fill_between_field=None,
                 cache_dir=None) -> None:

        self.data = {}
        self.ts_field = ts_field if ts_field is not None else "header.timestamp_sec"
        self.msg_fields = []

        self.channels = channels if isinstance(channels, list) else [
            channels] if channels is not None else None

        self.ts_camelized = camelize(
            ts_field) if ts_field is not None else None

        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.save_dir = f'{script_dir}/cached_files' if cache_dir is None else cache_dir
        self.save_file = '_'.join([a.split('/')[-1] for a in self.channels])

        self.use_cached = use_cached

        if src_folder is not None:
            self.load_data(src_folder)

        self.fill_between_field = fill_between_field

        self.max_points = 1e4

    def update(self, channel, msg, datatype, timestamp):

        if self.channels is not None:
            if channel not in self.channels:
                return

        msg_struct = get_msg_struct(msg, datatype)
        msg_dict = flatten_dict(MessageToDict(msg_struct))
        if self.ts_camelized in msg_dict:
            if msg_dict[self.ts_camelized] == 0:
                return

        new_keys = []
        for key, val in msg_dict.items():
            key = decamelize(key)
            if key not in self.data:
                self.data[key] = []
            new_keys.append(key)
            self.data[key].append(val)
        self.msg_fields.append(new_keys)

    def save_dict_to_file(self, fname):
        os.makedirs(self.save_dir, exist_ok=True)

        with open(fname, 'w') as f:
            json.dump([self.data, self.msg_fields], f)

    def load_data(self, src_folder):
        print("Loading data")
        save_prefix = src_folder.split(
            '/')[-1] if src_folder.split('/')[-1] != '' else src_folder.split('/')[-2]

        cache_file = f'{self.save_dir}/{save_prefix}_{self.save_file}.json'
        if self.use_cached and os.path.exists(cache_file):
            print("Using cached file")
            with open(cache_file, 'r') as f:
                self.data, self.msg_fields = json.load(f)
        else:
            for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):

                rfile = src_folder + '/' + f
                freader = record.RecordReader(rfile)
                time.sleep(.1)

                for channelname, msg, datatype, timestamp in (freader.read_messages()):
                    self.update(channelname, msg, datatype, timestamp)

            print(f"Saving data to json: {cache_file}")
            self.save_dict_to_file(cache_file)

        # print duration of records
        dur = self.data[self.ts_field][-1] - self.data[self.ts_field][0]

        print(
            f"Record starts: {datetime.fromtimestamp(self.data[self.ts_field][0])}")
        print(
            f"Record ends: {datetime.fromtimestamp(self.data[self.ts_field][-1])}")
        print(f"Duration of records: {timedelta(seconds=int(dur))}")

        # print number of points by each field
        print(f'Collected points:')
        for key, val in self.data.items():
            print(f'  {key}: {len(val)}')

        min_ts = min(self.data[self.ts_field])
        self.data[self.ts_field] = [
            a - min_ts for a in (self.data[self.ts_field])]

    def plot(self, plots):

        self.fig, axs = plt.subplots(nrows=int(np.ceil(len(plots) / 2)),
                                     ncols=2,
                                     figsize=(19.2, 10.8),
                                     constrained_layout=True,
                                     sharex='all',
                                     squeeze=False
                                     )

        for ax_idx, plot_kwargs in tqdm(zip(np.arange(axs.size), plots), total=len(plots), desc='Plotting'):
            plot_func = getattr(self, plot_kwargs['type'])
            plot_kwargs.pop('type')
            plot_func(axs[ax_idx // 2, ax_idx % 2], **plot_kwargs)

        plt.show()

    def plot_coordinates(self, axs, data_fields=['pose.position.x', 'pose.position.y'], ts=None, drop_zero=True):

        remove_shared_axis(axs)

        x, y = data_fields
        indxs = np.array(self.data[ts]) > 1e9 if ts is not None else np.ones_like(
            self.data[x], dtype=bool)

        xs = np.array(self.data[x])[indxs]
        ys = np.array(self.data[y])[indxs]

        if drop_zero:
            xs = xs[xs > 0]
            ys = ys[ys > 0]

        axs.plot(xs, ys)

        axs.set_aspect('equal')
        axs.set_adjustable('box')
        axs.set_box_aspect(True)

        axs.set_title('Coordinates')

    def plot_vals(self, axs, data_fields=[], title=None, ts_field=None):

        # return X ticks after sharex=True
        axs.tick_params(
            axis='x',           # changes apply to the x-axis
            which='both',       # both major and minor ticks are affected
            bottom=True,
            top=False,
            labelbottom=True)

        if title is None:
            title = data_fields[0]

        if not isinstance(data_fields, list):
            data_fields = [data_fields]

        ts = None
        if ts_field is not None:
            ts = vfrom_ts(self.data[ts_field])
            axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        elif self.ts_field is not None:
            ts = vfrom_ts(self.data[self.ts_field])
            axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        for field in data_fields:
            xs = ts[[field in fields for fields in self.msg_fields]
                    ] if ts is not None else np.arange(len(self.data[field]))

            step = int(len(xs) // self.max_points if len(xs)
                       > self.max_points else 1)
            axs.plot(xs[::step], self.data[field][::step], '.-', markevery=10)

        axs.set_title(title)
        axs.legend(data_fields)

        if self.fill_between_field is not None:
            self.fill_between(axs)

    def plot_hists(self, axs, data_fields=[], title=None):

        remove_shared_axis(axs)

        if title is None:
            title = data_fields[0]

        for key in (data_fields):
            val = np.array(self.data[key])
            axs.hist(val, bins='fd', alpha=0.5)
        axs.set_title(title)

    def plot_differences(self, axs, data_fields, title=None, ts_field=None, plot_type='line'):

        if title is None:
            title = 'Time differences'
        if plot_type == 'line':
            if ts_field is not None:
                ts = vfrom_ts(self.data[ts_field][1:])
                axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
            elif self.ts_field is not None:
                ts = vfrom_ts(self.data[self.ts_field][1:])
                axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        else:
            remove_shared_axis(axs)

        for field in (data_fields):
            val = np.array(self.data[field])
            diff = val[1:] - val[:-1]

            if plot_type == 'line':
                xs = ts[[field in fields for fields in self.msg_fields[1:]]
                        ] if ts is not None else np.arange(len(diff))
                axs.plot(xs[1:], diff)
            elif plot_type == 'hist':
                axs.hist(diff, bins='fd', alpha=0.5)
            else:
                logging.warn(
                    f"Wrong Differences plot type: {plot_type}. Possible are 'line' and 'hist' ")

        axs.legend(data_fields)
        axs.set_title(title)

    def fill_between(self, axs):

        if self.ts_field is not None:
            ts = vfrom_ts(self.data[self.ts_field])

        ts = ts[[self.fill_between_field in fields for fields in self.msg_fields]]

        change_tss = [ts[0]]
        seq_datum = [self.data[self.fill_between_field][0]]

        for prev_datum, datum, cur_ts in zip(self.data[self.fill_between_field][1:], self.data[self.fill_between_field][:-1], ts[1:]):
            if datum != prev_datum:
                change_tss.append(cur_ts)
                seq_datum.append(datum)

        change_tss.append(cur_ts)
        seq_datum.append(datum)

        np.random.seed(1)
        colors = {key: np.random.rand(3) for key in set(seq_datum)}
        for prev_ts, cur_ts, prev_datum, datum in zip(change_tss[:-1], change_tss[1:], seq_datum[:-1], seq_datum[1:]):
            axs.axvspan(prev_ts, cur_ts, color=colors[datum], alpha=0.2)
