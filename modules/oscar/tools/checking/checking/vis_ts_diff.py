#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import os
import argparse
from typing import Iterable

from tqdm import tqdm
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.dates as mdates


sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record
from checking.plotter import vfrom_ts

matplotlib.use("tkagg")
plt.rcParams['axes.grid'] = True


class Checker:

    def __init__(self, channels) -> None:

        self.channels = channels
        self.data = {}

    def update(self, timestamp, channel):

        if all(saved_channel not in channel for saved_channel in self.channels):
            return

        if channel not in self.data.keys():
            self.data[channel] = []

        self.data[channel].append(timestamp / 10**9)

    def plot(self):

        print(f'Collected points:')
        for key, val in self.data.items():
            print(f'  {key}: {len(val)}')

        plots = [
            self.plot_differences,
            self.plot_hists,
            self.plot_avg_msecs
        ]

        fig, axs = plt.subplots(nrows=len(plots), figsize=(
            12.8, 7.2), constrained_layout=True)

        if not isinstance(axs, Iterable):
            axs = [axs]
        for ax, plot_func in zip(axs, plots):
            plot_func(ax)

        [a.legend(self.data.keys()) for a in axs]
        plt.show()

    def plot_differences(self, axs):
        print('plot differences')
        for key, val in self.data.items():
            val = np.array(val)
            diff = val[1:] - val[:-1]
            axs.plot(vfrom_ts(val[1:]), diff)
        axs.set_title('Time differences')
        axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))

    def plot_hists(self, axs):
        print('plot hist')
        min_lim = 1e10
        max_lim = -1e10
        for key, val in tqdm(self.data.items()):
            val = np.array(val)
            diff = val[1:] - val[:-1]
            # if 'chassis' in key:
            #     print(diff.min())
            #     print(diff.max())
            #     plt.plot(np.arange(len(diff)), diff)
            #     plt.show()
            axs.hist(diff, bins=100, alpha=0.5)
            # axs.hist(diff, bins='fd', alpha=0.5)
            Q1, Q3 = np.percentile(diff, [25, 75])
            IQR = Q3 - Q1
            low_thresh = Q1 - IQR * 3
            high_thresh = Q3 + IQR * 3
            min_lim = low_thresh if min_lim > low_thresh else min_lim
            max_lim = high_thresh if max_lim < high_thresh else max_lim
        axs.set_xlim([min_lim, max_lim])
        axs.set_title('Histogramms of differences')

    def plot_avg_msecs(self, axs):
        print('plot avg msec timesamp')

        for key, val in tqdm(self.data.items()):
            val = np.array(val) % 0.2
            axs.hist(val, bins=100, alpha=0.5)
        axs.set_title('average msec timstamps')


def main(src_folder, channels):

    print(f'Checking folder: {src_folder}')
    checker = Checker(channels)

    start_time = None

    for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):
        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        time.sleep(.1)

        for channelname, msg, datatype, timestamp in (freader.read_messages()):
            checker.update(timestamp, channelname)

    checker.plot()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/29.07_ruby/')
    parser.add_argument('-c', '--channels', nargs='+',
                        help='channels to plot statistics. for example, /apollo/sensor/lidar/ - will look for all channels with this perfix', 
                        default=['/apollo/sensor/lidar'])

    args = parser.parse_args()
    print(args)

    try:
        # main('data/bag/2022-07-29-11-21-07/')
        # main('data/bag/ilya_first/')
        # main('data/bag/ilya_second/')
        # main('data/bag/2022-07-29-14-53-53')
        main(args.path, args.channels)
    except KeyboardInterrupt:
        exit()
