#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from datetime import datetime
import sys
from typing import Iterable
import time
import os

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
from tqdm import tqdm
import yaml

from cyber.python.cyber_py3 import record
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2

from checking.plotter import vfrom_ts

matplotlib.use("tkagg")
plt.rcParams['axes.grid'] = True
plt.rcParams['axes.xmargin'] = 0.


def get_fields(obj, fields: Iterable):
    out_list = []
    for f in fields:
        cur_obj = obj
        for i in f.split('.'):
            # print(i)
            cur_obj = getattr(cur_obj, i)
        out_list.append(cur_obj)
    return np.array(out_list).reshape(1, len(out_list))


def print_data(data_ar: np.array, required_fields: Iterable, groups=[]):

    if len(data_ar) == 0:
        print('No data to plot')
        return
    ts, data = data_ar[:, 0], data_ar[:, 1:]
    labels = required_fields[1:]
    labels = [i.split('.')[-1] for i in labels]

    ts = vfrom_ts(ts)

    print('Time:')
    print(f'From: {ts[0]}')
    print(f'To  : {ts[-1]}')
    print(f'Dur : {ts[-1] - ts[0]}')

    n_plots = len(groups)
    if len(groups) == 0:
        [groups.append([i]) for i in range(len(data_ar))]

    cols = 2

    rows = n_plots - n_plots // 2
    fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(
        19.2, 12.8), constrained_layout=True)

    for idx, group in enumerate(groups):

        if isinstance(group, list):
            label = [labels[i] for i in group]
            data_row = data[:, group]
        else:
            label = [labels[group]]
            data_row = data[:, group][:, np.newaxis]
        row = idx // axs.shape[1]
        col = idx % axs.shape[1]

        if 'x' in [i.split('.')[-1] for i in label] and ('y' in [i.split('.')[-1] for i in label]):

            keep_inds = [a[0] > 0 and a[1] > 0 for a in data_row]
            axs[row, col].plot(data_row[keep_inds, 0], data_row[keep_inds, 1])
            axs[row, col].set_aspect('equal')
            axs[row, col].set_adjustable('box')

        else:
            axs[row, col].plot(ts, data_row)
            axs[row, col].legend(label, loc='upper center')
            axs[row, col].xaxis.set_major_formatter(
                mdates.DateFormatter('%H:%M:%S'))

        # draw maximum or minimum value
            if abs(np.max(data_row)) > abs(np.min(data_row)):
                max_val = np.max(data_row, axis=0)

                axs[row, col].annotate(f'Max: {np.max(data_row):.3f}',
                                       xy=(1, 0.98),
                                       xycoords='axes fraction',
                                       horizontalalignment='right',
                                       verticalalignment='top'
                                       )
                axs[row, col].scatter(
                    ts[np.argmax(data_row, axis=0)], max_val, c=((1, 0, 0), ))
            else:
                min_val = np.min(data_row, axis=0)

                axs[row, col].annotate(f'Min: {np.min(data_row):.3f}',
                                       xy=(1, 0.98),
                                       xycoords='axes fraction',
                                       horizontalalignment='right',
                                       verticalalignment='top'
                                       )
                axs[row, col].scatter(
                    ts[np.argmin(data_row, axis=0)], min_val, c=((1, 0, 0), ))

        # axs[row, col].set_title(label.split('.')[-1])

    plt.show()


def get_groups_ids(required_fields):
    counter = 0
    groups = []
    for el in required_fields:
        if isinstance(el, list):
            groups.append(list(range(counter, counter + len(el))))
            counter += len(el)
        else:
            groups.append(counter)
            counter += 1
    return groups


def flatten_list(l):
    rt = []
    for el in l:
        if not isinstance(el, list):
            rt.append(el)
        else:
            for a in el:
                rt.append(a)
    return rt


def main(src_folder, required_fields):

    start = time.time()
    is_COMPLETE_AUTO_DRIVE = False

    groups = get_groups_ids(required_fields[1:])

    required_fields = flatten_list(required_fields)

    for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):

        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        data_ar = np.empty((0, len(required_fields)), dtype=np.float32)

        for channelname, msg, datatype, timestamp in freader.read_messages():
            if channelname == '/apollo/canbus/chassis':
                msg_struct = chassis_pb2.Chassis()
                msg_struct.ParseFromString(msg)
                if chassis_pb2._CHASSIS_DRIVINGMODE.values_by_number[msg_struct.driving_mode].name != 'COMPLETE_AUTO_DRIVE':

                    if is_COMPLETE_AUTO_DRIVE:
                        print_data(data_ar, required_fields, groups=groups)
                        # return
                    is_COMPLETE_AUTO_DRIVE = False
                    continue

                else:
                    is_COMPLETE_AUTO_DRIVE = True

            if channelname == '/apollo/control':
                if not is_COMPLETE_AUTO_DRIVE:
                    continue
                msg_struct = control_cmd_pb2.ControlCommand()
                msg_struct.ParseFromString(msg)
                cur_data = get_fields(msg_struct, required_fields)
                data_ar = np.concatenate((data_ar, cur_data))

    print_data(data_ar, required_fields, groups=groups)


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2022-08-11-13-16-59/')
    parser.add_argument('-c', '--conf', type=str, help='a path to config .yaml file',
                        default=f'{sys.path[0]}vis_fields_control.yaml')

    args = parser.parse_args()
    print(args)

    with open(args.conf, 'r') as f:
        params = yaml.safe_load(f)

    try:
        main(args.path, params['required_fields'])
    except KeyboardInterrupt:
        exit()
