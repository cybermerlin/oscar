###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np
from numpy.linalg import inv

import open3d as o3d
from utils import draw_points, timeit

np.set_printoptions(precision=3, suppress=True)


def dist(p1, p2):
    return np.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)


class PointCloudProjector:

    def __init__(self,
                 camera_matrix=np.eye(3),
                 lidar2novatel_tr=np.eye(4),
                 novatel2camera_tr=np.eye(4),
                 distortion_coeffs=None,
                 point_size=1
                 ) -> None:

        self.point_size = point_size
        self.lidar2novatel_tr = lidar2novatel_tr
        self.novatel2camera_tr = novatel2camera_tr.view()

        self.camera_matrix = camera_matrix
        self.distortion_coeffs = distortion_coeffs

    def draw(self, image, points_3d):
        
        points_3d, intensities = points_3d[:, :-1], points_3d[:, -1]
        pc = o3d.geometry.PointCloud()
        pc.points = o3d.utility.Vector3dVector(points_3d)
        pc = pc.voxel_down_sample(0.3)
        points_3d = np.asarray(pc.points)


        draw_points(image, points_3d, inv(self.lidar2novatel_tr @ self.novatel2camera_tr),
                    self.camera_matrix, self.distortion_coeffs, radius=self.point_size, colors=intensities)
