###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np

from utils import draw_line, load_if_needed, load_transformation, load_camera_matrix
from numpy.linalg import inv


class CarDimensionsProjector:
    def __init__(self,
                 interval=[0, 100],
                 width=2.5,
                 height=-2.5,
                 localization2novatel_tr=np.eye(4),
                 novatel2camera_tr=np.eye(4),
                 camera_matrix=np.eye(3),
                 distortion_coeffs=np.zeros(5),
                 color=(255, 0, 0),
                 line_width=3
                 ) -> None:

        self.localization2novatel_tr = load_if_needed(
            localization2novatel_tr, load_transformation)
        self.novatel2camera_tr = load_if_needed(
            novatel2camera_tr, load_transformation)
        self.init_novatel2camera_tr = self.novatel2camera_tr
        self.line_width = line_width
        self.color = color

        self.cam_matrix = load_if_needed(camera_matrix, load_camera_matrix)
        self.distortion_coeffs = distortion_coeffs

        self.l_line = np.array([
            [-width / 2, interval[0], height],
            [-width / 2, interval[1], height],

        ])
        self.c_line = np.array([
            [0, interval[0], height],
            [0, interval[1], height],

        ])
        self.r_line = np.array([
            [width / 2, interval[0], height],
            [width / 2, interval[1], height],
        ])

    def update_camera_transform(self, new_camera_matrix):
        self.novatel2camera_tr = new_camera_matrix

    def draw_car_dimensions(self, image):

        for line in [self.l_line, self.c_line, self.r_line]:
            draw_line(image,
                      line,
                      self.color,
                      inv(self.localization2novatel_tr @ self.novatel2camera_tr),
                      self.cam_matrix,
                      self.distortion_coeffs, line_width=self.line_width)
