###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys
from collections import deque
from copy import deepcopy as dc
import time
from threading import Lock

import yaml
import cv2 as cv
from scipy.spatial.transform import Rotation
import numpy as np

from pointcloud_projector import PointCloudProjector

sys.path.append('/apollo')  # nopep8
sys.path.append("/apollo/bazel-bin")  # nopep8

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2, pointcloud_pb2

from car_dimensions_projector import CarDimensionsProjector
from map_projector import MapProjector
from horizon_projector import HorizonProjector
from optical_flow import OpticalFlowDetector

from utils import load_camera_matrix, load_dimensions, load_distortion, load_transformation, draw_line, load_transformations, timeit

from extraction.utils import TFBuffer

np.set_printoptions(precision=2, suppress=True)


class ImagePainter:
    def __init__(self,
                 channel="",
                 loc2novatel_tr_file="",
                 novatel2cam_tr_file="",
                 camera_intrinsics_file="",
                 cam_rotation_euler_overwrite=None,
                 **kwargs
                 ) -> None:

        cyber.init()
        self.node = cyber.Node("ImagePainter")
        self.channel = channel

        self.alive = True

        self.loc_frame_height = 1
        self.width, self.height = load_dimensions(camera_intrinsics_file)

        self.image = np.zeros((self.height, self.width, 3))
        self.drawing_image = np.zeros((self.height, self.width, 3))

        self.world2loc = np.eye(4)
        self.last_transform = np.eye(4)
        self.tansforms_buffer = TFBuffer()
        self.loc2novatel_tr = load_transformation(
            loc2novatel_tr_file)
        self.novatel2camera_tr = load_transformation(
            novatel2cam_tr_file)

        self.lock = Lock()

        self.current_image_msg = None
        self.cyber_msg = None
        self.last_pc = None

        self.callbacks_active = True

        if cam_rotation_euler_overwrite is not None:
            self.novatel2camera_tr[:-1, :-1] = Rotation.from_euler(
                "xyz", cam_rotation_euler_overwrite, degrees=True).as_matrix()

        self.M = load_camera_matrix(
            camera_intrinsics_file)

        self.D = load_distortion(
            camera_intrinsics_file)

        self.distortion_maps = cv.initUndistortRectifyMap(
            self.M, self.D, np.eye(3), self.M, (self.width, self.height), cv.CV_32FC1)

        self.pipeline = [self.undistort]

        print("Using map projector")
        self.map_projector = MapProjector(
            camera_matrix=self.M,
            localization2novatel_tr=self.loc2novatel_tr,
            novatel2camera_tr=self.novatel2camera_tr,
            **kwargs["map_annotation_params"]
        )
        if kwargs['draw_map_data_on_output_image']:
            self.pipeline.append(self.draw_map)

        if kwargs["draw_car_dimensions"]:
            print("Using car dimensions projector")
            self.car_dimensions_projector = CarDimensionsProjector(
                localization2novatel_tr=self.loc2novatel_tr,
                novatel2camera_tr=self.novatel2camera_tr,
                camera_matrix=self.M,
                **kwargs["car_dimensions_drawing_params"]
            )
            self.pipeline.append(self.draw_car_dimensions)

        if kwargs["send_map_annotations_to_foxglove"]:
            from foxglove_annotations_writer import FoxGloveAnnotationsWriter

            self.fg_annotation_writer = FoxGloveAnnotationsWriter(
                self.node,
                left_line_color=self.map_projector.left_line_color,
                right_line_color=self.map_projector.right_line_color,
                central_line_color=self.map_projector.central_line_color,
                stop_line_color=self.map_projector.stop_line_color,
                traffic_light_color=self.map_projector.traffic_light_color,
                **kwargs["foxglove_sending_params"],
            )
            self.pipeline.append(self.send_fg_annotations)

        if kwargs["draw_horizon_line"]:
            print("Using horizon line projector")
            self.horizon_line_projector = HorizonProjector(
                localization2novatel_tr=self.loc2novatel_tr,
                novatel2cam_tr=self.novatel2camera_tr,
                camera_matrix=self.M,
                **kwargs["horizon_line_draw_params"]
            )
            self.pipeline.append(self.draw_horizon_line)

        # self.of_detector = OpticalFlowDetector(self.M, self.loc2novatel_tr, self.novatel2camera_tr)
        # self.pipeline.append(self.optical_flow_update)

        self.image_is_compressed = "compressed" in channel
        if self.image_is_compressed:
            image_type = sensor_image_pb2.CompressedImage
        else:
            image_type = sensor_image_pb2.Image

        self.image_reader = self.node.create_reader(
            channel, image_type, self.image_cb)
        self.writer = self.node.create_writer(
            "/camera_drawing", image_type)

        self.optical_flow_tf_writer = self.node.create_writer(
            "/camera_left_optical_flow", transform_pb2.TransformStampeds)

        self.transform_reader = self.node.create_reader(
            "/tf", transform_pb2.TransformStampeds, self.transform_cb)

        if kwargs["draw_pc"]:
            novatel2lidar_tr = load_transformations(
                kwargs["pc_projection_params"]["base_frame"], kwargs["pc_projection_params"]["extrinsic_files"])

            self.pc_projector = PointCloudProjector(camera_matrix=self.M,
                                                    lidar2novatel_tr=np.linalg.inv(
                                                        novatel2lidar_tr),
                                                    novatel2camera_tr=self.novatel2camera_tr,
                                                    )

            self.pc_reader = self.node.create_reader(
                kwargs["pc_projection_params"]["pc_channel"],
                pointcloud_pb2.PointCloud, self.pc_cb)

            self.pipeline.append(self.draw_pc)

        self.pipeline.append(self.send_out_msg)

        print("Init complete")

    def optical_flow_update(self):
        points, tf = self.of_detector.update(self.image, self.world2loc)

        for p in points:
            cv.circle(self.drawing_image,
                      (int(p[0, 0]), int(p[0, 1])), 10, (255, 0, 0), 5)

        out_transform = transform_pb2.TransformStampeds()
        tr = out_transform.transforms.add()

        tr.header.frame_id = "camera_left"
        tr.child_frame_id = "camera_left_optical_flow"

        tr.transform.translation.x = tf[0, -1]
        tr.transform.translation.y = tf[1, -1]
        tr.transform.translation.z = tf[2, -1]

        quat = Rotation.from_matrix(
            tf[:-1, :-1]).as_quat()
        tr.transform.rotation.qx, tr.transform.rotation.qy, tr.transform.rotation.qz, tr.transform.rotation.qw = quat

        self.optical_flow_tf_writer.write(out_transform)

    def pc_cb(self, pc):

        if not self.callbacks_active:
            return
        MAX_POINTS = 5e5

        step = int(len(pc.point) // MAX_POINTS)
        step = max(1, step)
        # self.last_pc = np.array([[p.x, p.y, p.z] for p in pc.point[::step]])
        self.last_pc = np.array([[p.x, p.y, p.z, p.intensity]
                                for p in pc.point[::step]])

    def transform_cb(self, msg):
        if not self.callbacks_active:
            return
        msg_tr = msg.transforms[0].transform.translation
        msg_quat = msg.transforms[0].transform.rotation

        self.last_transform[:-1, :-1] = Rotation.from_quat(
            [msg_quat.qx, msg_quat.qy, msg_quat.qz, msg_quat.qw]).as_matrix()
        self.last_transform[:-1, -1] = np.array([msg_tr.x, msg_tr.y, msg_tr.z])

        self.tansforms_buffer.add(
            self.last_transform, msg.transforms[0].header.timestamp_sec)

    def image_cb(self, msg: sensor_image_pb2.Image):
        if not self.callbacks_active:
            return

        with self.lock:
            self.current_image_msg = dc(msg)

    def draw_pc(self):
        if self.last_pc is None:
            return

        self.pc_projector.draw(self.drawing_image, self.last_pc)

    def run_pipeline(self):

        self.drawing_image = self.image.copy()

        for task in self.pipeline:
            task()

        return self.drawing_image

    def send_out_msg(self):

        if self.image_is_compressed:
            self.cyber_msg.data = cv.imencode(
                ".jpg", self.drawing_image)[1].tobytes()
        else:
            self.cyber_msg.data = self.drawing_image.tobytes()

        self.cyber_msg.header.timestamp_sec = cyber_time.Time.now().to_sec()

        self.writer.write(self.cyber_msg)

    def send_fg_annotations(self):
        map_data = self.map_projector.get_map_data(self.world2loc)

        self.fg_annotation_writer.send_foxglove_annotations(map_data)

    def undistort(self):
        self.drawing_image = cv.remap(
            self.drawing_image, *self.distortion_maps, cv.INTER_LINEAR)

    def draw_map(self):
        self.map_projector.draw(
            self.drawing_image, self.world2loc)

    def draw_car_dimensions(self):
        self.car_dimensions_projector.draw_car_dimensions(self.drawing_image)

    def draw_horizon_line(self):
        self.horizon_line_projector.draw_horizon(
            self.drawing_image, self.world2loc)

    def toggle_callbacks(self):
        self.callbacks_active = not self.callbacks_active
        print(f'callbacks are active: {self.callbacks_active}')

    def process_image(self):

        if self.current_image_msg is None:
            time.sleep(0.1)
            return None

        if len(self.tansforms_buffer.ts_buffer) == 0:
            return None

        with self.lock:
            msg = dc(self.current_image_msg)
            self.current_image_msg = None

        st = time.time()

        self.world2loc = self.tansforms_buffer.get_nearest(
            msg.header.timestamp_sec)

        self.cyber_msg = dc(msg)
        self.image = np.frombuffer(msg.data, np.uint8)

        if not self.image_is_compressed:
            self.image = self.image.reshape(
                (msg.height, msg.width, 3))
        else:
            self.image = cv.imdecode(self.image, cv.IMREAD_COLOR)

        self.run_pipeline()

        dur = time.time() - st
        print(dur)

    def spin(self):

        while cyber.ok():
            # st = time.time()
            self.process_image()
            # dur = time.time() - st
            # print(dur)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--cfg-file', type=str, default=f'{sys.path[0]}/cfg_annotation_lucid.yaml',
                        help='path config yaml file')

    parser.add_argument('-m', '--map', type=str,
                        default=None, help='Overwrite map file')
    parser.add_argument("--loc2novatel", type=str, default=None,
                        help="Overwrite transformation file between 'localization' frame and 'novatel' frame")
    parser.add_argument("--novatel2cam", type=str, default=None,
                        help="Overwrite transformation file between 'novatel' frame and 'camera' frame")
    parser.add_argument("--intrinsics", type=str, default=None,
                        help="Overwrite camera intrinsics file")
    parser.add_argument("--channel", type=str, default=None,
                        help="Overwrite Channel name for input image")

    args = parser.parse_args()

    print(args)

    assert os.path.exists(
        args.cfg_file), f"Config file does not exist: {args.cfg_file}"
    with open(args.cfg_file, 'r', encoding='utf-8') as f:
        cfg = yaml.safe_load(f)

    if args.loc2novatel is not None:
        cfg["loc2novatel_tr_file"] = args.loc2novatel

    if args.novatel2cam is not None:
        cfg["novatel2cam_tr_file"] = args.novatel2cam

    if args.intrinsics is not None:
        cfg["camera_intrinsics_file"] = args.intrinsics

    if args.channel is not None:
        cfg["channel"] = args.channel

    if args.map is not None:
        cfg["map_annotation_params"]["map_file"] = args.map

    tr = ImagePainter(**cfg)

    tr.spin()
