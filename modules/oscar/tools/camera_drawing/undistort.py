import sys
import time
import yaml

import cv2 as cv

import numpy as np
from tqdm import tqdm
from scipy.spatial.transform import Rotation

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from modules.common_msgs.sensor_msgs import sensor_image_pb2

np.set_printoptions(precision=2, suppress=True)

# window = cv.namedWindow("image", cv.WINDOW_GUI_NORMAL)
# cv.resizeWindow("image", 1280, 720)


def load_transformation(filename):
    T = np.zeros((4, 4))
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        quat = f_yaml['transform']['rotation']
        shift = f_yaml['transform']['translation']
        T[:-1, :-1] = Rotation.from_quat([quat['x'],
                                          quat['y'], quat['z'], quat['w']]).as_matrix()
        T[:-1, -1] = np.array((shift['x'], shift['y'], shift['z']))
    return T


def load_camera_matrix(filename):
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        # M = np.array(f_yaml['K']).reshape((3, 3))
        M = np.array(f_yaml['P']).reshape((3, 4))[:, :-1]
    return M


def load_distortion(filename):
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        return np.array(f_yaml['D'])


class ImagePainter:
    def __init__(self, update_ts=False) -> None:

        channel = "/apollo/sensor/camera/front_6mm/image"

        cyber.init()
        self.node = cyber.Node("ImagePainter")
        self.update_ts = update_ts
        self.channel = channel

        self.reader = self.node.create_reader(
            channel, sensor_image_pb2.Image, self.cb)

        self.alive = True

        self.width = 1920
        self.height = 1080

        self.image = np.zeros((self.height, self.width, 3))
        self.drawing_image = np.zeros((self.height, self.width, 3))
        self.writer = self.node.create_writer(
            "/camera_drawing", sensor_image_pb2.Image)

        self.T = load_transformation(
            "/apollo/modules/calibration/data/mb_actros/camera_conf/camera_front_usb_extrinsics.yaml")
        self.M = load_camera_matrix(
            "/apollo/modules/calibration/data/mb_actros/camera_conf/camera_front_usb_intrinsics.yaml")

        self.D = load_distortion(
            "/apollo/modules/calibration/data/mb_actros/camera_conf/camera_front_usb_intrinsics.yaml")

        print(self.M)
        print()
        print(self.T)
        print()

        self.distortion_maps = cv.initUndistortRectifyMap(
            self.M, self.D, np.eye(3), self.M, (1920, 1080), cv.CV_32FC1)

    def cb(self, msg: sensor_image_pb2.Image):

        self.image = np.fromstring(msg.data, np.uint8).reshape((1080, 1920, 3))
        # self.image = cv.cvtColor(self.image, cv.COLOR_BGR2RGB)

        out_image = self.draw_lines()
        msg.data = out_image.tobytes()

        self.writer.write(msg)

    def draw_lines(self):

        length = 30
        height = -2.05

        l_line = np.array([
            [-1.25, 0, height],
            [-1.25, length, height],

        ])
        r_line = np.array([
            [1.25, 0, height],
            [1.25, length, height],
        ])

        self.drawing_image = cv.remap(
            self.image, *self.distortion_maps, cv.INTER_LINEAR)

        def project_points(line):
            points_2d, _ = cv.projectPoints(line,
                                            Rotation.from_matrix(
                                                self.T[:-1, :-1]).as_rotvec(),
                                            self.T[:-1, -1],
                                            self.M,
                                            self.D)
            points_2d = points_2d.astype(np.int32)
            return points_2d

        l_line_camera_points = project_points(l_line)
        r_line_camera_points = project_points(r_line)

        # def is_oob(
        #     x, y): return x < 0 or y < 0 or x > self.width or y > self.height

        def draw_lines(image, lines, color):
            for (x1, y1), (x2, y2) in zip(lines[:-1, 0, :], lines[1:, 0, :]):
                # if is_oob(x1, y1) or is_oob(x2, y2):
                # continue
                cv.line(image, (x1, y1), (x2, y2), color, 2)

        red = (255, 0, 0)

        draw_lines(self.drawing_image, l_line_camera_points, red)
        draw_lines(self.drawing_image, r_line_camera_points, red)

        return self.drawing_image


if __name__ == "__main__":
    tr = ImagePainter()

    # print(tr.M)

    # tr.draw_lines()

    tr.node.spin()

    # while cyber.ok() and tr.alive:
    #     cv.imshow("image", tr.drawing_image)
    #     key = cv.waitKey(10)
    #     if key == ord('q'):
    #         cv.destroyAllWindows()
    #         break

    # cyber.shutdown()
