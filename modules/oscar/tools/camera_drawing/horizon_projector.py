###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np
from scipy.spatial.transform import Rotation as R

from utils import draw_line, timeit, removeXYRotation
from numpy.linalg import inv


class HorizonProjector:
    def __init__(self,
                 height=30,
                 localization2novatel_tr=np.eye(4),
                 novatel2cam_tr=np.eye(4),
                 camera_matrix=np.eye(3),
                 distortion_coeffs=np.zeros(5),
                 color=(255, 0, 0),
                 line_width=3,
                 num_vertices=8,
                 radius=1e5,
                 ) -> None:

        self.localization2novatel_tr = localization2novatel_tr
        self.novatel2cam_tr = novatel2cam_tr.view()
        self.line_width = line_width
        self.color = color

        self.cam_matrix = camera_matrix
        self.distortion_coeffs = distortion_coeffs

        self.num_vertices = num_vertices
        self.radius = radius

    def draw_horizon(self, image, world2localization_tr):

        points = []

        world2localization_tr = removeXYRotation(world2localization_tr)

        # We create equilateral regular polygon (approximately circle) in a center of car position
        # with large radius.
        for i in range(self.num_vertices):
            p = world2localization_tr[:-1, -1].copy()
            p[0] += self.radius * np.cos(2 * np.pi * i / self.num_vertices)
            p[1] += self.radius * np.sin(2 * np.pi * i / self.num_vertices)
            points.append(p)

        points.append(points[0])

        current_transform = inv(
            world2localization_tr @ self.localization2novatel_tr @ self.novatel2cam_tr)

        zip_points = np.stack([points[:-1], points[1:]])
        for pts in zip_points:
            draw_line(image,
                      pts,
                      self.color,
                      current_transform,
                      self.cam_matrix,
                      self.distortion_coeffs, line_width=self.line_width)
