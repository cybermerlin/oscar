###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
from copy import deepcopy as dc

import numpy as np
from google.protobuf.timestamp_pb2 import Timestamp

# sys.path.append("/apollo/bazel-bin/modules/oscar/tools/camera_drawing/foxglove_schemas") # nopep8

# from Color_pb2 import Color
# from Point2_pb2 import Point2
# from PointsAnnotation_pb2 import PointsAnnotation
# from ImageAnnotations_pb2 import ImageAnnotations

sys.path.append("/apollo/bazel-bin")  # nopep8

from modules.oscar.foxglove.schemas.Color_pb2 import Color
from modules.oscar.foxglove.schemas.Point2_pb2 import Point2
from modules.oscar.foxglove.schemas.PointsAnnotation_pb2 import PointsAnnotation
from modules.oscar.foxglove.schemas.ImageAnnotations_pb2 import ImageAnnotations


class FoxGloveAnnotationsWriter:

    def __init__(self,
                 node,
                 channel_name='/fg_image_annotations',
                 left_line_color=(255, 0, 0),
                 right_line_color=(0, 0, 255),
                 central_line_color=(0, 255, 0),
                 stop_line_color=(255, 128, 0),
                 traffic_light_color=(0, 255, 0),
                 ) -> None:
        self.writer = node.create_writer(channel_name, ImageAnnotations)

        self.writer.write(ImageAnnotations())

        self.left_line_color = left_line_color
        self.right_line_color = right_line_color
        self.central_line_color = central_line_color
        self.stop_line_color = stop_line_color
        self.traffic_light_color = traffic_light_color
        print("Using FoxGLove Annotations Writer")

        print("WARNING: running this foxglove publishing after any other module that pubslihes foxglove messages causes segfault.")
        print("To avoid it first run this script, wait for initialization and after this run another module that publishes FG messages")

    def send_foxglove_annotations(self, map_data):

        out_msg = ImageAnnotations()

        ts = Timestamp(seconds=int(time.time()),
                       nanos=int(time.time() % 1e9))

        for lane in map_data["lanes"]:

            # add polygon
            pointset = PointsAnnotation()
            pointset.timestamp.CopyFrom(ts)
            pointset.thickness = 3
            pointset.type = 2

            for (x, y) in np.concatenate((lane['left_borders'], np.flip(lane['right_borders'], axis=0))):
                pointset.points.append(Point2(x=x, y=y))

            pointset.outline_color.CopyFrom(Color(r=self.central_line_color[0],
                                                  g=self.central_line_color[1],
                                                  b=self.central_line_color[2],
                                                  a=0.))
            pointset.fill_color.CopyFrom(Color(r=self.central_line_color[0],
                                               g=self.central_line_color[1],
                                               b=self.central_line_color[2],
                                               a=0.1))

            out_msg.points.append(pointset)

            # add left border
            pointset = PointsAnnotation()
            pointset.timestamp.CopyFrom(ts)
            pointset.thickness = 3
            pointset.type = 3

            for (x, y) in lane['left_borders']:
                pointset.points.append(Point2(x=x, y=y))

            pointset.outline_color.CopyFrom(Color(r=self.left_line_color[0],
                                                  g=self.left_line_color[1],
                                                  b=self.left_line_color[2],
                                                  a=0.5))
            pointset.fill_color.CopyFrom(Color(r=self.left_line_color[0],
                                               g=self.left_line_color[1],
                                               b=self.left_line_color[2],
                                               a=0.1))

            out_msg.points.append(pointset)

            # add right border
            pointset = PointsAnnotation()
            pointset.timestamp.CopyFrom(ts)
            pointset.thickness = 3
            pointset.type = 3

            for (x, y) in lane['right_borders']:
                pointset.points.append(Point2(x=x, y=y))

            pointset.outline_color.CopyFrom(Color(r=self.right_line_color[0],
                                                  g=self.right_line_color[1],
                                                  b=self.right_line_color[2],
                                                  a=0.5))
            pointset.fill_color.CopyFrom(Color(r=self.right_line_color[0],
                                               g=self.right_line_color[1],
                                               b=self.right_line_color[2],
                                               a=0.1))

            out_msg.points.append(pointset)

            pointset = PointsAnnotation()
            pointset.timestamp.CopyFrom(ts)
            pointset.thickness = 3
            pointset.type = 3

            for (x, y) in lane["central_lines"]:
                pointset.points.append(Point2(x=x, y=y))

            pointset.outline_color.CopyFrom(Color(r=self.central_line_color[0],
                                                  g=self.central_line_color[1],
                                                  b=self.central_line_color[2],
                                                  a=0.5))
            pointset.fill_color.CopyFrom(Color(r=self.central_line_color[0],
                                               g=self.central_line_color[1],
                                               b=self.central_line_color[2],
                                               a=0.1))

            out_msg.points.append(pointset)

        for stop_line in map_data["stop_lines"]:

            pointset = PointsAnnotation()
            pointset.timestamp.CopyFrom(ts)
            pointset.thickness = 3
            pointset.type = 3

            for (x, y) in stop_line:
                pointset.points.append(Point2(x=x, y=y))

            pointset.outline_color.CopyFrom(Color(r=self.stop_line_color[0],
                                                  g=self.stop_line_color[1],
                                                  b=self.stop_line_color[2],
                                                  a=0.5))
            pointset.fill_color.CopyFrom(Color(r=self.stop_line_color[0],
                                               g=self.stop_line_color[1],
                                               b=self.stop_line_color[2],
                                               a=0.1))

            out_msg.points.append(dc(pointset))

        for line in map_data["traffic_lights"]:
            line_pairs = line.reshape((-1, 2, 2))
            for line1, line2 in line_pairs:

                pointset = PointsAnnotation()
                pointset.timestamp.CopyFrom(ts)
                pointset.thickness = 3
                pointset.type = 3

                pointset.points.append(Point2(x=line1[0], y=line1[1]))
                pointset.points.append(Point2(x=line2[0], y=line2[1]))

                pointset.outline_color.CopyFrom(Color(r=self.traffic_light_color[0],
                                                      g=self.traffic_light_color[1],
                                                      b=self.traffic_light_color[2],
                                                      a=0.5))
                pointset.fill_color.CopyFrom(Color(r=self.traffic_light_color[0],
                                                   g=self.traffic_light_color[1],
                                                   b=self.traffic_light_color[2],
                                                   a=0.1))

                out_msg.points.append(dc(pointset))
        self.writer.write(out_msg)
