#!/usr/bin/env python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import argparse
import google.protobuf.text_format as text_format

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.transform_msgs import transform_pb2


class FakeLocalization:

    def __init__(self, period, static_loc_file):

        print(f"Reading from {static_loc_file}")

        cyber.init()

        self.period = period
        self.node = cyber.Node("FakeObstacleDetection")

        self.loc_writer = self.node.create_writer(
            "/apollo/localization/pose", localization_pb2.LocalizationEstimate)

        self.tf_writer = self.node.create_writer(
            "/tf", transform_pb2.TransformStampeds)
        self.loc_msg = localization_pb2.LocalizationEstimate()

        try:
            with open(static_loc_file, 'r') as f:
                text_format.Merge(f.read(), self.loc_msg)

        except FileNotFoundError:
            print(
                f"Static perception file does not exist: {static_loc_file}")
        print(self.loc_msg)

        self.tf_msg = transform_pb2.TransformStampeds()
        self.tf_msg.header.timestamp_sec = cyber_time.Time.now().to_sec()

        transform = self.tf_msg.transforms.add()

        transform.header.frame_id = "world"
        transform.header.timestamp_sec = cyber_time.Time.now().to_sec()
        transform.child_frame_id = "localization"
        transform.transform.translation.x = self.loc_msg.pose.position.x
        transform.transform.translation.y = self.loc_msg.pose.position.y
        transform.transform.translation.z = self.loc_msg.pose.position.z
        transform.transform.rotation.qx = self.loc_msg.pose.orientation.qx
        transform.transform.rotation.qy = self.loc_msg.pose.orientation.qy
        transform.transform.rotation.qz = self.loc_msg.pose.orientation.qz
        transform.transform.rotation.qw = self.loc_msg.pose.orientation.qw

    def spin(self):
        while cyber.ok():

            current_time = time.time()

            self.loc_msg.header.timestamp_sec = current_time
            self.tf_msg.transforms[0].header.timestamp_sec = current_time

            self.loc_writer.write(self.loc_msg)
            self.tf_writer.write(self.tf_msg)
            time.sleep(1 / self.period)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--period', type=float, help='period between publishing tfs',
                        default=100)
    parser.add_argument('-f', '--localization_file', type=str, help='a path to static obstacles protobuf file',
                        default="/apollo/modules/oscar/tools/fake_localization/fake_localization.pb.txt")

    args = parser.parse_args()
    print(args)
    tr = FakeLocalization(args.period, args.localization_file)
    tr.spin()
