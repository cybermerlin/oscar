
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include "modules/common_msgs/localization_msgs/localization.pb.h"

#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"
#include "modules/common/adapters/adapter_gflags.h"
#include "modules/common/util/message_util.h"
#include "modules/transform/transform_broadcaster.h"

namespace apollo {
namespace localization {

/**
 * class FakeLocalizationComponent
 * This class generates fake localization messages. The localization message
 * only has valid headers.
 *
 * This tool is used to trigger modules that depends on localization message.
 */

class FakeLocalizationComponent : public apollo::cyber::TimerComponent {
 public:
  bool Init() override {
    ZoneScopedN("Init zone");
    tf2_broadcaster_.reset(new apollo::transform::TransformBroadcaster(node_));

    int fd = open(
        "/apollo/modules/oscar/tools/fake_localization/"
        "fake_localization.pb.txt",
        O_RDONLY);
    google::protobuf::io::FileInputStream fstream(fd);
    google::protobuf::TextFormat::Parse(&fstream, &localization);
    close(fd);

    localization_writer_ =
        node_->CreateWriter<LocalizationEstimate>(FLAGS_localization_topic);
    // return false;
    return true;
  }
  bool Proc() override {
    tracy::SetThreadName("Fake localization Proc thread");
    // ZoneScopedN("Fake localization zone");
    ZoneTransientN(Scope1, "Fake localization zone", true);
    {
      ZoneTransientN(Scope2, "Ptr", true);
      // ZoneScopedN("Ptr");
      auto ptr = std::make_shared<double>();
      usleep(200'000);
      ptr.get();
      // std::cout << *ptr << "\n";
    }
    // auto localization = std::make_shared<LocalizationEstimate>();

    common::util::FillHeader("fake_localization", &localization);
    localization.set_measurement_time(cyber::Time::Now().ToSecond());

    localization_writer_->Write(localization);
    PublishPoseBroadcastTF(localization);
    return true;
  }

  void PublishPoseBroadcastTF(const LocalizationEstimate& localization) {
    ZoneTransientN(Scope3, "TF Broadcasting", true);
    // ZoneScopedN("TF Broadcasting");
    // broadcast tf message
    apollo::transform::TransformStamped tf2_msg;

    auto mutable_head = tf2_msg.mutable_header();
    mutable_head->set_timestamp_sec(localization.measurement_time());
    mutable_head->set_frame_id(broadcast_tf_frame_id_);
    tf2_msg.set_child_frame_id(broadcast_tf_child_frame_id_);

    auto mutable_translation =
        tf2_msg.mutable_transform()->mutable_translation();
    mutable_translation->set_x(localization.pose().position().x());
    mutable_translation->set_y(localization.pose().position().y());
    mutable_translation->set_z(localization.pose().position().z());

    auto mutable_rotation = tf2_msg.mutable_transform()->mutable_rotation();
    mutable_rotation->set_qx(localization.pose().orientation().qx());
    mutable_rotation->set_qy(localization.pose().orientation().qy());
    mutable_rotation->set_qz(localization.pose().orientation().qz());
    mutable_rotation->set_qw(localization.pose().orientation().qw());

    tf2_broadcaster_->SendTransform(tf2_msg);
  }

 private:
  std::unique_ptr<apollo::transform::TransformBroadcaster> tf2_broadcaster_;
  std::string broadcast_tf_frame_id_ = "world";
  std::string broadcast_tf_child_frame_id_ = "localization";
  LocalizationEstimate localization;

  std::shared_ptr<apollo::cyber::Writer<LocalizationEstimate>>
      localization_writer_;
};
CYBER_REGISTER_COMPONENT(FakeLocalizationComponent);

}  // namespace localization
}  // namespace apollo
