#!/usr/bin/env python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

from glob import glob
import os
import sys
import time
import argparse
from tqdm import tqdm
import yaml

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber
from modules.oscar.foxglove.schemas.PosesInFrame_pb2 import PosesInFrame

from modules.common_msgs.transform_msgs.transform_pb2 import TransformStampeds


from extraction.utils import ReaderWrapper, MsgCache


def read_poses(folder):

    poses_msg = PosesInFrame()
    poses_msg.frame_id = "world"

    for channelname, msg, datatype, ts in tqdm(ReaderWrapper(folder)):

        if channelname != "/tf":
            continue

        tf = TransformStampeds.FromString(msg).transforms[0].transform

        pose = poses_msg.poses.add()

        pose.position.x = tf.translation.x
        pose.position.y = tf.translation.y
        pose.position.z = tf.translation.z

        pose.orientation.x = tf.rotation.qx
        pose.orientation.y = tf.rotation.qy
        pose.orientation.z = tf.rotation.qz
        pose.orientation.w = tf.rotation.qw

    return poses_msg


X_OFFSET = 3.5e5
Y_OFFSET = 6.6e6
Z_OFFSET = 0

WHEEL_RADIUS = 0.509


class CameraCalibrationPublisher:

    def __init__(self, period, path, step=100):

        self.period = period

        self.writers = []

        self.objects = []

        self.static_tf_msg = TransformStampeds()
        tf = self.static_tf_msg.transforms.add()

        tf.header.frame_id = "world"
        tf.child_frame_id = "path_local_tf"

        tf.transform.translation.x = X_OFFSET
        tf.transform.translation.y = Y_OFFSET
        tf.transform.translation.z = Z_OFFSET

        tf.transform.rotation.qx = 0
        tf.transform.rotation.qy = 0
        tf.transform.rotation.qz = 0
        tf.transform.rotation.qw = 0

        self.cache = MsgCache(
            "/apollo/modules/oscar/tools/data/path_publisher_cache")

        folder = path[0].split("/")[-2]
        self.poses = self.cache.load(folder)
        if self.poses is None:
            self.poses = read_poses(path)

            self.cache.save(self.poses, folder)
        else:
            self.poses = PosesInFrame.FromString(self.poses)

        filtered_poses = []
        for p in tqdm(self.poses.poses[::step]):
            filtered_poses.append(p)

        del self.poses.poses[:]

        self.poses.poses.extend(filtered_poses)

        for p in self.poses.poses:
            p.position.x -= X_OFFSET
            p.position.y -= Y_OFFSET
            # p.position.z -= Z_OFFSET
            p.position.z -= Z_OFFSET + WHEEL_RADIUS

        self.poses.frame_id = tf.child_frame_id

        write_channel = f"/record_poses"
        local_tf_channel = "/path_local_tf"

        cyber.init()
        self.node = cyber.Node("PosesPublisher")
        self.writer = self.node.create_writer(
            write_channel, PosesInFrame)

        self.local_tf_writer = self.node.create_writer(
            local_tf_channel, TransformStampeds)

    def __del__(self):
        cyber.shutdown()

    def spin(self):
        while cyber.ok():

            self.writer.write(self.poses)
            self.local_tf_writer.write(self.static_tf_msg)
            time.sleep(self.period)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--period', type=float, help='period between publishing obstacles',
                        default=5)
    parser.add_argument('-f', '--folder', type=str,
                        help='folder with record files', nargs="+")

    args = parser.parse_args()
    print(args)

    tr = CameraCalibrationPublisher(args.period, args.folder)
    tr.spin()
