///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "Eigen/Geometry"

#include "modules/common_msgs/map_msgs/map.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/oscar/foxglove/schemas/SceneUpdate.pb.h"
#include "modules/oscar/tools/map_publisher/proto/map_publisher_conf.pb.h"

using apollo::transform::Transform;

#define THICKNESS 2

namespace apollo {
namespace oscar {

namespace {
apollo::oscar::map::publisher::ColorOptions create_default_color_options() {
  map::publisher::ColorOptions options;
  auto central_color = options.mutable_central_line_color();
  central_color->set_r(1);
  central_color->set_g(0);
  central_color->set_b(0);
  central_color->set_a(1);

  auto left_color = options.mutable_left_line_color();
  left_color->set_r(0);
  left_color->set_g(1);
  left_color->set_b(0);
  left_color->set_a(1);

  auto right_color = options.mutable_right_line_color();
  right_color->set_r(0);
  right_color->set_g(1);
  right_color->set_b(0);
  right_color->set_a(1);

  auto traffic_light_color = options.mutable_traffic_light_color();
  traffic_light_color->set_r(1);
  traffic_light_color->set_g(1);
  traffic_light_color->set_b(1);
  traffic_light_color->set_a(0.5);

  auto stop_line_color = options.mutable_stop_line_color();
  stop_line_color->set_r(1);
  stop_line_color->set_g(0.5);
  stop_line_color->set_b(0.3);
  stop_line_color->set_a(1);

  return options;
}

}  // namespace

static const map::publisher::ColorOptions default_options =
    create_default_color_options();

void convert_map_object(
    foxglove::SceneEntity* entity, const apollo::hdmap::Lane& lane,
    const std::array<double, 2>& map_offset, double fixed_height = 0,
    const map::publisher::ColorOptions& options = default_options);

void convert_map_object(
    foxglove::SceneEntity* entity, const apollo::hdmap::Signal& signal,
    const std::array<double, 2>& map_offset, double fixed_height = 0,
    const map::publisher::ColorOptions& options = default_options);

double distance(const Transform& tf1, const Transform tf2);

bool is_near(const apollo::hdmap::Lane& object,
             const std::array<double, 3>& current_coords,
             double distance_threshold);

bool is_near(const apollo::hdmap::Signal& object,
             const std::array<double, 3>& current_coords,
             double distance_threshold);

void convert_timestamp(double src_ts, google::protobuf::Timestamp* out_ts);

}  // namespace oscar
}  // namespace apollo