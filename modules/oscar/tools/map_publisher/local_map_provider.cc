///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/tools/map_publisher/local_map_provider.h"

#include <cmath>
#include <string>

#include <google/protobuf/util/time_util.h>

#include "cyber/common/file.h"

namespace apollo {
namespace oscar {

LocalMapProvider::LocalMapProvider(const std::string& map_file,
                                   double distance_threshold,
                                   std::array<double, 2> local_map_offset,
                                   double loc_frame_height,
                                   const ColorOptions& color_options)
    : loc_frame_height_(loc_frame_height),
      dist_thresh_(distance_threshold),
      local_map_offset_(local_map_offset),
      color_options_(color_options) {
  if (!cyber::common::GetProtoFromFile(map_file, &map_data_)) {
    throw std::runtime_error("Failed to parse file: " + map_file);
  }
}

foxglove::SceneUpdate LocalMapProvider::provideLocalMap(
    const TransformStampeds& loc_tf_msg) {
  std::array<double, 3> current_coords{
      loc_tf_msg.transforms(0).transform().translation().x(),
      loc_tf_msg.transforms(0).transform().translation().y(),
      loc_tf_msg.transforms(0).transform().translation().z() -
          loc_frame_height_};

  foxglove::SceneUpdate out_update_msg;

  filter_objects(map_data_.lane(), &out_update_msg, current_coords,
                 dist_thresh_);
  filter_objects(map_data_.signal(), &out_update_msg, current_coords,
                 dist_thresh_);

  auto deletion = out_update_msg.mutable_deletions()->Add();
  deletion->set_id("");
  deletion->set_type(
      foxglove::SceneEntityDeletion_Type::SceneEntityDeletion_Type_ALL);

  convert_timestamp(loc_tf_msg.header().timestamp_sec(),
                    deletion->mutable_timestamp());

  return out_update_msg;
}

template <typename T, typename = std::enable_if_t<
                          std::is_same<T, apollo::hdmap::Lane>::value ||
                          std::is_same<T, apollo::hdmap::Signal>::value>>
void LocalMapProvider::filter_objects(
    const google::protobuf::RepeatedPtrField<T>& map_objects,
    foxglove::SceneUpdate* out_update_msg,
    const std::array<double, 3>& current_coords, double distance_threshold) {
  for (auto& object : map_objects) {
    auto object_id = object.id().id();

    bool object_is_near = is_near(object, current_coords, distance_threshold);

    if (object_is_near) {
      add_map_object(out_update_msg, object, current_coords[2], object_id);
    }
  }
}

template <typename T>
void LocalMapProvider::add_map_object(foxglove::SceneUpdate* out_update_msg,
                                      const T& object, double height,
                                      const std::string& object_id) {
  auto entity_to_add = out_update_msg->add_entities();
  convert_map_object(entity_to_add, object, local_map_offset_, height,
                     color_options_);
  entity_to_add->set_frame_id("local_map_frame");
  *entity_to_add->mutable_timestamp() =
      google::protobuf::util::TimeUtil::GetCurrentTime();
  entity_to_add->set_id(object_id);

  entity_to_add->set_frame_locked(true);
}

}  // namespace oscar
}  // namespace apollo
