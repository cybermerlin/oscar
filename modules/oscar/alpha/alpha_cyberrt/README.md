#### Alpha CyberRT

Модуль предназначен для взаимодействия с ПАК [Alpha](alpha.starline.ru) и включает:

* Драйвер для автомобиля с установленным на нем ПАК [Alpha](alpha.starline.ru).
Является оберткой над [alpha_py](https://gitlab.com/starline/alpha_py) для
[CyberRT](https://gitlab.com/starline/oscar/-/tree/master/cyber).

* Узел для управления транспортным средством с геймпада.



#### Установка

1. Для начала работы требуется предварительно склонировать [alpha_py](https://gitlab.com/starline/alpha_py):

        cd <path-to-apollo>/modules/drivers
        mkdir alpha
        cd alpha
        git clone https://gitlab.com/starline/alpha_py.git

    И установить зависимости:

        pip3 install -r alpha_py/requirements.txt

    Обеспечить видимость пакета alpha внутри docker-окружения Apollo можно посредством добавления соответствующего пути в переменную окружения PYTHONPATH:

        export PYTHONPATH=/apollo/modules/drivers/alpha/alpha_py:\${PYTHONPATH}


2. Далее требуется склонировать данный пакет и установить его зависимости:

        cd <path-to-apollo>/modules/drivers/alpha
        git clone https://gitlab.com/starline/alpha_cyberRT.git

        pip3 install -r alpha_cyberRT/requirements.txt

    После сборки apollo (6-ой версии и старше) узлы пакета будут доступны, соответственно, как:

        /apollo/bazel-bin/modules/drivers/alpha/driver
        /apollo/bazel-bin/modules/drivers/alpha/joy


3. В качестве примера рекомендуем ознакомиться с интеграцией alpha на примере проекта [OSCAR](https://gitlab.com/starline/oscar).



#### Драйвер автомобиля с Alpha

Для запуска драйвера запустите скрипт:

```
python3 vehicle_driver/driver.py
```

В случае использования oscar_tools можно воспользоваться следующими командами:

```
oscar alpha       - запуск узла Alpha
oscar alpha stop  - остановка узла Alpha
```

* **Конфигурация**

    Для корректной работы драйвера следует убедится в наличии конфигурационных
    файлов для автомобиля и для Alpha.

    После выбора в Dreamview используемого автомобиля конфигурационные файлы
    должны находиться по следующим путям:

    ```
    /apollo/modules/common/data/vehicle_param.pb.txt
    /apollo/modules/common/data/alpha_param.json
    ```


#### Геймпад

Для управления автомобилем с помощью геймпада Logitech F310 требуется
подключить его к ПК и запустить скрипт:

```
python3 teleop/joy.py
```

В случае использования oscar_tools можно воспользоваться следующими командами:

```
oscar joy         - запуск узла телеуправления
oscar joy no-ui   - запуск узла телеуправления без консольного интерфейса
oscar joy stop    - остановка узла телеуправления
```

* **Консольный интерфейс**

    ![](docs/pics/teleop.png)


* **Управление**

    ![](docs/pics/lf310.png)

    ```
    1,2 - Вкл/Выкл узла, для переключения требуется зажимать одновременно.
          TELEOP ON/OFF в консольном интерфейсе. При OFF не шлет данные
          в канал /control.

      3 - Ускорение в процентах. Throttle в консольном интерфейсе.

      4 - Торможение в процентах. Имеет приоритет над ускорением.

     11 - Вкл/Выкл перехвата управлением автомобиля.
          MODE AUTO/MANUAL в консольном интерфейсе.

     12 - Вкл/Выкл экстренного торможения. EBRAKE в консольном интерфейсе.

     14 - Виртуальный руль. SW_ANGLE в консольном интерфейсе. Для задания
          Целевого угла прижмите стик к любому краю и начните его вращение.
          Для фиксации целевого угла отведите стик от края или
          отпустите его.

     15 - Задний ход. DIRECTION REVERSE в консольном интерфейсе.

     16 - Передний ход. DIRECTION DRIVE в консольном интерфейсе.

     17 - Вкл/Выкл аппаратного индикатора Alpha. Обычно выполняется в виде
          зеленого светодиода и отображает состояние перехвата управления
          автомобилем. В данном случае может служить для проверки связи
          программного стека и аппаратной части alpha.
    ```


#### oscar_tools

В рамках проекта OSCAR работа с узлами данного пакета доступна посредством [oscar_tools](https://gitlab.com/starline/oscar/-/blob/master/docs/oscar/oscar_tools.md).
