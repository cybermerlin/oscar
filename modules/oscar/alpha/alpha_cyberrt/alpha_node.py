#!/usr/bin/env python

#############################################################################
# Copyright 2021 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import os
import sys
import time
import json

from argparse import ArgumentParser
from math     import isnan

sys.path.append('/apollo/cyber/python')

from cyber_py3 import cyber, cyber_time

sys.path.append('/apollo/bazel-bin')

from modules.common_msgs.chassis_msgs import chassis_pb2, chassis_detail_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2

from alpha import ( Vehicle,
                    VehicleParams,
                    Spinner,
                    WSCfg,
                    DEFAULT_TAKEOVER_ID )

from alpha.protocol import VEHICLE_MODE


ALPHA_PARAM_PATH = "/apollo/modules/common/data/alpha_param.json"
CHASSIS_PUB_RATE = 100

class VehicleCyberRTWrapper:

    def __init__(self, port = None):

        cyber.init()
        node = cyber.Node("alpha")

        self._aws_cfg = WSCfg({'port' : port})

        with open(ALPHA_PARAM_PATH) as alpha_params_file:
            self._vparams = VehicleParams(json.load(alpha_params_file))

        self.vehicle = Vehicle(self._vparams, self._aws_cfg)

        self._vehicle_estop = False
        self._vehicle_mode  = VEHICLE_MODE.UNKNOWN
        self._takeover_id   = None

        self._agear = chassis_pb2.Chassis.GEAR_NEUTRAL

        self._throttle = 0.0

        self._control_sub = node.create_reader('/apollo/control',
                                               control_cmd_pb2.ControlCommand,
                                               self.control_cb)

        self._chassis_pub = node.create_writer('/apollo/canbus/chassis',
                                               chassis_pb2.Chassis)

        self._chassis_spn = Spinner(self._chassis_loop, CHASSIS_PUB_RATE)
        self._chassis_spn.start()


    def control_cb(self, msg):

        vehicle_mode = self._vehicle_mode

        if msg.HasField("throttle"):
            self._throttle = (-1 * msg.brake) if msg.brake > 0 else msg.throttle

        if not vehicle_mode in (VEHICLE_MODE.DRIVE, VEHICLE_MODE.REVERSE):
            return

        if self._takeover_id != DEFAULT_TAKEOVER_ID:
            return

        # -------------------------------------------------

        if not self._vehicle_estop and msg.HasField("throttle"):

            if not isnan(self._throttle):
                self.vehicle.move(self._throttle)

        # ---------------------------------------------

        if msg.HasField("steering_target") and not isnan(msg.steering_target):

            sw_angle = msg.steering_target * 0.01 * self._vparams["max_sw_angle"]
            sw_rate = None

            if msg.HasField("steering_rate") and not isnan(msg.steering_rate):
                sw_rate = msg.steering_rate * 0.01 * self._vparams["max_sw_rate"]

            self.vehicle.steer(sw_angle, sw_rate)

        # ---------------------------------------------

        if msg.HasField("gear_location"):

            if self._agear != msg.gear_location:

                control_mode = self.agear_to_alpha_mode(msg.gear_location)

                if control_mode != vehicle_mode:

                    if control_mode == VEHICLE_MODE.DRIVE:
                        self.vehicle.drive()

                    elif control_mode == VEHICLE_MODE.REVERSE:
                        self.vehicle.reverse()

                self._agear = msg.gear_location

        # -------------------------------------------------

        # if msg.HasField("signal"):
        #     has_tsignal = msg.signal.HasField("turn_signal")
        #     has_elight  = msg.signal.HasField("emergency_light")


    def agear_to_alpha_mode(self, agear):

        if agear == chassis_pb2.Chassis.GEAR_REVERSE:
            return VEHICLE_MODE.REVERSE

        elif agear == chassis_pb2.Chassis.GEAR_DRIVE:
            return VEHICLE_MODE.DRIVE

        else:
            return VEHICLE_MODE.UNKNOWN


    def alpha_mode_to_apollo(self, mode, estop):

        amode = None
        agear = None

        if mode == VEHICLE_MODE.DRIVE:
            amode = chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE
            agear = chassis_pb2.Chassis.GEAR_DRIVE

        elif mode == VEHICLE_MODE.REVERSE:
            amode = chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE
            agear = chassis_pb2.Chassis.GEAR_REVERSE

        else:
            amode = chassis_pb2.Chassis.COMPLETE_MANUAL
            agear = chassis_pb2.Chassis.GEAR_NEUTRAL

        if estop:
            agear = chassis_pb2.Chassis.GEAR_PARKING

        return (amode, agear)


    def _chassis_loop(self):

        self._vehicle_mode  = self.vehicle.get_mode()
        self._vehicle_estop = self.vehicle.get_emergency_stop()
        self._takeover_id   = self.vehicle.get_takeover_id()

        speed    = self.vehicle.get_vehicle_speed()
        sw_angle = self.vehicle.get_steering_wheel_angle()

        sw_perc = sw_angle * 100.0 / self._vparams["max_sw_angle"]

        chassis_msg = chassis_pb2.Chassis()

        chassis_msg.speed_mps           = speed
        chassis_msg.steering_percentage = sw_perc

        if self._throttle > 0:
            chassis_msg.throttle_percentage = self._throttle
            chassis_msg.brake_percentage    = 0.0
        else:
            chassis_msg.throttle_percentage = 0.0
            chassis_msg.brake_percentage    = abs(self._throttle)

        amode, agear = self.alpha_mode_to_apollo(self._vehicle_mode,
                                                 self._vehicle_estop)

        chassis_msg.driving_mode  = amode
        chassis_msg.gear_location = agear

        chassis_msg.header.timestamp_sec = cyber_time.Time.now().to_sec()

        self._chassis_pub.write(chassis_msg)


    def spin(self):
        while cyber.ok():
            time.sleep(1)


if __name__ == "__main__":

    parser = ArgumentParser(description = "CyberRT Alpha Wrapper")

    parser.add_argument('-p', '--port', default = None)

    wrapper = VehicleCyberRTWrapper(port = parser.parse_args().port)
    wrapper.spin()
