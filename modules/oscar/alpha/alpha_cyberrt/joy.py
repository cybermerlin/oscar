#!/apollo/modules/oscar/alpha/alpha_venv/bin/python3

#############################################################################
# Copyright 2021 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import os
import sys
import time
import struct
import curses

from array import array
from fcntl import ioctl
from math  import sqrt, atan2, copysign, pi

from argparse import ArgumentParser

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.control_msgs import control_cmd_pb2
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.basic_msgs   import vehicle_signal_pb2

from modules.common_msgs.config_msgs import vehicle_config_pb2
import modules.tools.common.proto_utils as proto_utils

from alpha.protocol import VEHICLE_MODE
from alpha.loops    import Spinner


JOY_FILE_PATH = "/dev/input/js0"

APOLLO_VEHICLE_PARAM_PATH = "/apollo/modules/common/data/vehicle_param.pb.txt"

CONTROL_PUB_RATE = 30

READ_JOY_RATE  = 100
UI_UPDATE_RATE = 25

MAX_SW_TORQUE = 90.0
MAX_SW_ANGLE = 500.0
MAX_THROTTLE = 70.0
MAX_BRAKE = 70.0


class JoyTeleop:


    def __init__(self):

        self._axis_map   = []
        self._button_map = []

        self._axis_states   = {}
        self._button_states = {}

        self._prev_axis_states   = {}
        self._prev_button_states = {}

        self._joy_dev = None
        self._joy_init()

        self._teleop_allowed = False

        self._steering_by_angle      = False
        self._prev_steering_by_angle = False
        self._prev_angle = 0.0

        self._cmd = {
            'throttle': 0.0,
            'brake': 0.0,
            'sw_torque': 0.0,
            'sw_angle': 0.0,
            'left_turn_signal': False,
            'right_turn_signal': False,
            'emergency_lights': False,
            'emergency_stop': False,
            'mode': VEHICLE_MODE.MANUAL,
            'led': False
        }

        # Apollo params loading -----------------------------------------------

        apollo_vehicle_config = vehicle_config_pb2.VehicleConfig()
        proto_utils.get_pb_from_text_file(APOLLO_VEHICLE_PARAM_PATH, apollo_vehicle_config)
        apollo_vehicle_params = apollo_vehicle_config.vehicle_param
        global MAX_SW_ANGLE
        MAX_SW_ANGLE = apollo_vehicle_params.max_steer_angle / pi * 180

        cyber.init()
        node = cyber.Node("joy")

        self._chassis_sub = node.create_reader('/apollo/canbus/chassis',
                                               chassis_pb2.Chassis,
                                               self._canbus_cb)

        self._control_pub = node.create_writer('/apollo/control',
                                               control_cmd_pb2.ControlCommand)

        self._joy_spn = Spinner(self._joy_loop, READ_JOY_RATE)
        self._cmd_spn = Spinner(self._cmd_loop, CONTROL_PUB_RATE)

        self._joy_spn.start()
        self._cmd_spn.start()


    def _joy_init(self):

        self._joy_dev = open(JOY_FILE_PATH, 'rb')

        # These constants were borrowed from linux/input.h
        axis_names = {
            0x00 : 'xl',
            0x01 : 'yl',
            0x02 : 'zl',
            0x03 : 'xr',
            0x04 : 'yr',
            0x05 : 'zr',
            0x06 : 'throttle',
            0x07 : 'rudder',
            0x08 : 'wheel',
            0x09 : 'gas',
            0x0a : 'brake',
            0x10 : 'arrx',
            0x11 : 'arry',
            0x12 : 'hat1x',
            0x13 : 'hat1y',
            0x14 : 'hat2x',
            0x15 : 'hat2y',
            0x16 : 'hat3x',
            0x17 : 'hat3y',
            0x18 : 'pressure',
            0x19 : 'distance',
            0x1a : 'tilt_x',
            0x1b : 'tilt_y',
            0x1c : 'tool_width',
            0x20 : 'volume',
            0x28 : 'misc',
        }

        button_names = {
            0x120 : 'trigger',
            0x121 : 'thumb',
            0x122 : 'thumb2',
            0x123 : 'top',
            0x124 : 'top2',
            0x125 : 'pinkie',
            0x126 : 'base',
            0x127 : 'base2',
            0x128 : 'base3',
            0x129 : 'base4',
            0x12a : 'base5',
            0x12b : 'base6',
            0x12f : 'dead',
            0x130 : 'a',
            0x131 : 'b',
            0x132 : 'c',
            0x133 : 'x',
            0x134 : 'y',
            0x135 : 'z',
            0x136 : 'tl',
            0x137 : 'tr',
            0x138 : 'tl2',
            0x139 : 'tr2',
            0x13a : 'back',
            0x13b : 'start',
            0x13c : 'log',
            0x13d : 'thumbl',
            0x13e : 'thumbr',

            0x220 : 'dpad_up',
            0x221 : 'dpad_down',
            0x222 : 'dpad_left',
            0x223 : 'dpad_right',

            # XBox 360 controller uses these codes.
            0x2c0 : 'dpad_left',
            0x2c1 : 'dpad_right',
            0x2c2 : 'dpad_up',
            0x2c3 : 'dpad_down',
        }

        # Get number of axes and buttons.
        buf = array('B', [0])
        ioctl(self._joy_dev, 0x80016a11, buf) # JSIOCGAXES
        num_axes = buf[0]

        buf = array('B', [0])
        ioctl(self._joy_dev, 0x80016a12, buf) # JSIOCGBUTTONS
        num_buttons = buf[0]

        # Get the axis map.
        buf = array('B', [0] * 0x40)
        ioctl(self._joy_dev, 0x80406a32, buf) # JSIOCGAXMAP

        for axis in buf[:num_axes]:
            axis_name = axis_names.get(axis, '_(0x%02x)' % axis)
            self._axis_map.append(axis_name)
            self._axis_states[axis_name] = 0.0

        # Get the button map.
        buf = array('H', [0] * 200)
        ioctl(self._joy_dev, 0x80406a34, buf) # JSIOCGBTNMAP

        for button in buf[:num_buttons]:
            button_name = button_names.get(button, '_(0x%03x)' % button)
            self._button_map.append(button_name)
            self._button_states[button_name] = 0

        self._prev_axis_states = self._axis_states.copy()
        self._prev_button_states = self._button_states.copy()


    def _canbus_cb(self, msg):
        # Очень странная вещь: если отсутствует подписка на какой-то из топиков,
        # то при _control_pub.write процесс падает с Segmentation fault
        # обажаю cyberRT
        pass


    def _cmd_loop(self):

        if self._teleop_allowed:

            control_msg = control_cmd_pb2.ControlCommand()

            # GEAR ------------------------------------------------------------
            if self._cmd['mode'] == VEHICLE_MODE.MANUAL:
                control_msg.driving_mode = chassis_pb2.Chassis.COMPLETE_MANUAL
                control_msg.gear_location = chassis_pb2.Chassis.GEAR_NONE

            else:
                control_msg.driving_mode = chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE

                if self._cmd['mode'] == VEHICLE_MODE.REVERSE:
                    control_msg.gear_location = chassis_pb2.Chassis.GEAR_REVERSE
                else:
                    control_msg.gear_location = chassis_pb2.Chassis.GEAR_DRIVE

            # CONTROLS --------------------------------------------------------
            control_msg.throttle = self._cmd['throttle']
            control_msg.brake    = self._cmd['brake']

            control_msg.steering_target = self._cmd['sw_angle'] * 100.0 / MAX_SW_ANGLE

            # LIGHTS ----------------------------------------------------------
            control_msg.signal.emergency_light = self._cmd['emergency_lights']

            if self._cmd['left_turn_signal']:
                control_msg.signal.turn_signal = vehicle_signal_pb2.VehicleSignal.TURN_LEFT

            elif self._cmd['right_turn_signal']:
                control_msg.signal.turn_signal = vehicle_signal_pb2.VehicleSignal.TURN_RIGHT

            else:
                control_msg.signal.turn_signal = vehicle_signal_pb2.VehicleSignal.TURN_NONE

            control_msg.signal.low_beam = self._cmd['led']

            # EMERGENCY -------------------------------------------------------
            control_msg.parking_brake = self._cmd['emergency_stop']

            control_msg.header.timestamp_sec = cyber_time.Time.now().to_sec()

            self._control_pub.write(control_msg)


    def _update_cmd(self):

        # turn on/off teleop by joy -------------------------------------------
        if ((not self._prev_button_states["tl"]) and
            (not self._prev_button_states["tr"]) and
                 self._button_states["tl"]       and
                 self._button_states["tr"]):

            self._teleop_allowed = not self._teleop_allowed

        # throttle / brake ----------------------------------------------------
        if self._axis_states["zl"] > 0.01:
            self._cmd["throttle"] = 0.0
            self._cmd["brake"] =  self._axis_states["zl"] * MAX_BRAKE
        else:
            self._cmd["throttle"] = self._axis_states["zr"] * MAX_THROTTLE
            self._cmd["brake"] = 0.0

        # steering by torque
        self._cmd["sw_torque"] = self._axis_states["xl"] * -1.0 \
                                             * MAX_SW_TORQUE

        # steering by angle ---------------------------------------------------
        self._steering_by_angle = (sqrt(self._axis_states["xr"] ** 2 +
                                        self._axis_states["yr"] ** 2) > 0.99)

        if self._steering_by_angle:

            cur_angle = atan2(self._axis_states["xr"],
                              self._axis_states["yr"]) / 3.14 * 180 * (-1)

            if self._prev_steering_by_angle:
                self.delta_angle = cur_angle - self._prev_angle

                if abs(self.delta_angle) < 100:
                    self._cmd["sw_angle"] += self.delta_angle

                    if not (-MAX_SW_ANGLE < self._cmd["sw_angle"] < MAX_SW_ANGLE):

                        self._cmd["sw_angle"] = copysign(MAX_SW_ANGLE,
                                                         self._cmd["sw_angle"])

            self._prev_angle = cur_angle
            self._prev_steering_by_angle = True

        else:
            self._prev_steering_by_angle = False

        # mode ----------------------------------------------------------------
        if (not self._prev_button_states["a"] and self._button_states["a"]):
            if self._cmd["mode"] == VEHICLE_MODE.MANUAL:
                self._cmd["mode"] = VEHICLE_MODE.DRIVE
            else:
                self._cmd["mode"] = VEHICLE_MODE.MANUAL

        if (not self._prev_button_states["start"] and self._button_states["start"]):
            if self._cmd["mode"] == VEHICLE_MODE.REVERSE:
                self._cmd["mode"] = VEHICLE_MODE.DRIVE

        if (not self._prev_button_states["back"] and self._button_states["back"]):
            if self._cmd["mode"] == VEHICLE_MODE.DRIVE:
                self._cmd["mode"] = VEHICLE_MODE.REVERSE

        # emergency stop ------------------------------------------------------
        if (not self._prev_button_states["b"] and self._button_states["b"]):
            self._cmd["emergency_stop"] = not self._cmd["emergency_stop"]

        # lights --------------------------------------------------------------
        if (self._prev_axis_states["arrx"] == 0 and self._axis_states["arrx"] == 1):
            self._cmd["right_turn_signal"] = True
            self._cmd["left_turn_signal"]  = False
            self._cmd["emergency_lights"]   = False

        if (self._prev_axis_states["arrx"] == 0 and self._axis_states["arrx"] == -1):
            self._cmd["right_turn_signal"] = False
            self._cmd["left_turn_signal"]  = True
            self._cmd["emergency_lights"]   = False

        if (self._prev_axis_states["arry"] == 0 and self._axis_states["arry"] == -1):
            self._cmd["right_turn_signal"] = False
            self._cmd["left_turn_signal"]  = False
            self._cmd["emergency_lights"]   = True

        if (self._prev_axis_states["arry"] == 0 and self._axis_states["arry"] == 1):
            self._cmd["right_turn_signal"] = False
            self._cmd["left_turn_signal"]  = False
            self._cmd["emergency_lights"]   = False

        # led -----------------------------------------------------------------
        if (not self._prev_button_states["log"] and self._button_states["log"]):
            self._cmd["led"] = not self._cmd["led"]


    def _joy_loop(self):

        evbuf = self._joy_dev.read(8)

        if evbuf:
            timestamp, value, type, number = struct.unpack('IhBB', evbuf)

            if type & 0x01:
                button = self._button_map[number]
                self._prev_button_states[button] = self._button_states[button]
                self._button_states[button] = value

            if type & 0x02:
                axis = self._axis_map[number]
                self._prev_axis_states[axis] = self._axis_states[axis]
                fvalue = value  / 32767.0

                if axis == "zr" or axis == "zl":
                    self._axis_states[axis] = 0.5 * (fvalue + 1.0)  # [0:100]

                elif axis == "yr" or axis == "yl":
                    self._axis_states[axis] = -1.0 * fvalue

                elif axis == "arrx" or axis == "arry":
                    self._axis_states[axis] = int(fvalue)

                else:
                    self._axis_states[axis] = fvalue

            self._update_cmd()


    def spin(self, with_ui = True):

        if with_ui:

            update_delay = 1.0 / UI_UPDATE_RATE
            screen = curses.initscr()

            curses.start_color()
            curses.use_default_colors()
            curses.init_pair(1, curses.COLOR_RED, -1)
            curses.init_pair(2, curses.COLOR_GREEN, -1)

            while cyber.ok():

                screen.clear()

                screen.addstr(1, 0, " --- STATE ------------------------------------------")

                screen.addstr(3, 2, "TELEOP")
                if self._teleop_allowed:
                    screen.addstr(4, 2, "ON", curses.A_BOLD | curses.color_pair(2))
                else:
                    screen.addstr(4, 2, "OFF", curses.color_pair(1))

                screen.addstr(3, 18, "MODE")
                if (self._cmd["mode"] == VEHICLE_MODE.DRIVE or
                    self._cmd["mode"] == VEHICLE_MODE.REVERSE):
                    screen.addstr(4, 18, "AUTO", curses.A_BOLD)
                else:
                    screen.addstr(4, 18, "MANUAL")

                screen.addstr(3, 32, "DIRECTION")
                if self._cmd["mode"] == VEHICLE_MODE.MANUAL:
                    screen.addstr(4, 32, "-")
                else:
                    if self._cmd["mode"] == VEHICLE_MODE.REVERSE:
                        screen.addstr(4, 32, "REVERSE", curses.A_BOLD)
                    else:
                        screen.addstr(4, 32, "DRIVE", curses.A_BOLD)

                screen.addstr(3, 46, "EBRAKE")
                if self._cmd["emergency_stop"]:
                    screen.addstr(4, 46, "ON", curses.A_BOLD)
                else:
                    screen.addstr(4, 46, "OFF")

                screen.addstr(7, 1, "--- COMMANDS ---------------------------------------")

                screen.addstr(9, 3, "THROTTLE")
                if self._cmd["brake"] > 0:
                    screen.addstr(10, 2, str(format(-self._cmd["brake"], '.0f')),
                                         curses.A_BOLD)
                else:
                    screen.addstr(10, 3, str(format(self._cmd["throttle"], '.0f')),
                                         curses.A_BOLD)

                screen.addstr(9, 19, "SW_ANGLE")

                if self._steering_by_angle:
                    screen.addstr(10, 19, str(format(self._cmd["sw_angle"], '.0f')),
                                          curses.A_BOLD)
                else:
                    screen.addstr(10, 19, str(format(self._cmd["sw_angle"], '.0f')))

                screen.addstr(12, 1, "")

                screen.refresh()

                time.sleep(update_delay)

            curses.endwin()

        else:
            while cyber.ok():
                time.sleep(1)


if __name__ == "__main__":

    parser = ArgumentParser(description="Joy teleop for Alpha. Publish to Control channel.")
    parser.add_argument('--without-ui', default=True, action='store_false', dest='with_ui')

    args = parser.parse_args()

    teleop = JoyTeleop()
    teleop.spin(args.with_ui)
