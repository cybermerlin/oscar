///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/channel_checker.h"

#include <deque>

#include "cyber/proto/topology_change.pb.h"

namespace apollo {
namespace monitor {

using apollo::cyber::message::RawMessage;
using apollo::cyber::proto::ChangeMsg;
using apollo::cyber::proto::ChangeType;
using apollo::cyber::proto::OperateType;
using apollo::cyber::proto::RoleType;

constexpr auto kFpsTrackerHistoryTimeS = 5;

ReaderWrapper::ReaderWrapper(const std::string& channel,
                             std::weak_ptr<apollo::cyber::Node> node,
                             bool skip_latency)
    : skip_latency_(skip_latency), channel_(channel) {
  auto node_ptr = node.lock();

  reader_ = node_ptr->CreateReader<RawMessage>(
      channel_, [this](const auto&) { fps_tracker_.increment(); });
}

[[nodiscard]] apollo::oscar::monitor::ChannelStatus ReaderWrapper::get_info() {
  const bool has_writer = reader_->HasWriter();
  const double delay = reader_->GetDelaySec();

  apollo::oscar::monitor::ChannelStatus status;
  status.set_name(channel_);
  status.set_delay(delay);

  status.set_fps(fps_tracker_.get());
  status.set_has_writer(has_writer);

  reader_->Observe();

  auto msg = reader_->GetLatestObserved();

  if (!skip_latency_ && msg != nullptr &&
      header_struct_msg_.ParseFromString(msg->message) &&
      header_struct_msg_.header().has_timestamp_sec()) {
    const auto& header = header_struct_msg_.header();
    const auto& ts_sec = header.timestamp_sec();

    if (header.has_lidar_timestamp() && header.lidar_timestamp() != 0)
      status.set_lidar_latency(
          ts_sec - (static_cast<double>(header.lidar_timestamp()) / 1e9));

    if (header.has_camera_timestamp() && header.camera_timestamp() != 0)
      status.set_camera_latency(
          ts_sec - (static_cast<double>(header.camera_timestamp()) / 1e9));
  }

  reader_->ClearData();

  return status;
}

bool ChannelChecker::filterChannel(const std::string& channel) const {
  auto check_fn = [&](auto& element) { return channel.find(element) == 0; };

  switch (config_.lists_case()) {
    case ChannelMonitorConfig::LISTS_NOT_SET:
      return false;
    case ChannelMonitorConfig::kBlackList:
      return std::any_of(config_.black_list().channel_prefix().begin(),
                         config_.black_list().channel_prefix().end(), check_fn)
                 ? true
                 : false;
    case ChannelMonitorConfig::kWhiteList:
      return std::any_of(config_.white_list().channel_prefix().begin(),
                         config_.white_list().channel_prefix().end(), check_fn)
                 ? false
                 : true;
  }

  return false;
}

ChannelChecker::ChannelChecker(std::weak_ptr<apollo::cyber::Node> node,
                               const ChannelMonitorConfig& config)
    : node_(node), config_(config) {
  static auto channel_manager_ =
      apollo::cyber::service_discovery::TopologyManager::Instance()
          ->channel_manager();

  auto topologyCallback = [this](const ChangeMsg& change_msg) {
    if ((ChangeType::CHANGE_CHANNEL == change_msg.change_type() &&
         RoleType::ROLE_WRITER == change_msg.role_type() &&
         OperateType::OPT_JOIN == change_msg.operate_type())) {
      const std::string& channel_name = change_msg.role_attr().channel_name();

      if (filterChannel(channel_name)) return;

      if (readers_.find(channel_name) == readers_.end())
        readers_.emplace(
            std::piecewise_construct, std::forward_as_tuple(channel_name),
            std::forward_as_tuple(channel_name, node_, skip_latency_));
    }
  };

  channel_manager_->AddChangeListener(topologyCallback);
}

ChannelChecker::StatusT ChannelChecker::getStatus() noexcept {
  auto st = apollo::cyber::Time::Now().ToSecond();

  std::vector<std::future<apollo::oscar::monitor::ChannelStatus>>
      future_results;
  ChannelChecker::StatusT results;
  future_results.reserve(readers_.size());
  results.reserve(readers_.size());

  for (auto& it : readers_) {
    future_results.emplace_back(
        std::async([&]() { return it.second.get_info(); }));
  }

  for (auto& it : future_results) {
    results.emplace_back(it.get());
  }

#if 0
  ChannelChecker::StatusT results;
  for (auto& it : readers_) {
    results.emplace_back(it.second.get_info());
  }
#endif
  auto dur = apollo::cyber::Time::Now().ToSecond() - st;
  AINFO << "Getting Channel info: " << dur << " seconds";
  return results;
}

void ChannelChecker::handleResult(StatusT result,
                                  MonitorMessage& out_message) const noexcept {
  *out_message.mutable_channels() = {result.begin(), result.end()};
}

}  // namespace monitor
}  // namespace apollo
