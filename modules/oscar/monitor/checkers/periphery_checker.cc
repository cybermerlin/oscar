///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/periphery_checker.h"

#include <cstring>
#include <vector>

#include "absl/strings/str_split.h"

namespace apollo {
namespace monitor {

PeripheryDeviceChecker::PeripheryDeviceChecker(
    const PeripheryDeviceMonitorConfig& config)
    : name_(config.name()), last_connected_time_(current_time()) {
  switch (config.device_case()) {
    case PeripheryDeviceMonitorConfig::kIp:
      check_command_ =
          "ping -c 1 -s 1 -W 1 " + config.ip() + " > /dev/null 2>&1";
      break;
    case PeripheryDeviceMonitorConfig::kDevPath:
      check_command_ =
          "udevadm info " + config.dev_path() + " > /dev/null 2>&1";
      break;
    default:
      throw std::runtime_error("Unhandled device");
  }

  const std::vector<std::string> splitted_cmd =
      absl::StrSplit(check_command_, " ", absl::SkipWhitespace());
  const auto prog = std::string(splitted_cmd[0]);
  const std::string check_cmd = "which " + prog + " > /dev/null";
  bool has_cmd_installed = std::system(check_cmd.c_str()) == 0;

  if (!has_cmd_installed)
    throw std::runtime_error("Command not found: " + prog);
}

[[nodiscard]] PeripheryDeviceChecker::StatusT
PeripheryDeviceChecker::getStatus() noexcept {
  StatusT result;

  const int ret_val = return_value(check_command_);

  if (!was_connected_ && ret_val == 0) was_connected_ = true;

  if (ret_val == 0) last_connected_time_ = current_time();

  result.set_status(ret_val == 0 ? PeripheryConnectionStatus::CONNECTED
                    : was_connected_
                        ? PeripheryConnectionStatus::CONNECTION_LOST
                        : PeripheryConnectionStatus::NOT_CONNECTED);

  result.set_name(name_);
  result.set_delay(current_time() - last_connected_time_);

  return result;
}

void PeripheryDeviceChecker::handleResult(
    const PeripheryDeviceChecker::StatusT& result,
    MonitorMessage& out_message) const {
  auto el = out_message.mutable_periphery_devices()->Add();
  el->CopyFrom(result);
}

}  // namespace monitor
}  // namespace apollo
