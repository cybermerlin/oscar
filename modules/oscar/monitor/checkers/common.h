///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <chrono>
#include <cstdint>
#include <deque>
#include <mutex>
#include <string>

#define ONLY_MOVABLE(classname)                    \
  classname(const classname&) = delete;            \
  classname& operator=(const classname&) = delete; \
  classname& operator=(classname&&) = default;     \
  classname(classname&&) = default;                \
  ~classname() = default;

namespace apollo {
namespace monitor {

inline double current_time() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
             std::chrono::system_clock::now().time_since_epoch())
             .count() /
         1e9;
}

struct FpsHistoryElement {
  FpsHistoryElement(double ts, uint32_t messages)
      : timestamp(ts), messages_count(messages) {}
  double timestamp;
  uint32_t messages_count;
};

class FPSTracker {
 public:
  double get() {
    uint32_t count = 0;
    double current_ts = 0;

    {
      std::lock_guard<std::mutex> lg(data_mutex_);
      count = msg_counter_;
      msg_counter_ = 0;
      current_ts = current_msg_ts_;
    }

    if (count == 0) return 0;

    auto ts_diff = current_ts - last_msg_ts_;
    double fps = count / ts_diff;
    last_msg_ts_ = current_ts;

    return fps;
  }

  void increment() {
    std::lock_guard<std::mutex> lg(data_mutex_);
    msg_counter_++;
    current_msg_ts_ = current_time();
  }

 private:
  std::mutex data_mutex_;
  uint32_t msg_counter_ = 0;
  double current_msg_ts_ = 0;
  double last_msg_ts_ = current_time();
};

int return_value(const std::string& cmd);

}  // namespace monitor
}  // namespace apollo
