///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/system_checker.h"

#include <regex>
#include <unordered_set>

#include "cyber/cyber.h"
#include "modules/oscar/monitor/checkers/system_utils.h"

namespace {

using apollo::oscar::monitor::Sensor;

const std::regex regex_pattern(
    "(.*):( +)([+-]*[0-9]+\\.?[0-9]*)(\\ ?)([°a-zA-Z]+).*");

/*
Parsing of output of "sensors" command like:
nct6795-isa-0a20
Adapter: ISA adapter
in0:                    +1.44 V  (min =  +0.00 V, max =  +1.74 V)
in1:                    +1.03 V  (min =  +0.00 V, max =  +0.00 V)  ALARM
SYSTIN:                 +46.5°C    sensor = CPU diode
...
*/
std::vector<Sensor> parseSensorInfo(const std::vector<std::string>& info) {
  std::string current_group;
  std::smatch pieces_match;

  std::vector<Sensor> sensors;
  sensors.reserve(info.size());

  for (auto& line : info) {
    if (line.find(":") == std::string::npos) {
      current_group = line;
    } else if (std::regex_match(line, pieces_match, regex_pattern)) {
      Sensor sensor_info;
      sensor_info.set_name(current_group + "/" + pieces_match[1].str());
      sensor_info.set_value(std::stod(pieces_match[3]));

      auto unit =
          std::regex_replace(pieces_match[5].str(), std::regex("°"), "deg ");
      sensor_info.set_unit(unit);

      sensors.push_back(sensor_info);
    }
  }

  return sensors;
}
}  // namespace

namespace apollo {
namespace monitor {

SystemChecker::SystemChecker(const std::vector<std::string>& disk_paths) {
  std::unordered_set<std::string> added_devices;
  for (auto& path : disk_paths) {
    const auto device_name = getDeviceName(path);
    if (added_devices.find(device_name) == added_devices.end()) {
      disk_devices_map_.emplace(path, device_name);
      added_devices.insert(device_name);
    } else {
      AERROR << "Skipping path " << path << " because corresponding device "
             << device_name << " already added.";
    }
  }
}

[[nodiscard]] SystemChecker::StatusT SystemChecker::getStatus() const {
  SystemChecker::StatusT result;

  result.set_memory_usage_percentage(GetSystemMemoryUsage());
  result.set_cpu_usage_percentage(GetSystemCPUUsage());

  const auto gpu_info = getGPUInfo();

  std::for_each(gpu_info.begin(), gpu_info.end(), [&](const auto& el) {
    auto gpu_pb = result.add_gpu();
    gpu_pb->set_name(el.name);
    gpu_pb->set_memory_total_mb(el.memory_total_mb);
    gpu_pb->set_memory_usage_mb(el.memory_usage_mb);
  });

  auto sensor_info =
      parseSensorInfo(sensorsInfo());  // relatively slow - 0.01 sec

  *result.mutable_sensors() = {sensor_info.begin(), sensor_info.end()};

  for (auto& [path, device] : disk_devices_map_) {
    auto disk_load = result.add_disks_load();

    disk_load->set_path(path);
    disk_load->set_device(device);
    disk_load->set_io_load_percentage(GetSystemDiskload(device));
    disk_load->set_free_space_gb(getDiskSpace(path));
  }

  return result;
}

void SystemChecker::handleResult(const SystemChecker::StatusT& result,
                                 MonitorMessage& out_message) const noexcept {
  *out_message.mutable_system() = result;
}
}  // namespace monitor
}  // namespace apollo
