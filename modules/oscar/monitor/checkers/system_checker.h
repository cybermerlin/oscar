///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "modules/oscar/monitor/proto/monitor_message.pb.h"

#include "cyber/common/macros.h"
#include "modules/oscar/monitor/checkers/common.h"
#include "modules/oscar/monitor/checkers/system_utils.h"

using apollo::oscar::monitor::MonitorMessage;

namespace apollo {
namespace monitor {

class SystemChecker {
 public:
  ONLY_MOVABLE(SystemChecker)

  explicit SystemChecker(const std::vector<std::string>& disk_paths);

  using StatusT = apollo::oscar::monitor::SystemStatus;

  StatusT getStatus() const;

  void handleResult(const SystemChecker::StatusT& result,
                    MonitorMessage& out_message) const noexcept;

 private:
  std::unordered_map<std::string, std::string> disk_devices_map_;
};

}  // namespace monitor
}  // namespace apollo
