///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/nodes_checker.h"

#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "cyber/proto/role_attributes.pb.h"

#include "cyber/cyber.h"
#include "modules/oscar/monitor/checkers/system_utils.h"

constexpr std::array<std::string_view, 3> black_list_node_prefixes = {
    "logger",
    "MonitorReader",
    "TransformListener",
};

namespace apollo {
namespace monitor {

using apollo::cyber::proto::RoleAttributes;

using ActiveNodesMap =
    std::map<std::string, apollo::oscar::monitor::NodeStatus>;

[[nodiscard]] NodesChecker::StatusT NodesChecker::getStatus() {
  for (auto& node : nodes_) {
    node.set_is_alive(false);
  }

  NodesChecker::StatusT results;

  static auto* topology =
      apollo::cyber::service_discovery::TopologyManager::Instance();

  std::vector<RoleAttributes> current_nodes;
  topology->node_manager()->GetNodes(&current_nodes);

  for (auto& node : current_nodes) {
    auto pid = node.process_id();
    auto& node_name = node.node_name();

    if (std::any_of(black_list_node_prefixes.begin(),
                    black_list_node_prefixes.end(), [&](auto&& prefix) {
                      return node_name.find(prefix) != std::string::npos;
                    }))
      continue;

    double memory_usage = GetMemoryUsage(pid, node_name) * 1024;
    double cpu_usage = GetCPUUsage(pid, node_name);

    auto saved_node_it =
        std::find_if(nodes_.begin(), nodes_.end(),
                     [&](auto& el) { return el.name() == node_name; });
    if (saved_node_it == nodes_.end()) {
      apollo::oscar::monitor::NodeStatus node_status;
      node_status.set_name(node_name);
      nodes_.emplace_back(std::move(node_status));
      saved_node_it = std::prev(nodes_.end());
    }

    saved_node_it->set_is_alive(true);
    saved_node_it->set_memory_usage_mb(memory_usage);
    saved_node_it->set_cpu_usage_percentage(cpu_usage);
    saved_node_it->set_pid(pid);
  }
  return nodes_;
}

void NodesChecker::handleResult(const NodesChecker::StatusT& result,
                                MonitorMessage& out_message) {
  *out_message.mutable_active_nodes() = {result.begin(), result.end()};
}

}  // namespace monitor
}  // namespace apollo
