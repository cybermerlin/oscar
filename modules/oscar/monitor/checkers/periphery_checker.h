///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "modules/oscar/monitor/proto/monitor_config.pb.h"
#include "modules/oscar/monitor/proto/monitor_message.pb.h"

#include "cyber/cyber.h"
#include "modules/oscar/monitor/checkers/common.h"

namespace apollo {
namespace monitor {

using apollo::oscar::monitor::MonitorMessage;
using apollo::oscar::monitor::PeripheryConnectionStatus;
using apollo::oscar::monitor::PeripheryDeviceMonitorConfig;
using apollo::oscar::monitor::PeripheryDeviceStatus;

class PeripheryDeviceChecker {
 public:
  ONLY_MOVABLE(PeripheryDeviceChecker)

  explicit PeripheryDeviceChecker(const PeripheryDeviceMonitorConfig& config);

  using StatusT = PeripheryDeviceStatus;

  [[nodiscard]] StatusT getStatus() noexcept;
  void handleResult(const StatusT& result, MonitorMessage& out_message) const;

 private:
  const std::string name_;
  const std::string dev_path_;
  std::string check_command_;
  bool was_connected_ = false;

  double last_connected_time_;
};

}  // namespace monitor
}  // namespace apollo
