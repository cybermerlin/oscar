///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/oscar_monitor_component.h"

#include <cstdio>
#include <cstdlib>
#include <string>
#include <type_traits>
#include <utility>

namespace apollo {
namespace monitor {

bool OscarMonitor::Init() {
  if (!apollo::cyber::common::GetProtoFromFile(config_file_path_, &config_)) {
    AERROR << "Unable to load gnss conf file: " << config_file_path_;
    return false;
  }
  AINFO << "Config: " << config_.DebugString();

  for (auto& eth_checker_conf : config_.periphery_devices()) {
    checkers_.emplace_back(std::in_place_type<PeripheryDeviceChecker>,
                           eth_checker_conf);
  }

  const std::vector<std::string> disk_paths{
      config_.disk_monitoring_paths().begin(),
      config_.disk_monitoring_paths().end()};

  checkers_.emplace_back(std::in_place_type<SystemChecker>, disk_paths);
  checkers_.emplace_back(std::in_place_type<NodesChecker>, node_);

  // ChannelChecker should be last added Checker. Otherwise, somewhere maybe
  // data race. Need to check it
  checkers_.emplace_back(std::in_place_type<ChannelChecker>, node_,
                         config_.channel_monitor());

  results_.resize(checkers_.size());
  writer_ = node_->CreateWriter<MonitorMessage>("/oscar/monitor");

  return true;
}

bool OscarMonitor::Proc() {
  auto out_message = std::make_shared<MonitorMessage>();

  // send requests in different threads
  for (size_t i = 0, sz = checkers_.size(); i < sz; i++) {
    results_[i] = std::visit(
        [](auto&& variant) {
          return future_result_variant(
              std::async([&]() { return variant.getStatus(); }));
        },
        checkers_[i]);
  }

  // process results
  for (size_t i = 0, sz = checkers_.size(); i < sz; i++) {
    std::visit(
        [&](auto& checker) {
          auto result =
              std::get<variant_future_result_t<decltype(checker)>>(results_[i])
                  .get();
          checker.handleResult(result, *out_message);
        },
        checkers_[i]);
  }

  out_message->mutable_header()->set_timestamp_sec(
      apollo::cyber::Time::Now().ToSecond());
  writer_->Write(out_message);
  return true;
}
}  // namespace monitor
}  // namespace apollo
