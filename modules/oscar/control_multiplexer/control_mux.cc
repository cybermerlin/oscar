#include "control_mux.h"

using apollo::cyber::Timer;

namespace apollo {
namespace control {

void ControlMux::ControlCallback(const std::shared_ptr<apollo::control::ControlCommand>& msg, unsigned int idx) {
  // reset channel timer
  timers_[idx]->Stop();
  timers_[idx]->Start();
  // set source is active
  ctl_readers_[idx].is_active = true;

  // check if this source allowed to write
  if ((allowed_source_id_ == 0)   || 
      (allowed_source_id_ == idx) ||
      (ctl_readers_[idx].priority < ctl_readers_[allowed_source_id_].priority)) {
        if (allowed_source_id_ != idx) {
          allowed_source_id_ = idx;
          AERROR << "Changing source to " << idx;
        }
        control_writer_->Write(msg);
  }
}

void ControlMux::OnLocalTimer(unsigned int idx) {
  AERROR << "Source " << idx << " is inactive now";
  ctl_readers_[idx].is_active = false;
  if (allowed_source_id_ == idx) {
    allowed_source_id_ = 0;
    AERROR << "Changing source to 0";
  }
  timers_[idx]->Stop();
}

bool ControlMux::Init() {
  allowed_source_id_ = 0;
  unsigned int idx = 0;
  std::shared_ptr<apollo::cyber::Node> control_mux_node_(apollo::cyber::CreateNode("control_mux_node"));
  control_writer_ = control_mux_node_->CreateWriter<ControlCommand>("/apollo/control");
  // read config and get channels list
  mux_parameters_ = YAML::LoadFile("/apollo/modules/control/control_multiplexer/mux_parameters.yaml")["channel_list"];
  // filling vector with priority data
  for (YAML::const_iterator it=mux_parameters_.begin(); it != mux_parameters_.end(); it++) {
    reader_ = control_mux_node_->CreateReader<apollo::control::ControlCommand>
      (it->second["name"].as<std::string>(), boost::bind(&ControlMux::ControlCallback, this, _1, idx++));
    reader_item_.reader = reader_;
    reader_item_.priority = it->second["priority"].as<int>();
    reader_item_.timeout_ms = it->second["timeout_ms"].as<double>();
    reader_item_.is_active = false;
    ctl_readers_.push_back(reader_item_);
    timers_.emplace_back(new Timer(reader_item_.timeout_ms, [this, idx]() {this->OnLocalTimer(idx);}, false));
    if (idx == 1)
    {
      std::cout << "\033[1m\033[32mChannel " << idx << " is " << it->second["name"].as<std::string>() << "\033[0m"<<std::endl;
    }
    else
    std::cout << "\033[1m\033[33mChannel " << idx << " is " << it->second["name"].as<std::string>() << "\033[0m"<<std::endl;
  }
  return true;
}

bool ControlMux::Proc() {
  // set inactive channels
  return true;
}

}
}