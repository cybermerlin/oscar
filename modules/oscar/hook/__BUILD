###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.                      #
#                                                                             #
# Created by Dema Nikolay <ndema2301@gmail.com>                               #
#                                                                             #
# Licensed under the Apache License, Version 2.0 (the "License");             #
# you may not use this file except in compliance with the License.            #
# You may obtain a copy of the License at                                     #
#                                                                             #
# http://www.apache.org/licenses/LICENSE-2.0                                  #
#                                                                             #
# Unless required by applicable law or agreed to in writing, software         #
# distributed under the License is distributed on an "AS IS" BASIS,           #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    #
# See the License for the specific language governing permissions and         #
# limitations under the License.                                              #
###############################################################################

load("@rules_python//python:defs.bzl", "py_binary")

package(default_visibility = ["//visibility:public"])

py_binary(
    name = "oscar_hook",
    srcs = ["src/oscar_hook.py"],
    data = ["src/config.json"],
    deps = [
        "//cyber/python/cyber_py3:cyber",
        "//cyber/python/cyber_py3:cyber_time",
        "//modules/common_msgs/chassis_msgs:chassis_py_pb2",
        "//modules/common_msgs/localization_msgs:localization_py_pb2",
        "//modules/common_msgs/sensor_msgs:gnss_best_pose_py_pb2",
        "//modules/common_msgs/dreamview_msgs:hmi_status_py_pb2",
        "//modules/common_msgs/control_msgs:control_cmd_py_pb2",
        "//modules/common_msgs/routing_msgs:routing_py_pb2",
        "//modules/common_msgs/routing_msgs:poi_py_pb2",
        "//modules/tools/common:proto_utils",
    ],
)
