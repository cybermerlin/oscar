import logging
import json
import paho.mqtt.client as mqtt
from time import sleep


logging.basicConfig(level='INFO')


class MQTTTransport:
    def __init__(self, broker_address, uuid, car_interface, period):
        self.car_interface = car_interface
        self.uuid = uuid
        self.client = mqtt.Client("apollo")
        self.client.connect(broker_address)
        self.period = period

        def on_message(client, userdata, message):
            if message.topic == f'{uuid}/start':
                self.car_interface.start_auto()

            if message.topic == f'{uuid}/astop':
                self.car_interface.stop_auto()

            '''
            if message.topic == 'routing_request':
                self.send_routing_request(message.payload)
            '''

        self.client.subscribe([(f'{uuid}/stop', 0), (f'{uuid}/start', 0), (f'{uuid}/routing_request', 0)])
        self.client.on_message = on_message

    def start(self):
        self.client.loop_start()

    def stop(self):
        self.client.loop_stop()

    def send_messages(self):
        while self.car_interface.hook.cyber.ok():

            msgs = self.car_interface.get_info(self.uuid)
            for msg in msgs:
                topic, payload = msg
                payload = json.dumps(payload)
                self.client.publish(topic, str(payload))
            sleep(self.period)


if __name__ == '__main__':
    pass
