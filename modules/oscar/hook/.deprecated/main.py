from mqtt_transport import MQTTTransport
from apollo_contact_layer import ApolloAutoHook
from car_interface import CarInterface
import json
import signal
import sys


def read_config():
    with open('config.json', 'r') as config:
        data = json.load(config)

    return data


if __name__ == '__main__':
    broker_connection_params = read_config()
    broker_address, broker_port, uuid = broker_connection_params.values()

    apollo_hook = ApolloAutoHook()

    car_interface = CarInterface(apollo_hook)

    mqtt_transport = MQTTTransport(broker_address, uuid, car_interface, 0.05)

    mqtt_transport.start()
    mqtt_transport.send_messages()


