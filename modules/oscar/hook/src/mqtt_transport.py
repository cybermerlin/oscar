# #############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# #############################################################################

import logging
import json
import paho.mqtt.client as mqtt
from time import sleep
from car_interface import CarInterface


logging.basicConfig(level='INFO')


class MQTTTransport:
    def __init__(self, broker_address: str, uuid: str, car_interface: CarInterface, period: float):
        self.car_interface = car_interface
        self.uuid = uuid
        self.client = mqtt.Client(f'apollo_{uuid}')
        self.client.connect(broker_address)
        self.period = period
        self.client.subscribe([(f'{uuid}/stop', 0), (f'{uuid}/start', 0), (f'{uuid}/routing_request', 0)])
        self.client.on_message = self.__on_message

    def __on_message(self, client, userdata, message):
        if message.topic == f'{self.uuid}/start':
            self.car_interface.start_auto()

        if message.topic == f'{self.uuid}/stop':
            self.car_interface.stop_auto()

        '''
        if message.topic == 'routing_request':
            self.send_routing_request(message.payload)
        '''

    def start(self):
        self.client.loop_start()

    def stop(self):
        self.client.loop_stop()

    def send_messages(self):
        while self.car_interface.hook.cyber.ok():

            msgs = self.car_interface.get_info(self.uuid)
            for msg in msgs:
                topic, payload = msg
                payload = json.dumps(payload)
                self.client.publish(topic, payload)
            sleep(self.period)


if __name__ == '__main__':
    pass
