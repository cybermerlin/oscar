# #############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# #############################################################################

import logging
import os
import signal
from collections import OrderedDict
from time import time
from math import pi
from cyber.python.cyber_py3 import cyber, cyber_time
from modules.localization.proto.localization_pb2 import LocalizationEstimate
from modules.canbus.proto.chassis_pb2 import Chassis
from modules.drivers.gnss.proto.gnss_best_pose_pb2 import GnssBestPose
from modules.routing.proto.routing_pb2 import RoutingRequest, LaneWaypoint, RoutingResponse
from modules.dreamview.proto.hmi_status_pb2 import HMIStatus
from modules.control.proto.control_cmd_pb2 import ControlCommand


logging.basicConfig(level=logging.INFO)


class ApolloAutoHook:
    def __init__(self):
        self.cyber = cyber
        self.cyber.init()
        self.cyber_node = cyber.Node("oscar/hook")

        '''INITIALIZING WRITERS'''

        self.routing_writer = self.cyber_node.create_writer('/apollo/routing_request', RoutingRequest)
        self.control_writer = self.cyber_node.create_writer('/apollo/control', ControlCommand)
        self.hmi_writer = self.cyber_node.create_writer("/apollo/hmi/status", HMIStatus)

        '''INITIALIZING READERS'''

        self.cyber_node.create_reader("/apollo/localization/pose", LocalizationEstimate, self._localization_callback)
        self.cyber_node.create_reader("/apollo/canbus/chassis", Chassis, self._canbus_callback)
        self.cyber_node.create_reader("/apollo/sensor/gnss/best_pose", GnssBestPose, self._gnss_callback)
        self.cyber_node.create_reader("/apollo/routing_response", RoutingResponse, self._routing_callback)
        self.cyber_node.create_reader("/apollo/hmi/status", HMIStatus, self._hmi_callback)

        '''KEEPING DATA'''

        self.loc = {"x": 0.0, "y": 0.0, "z": 0.0, "heading": 0.0}
        self.veh = {"speed": 0.0, "steering": 0.0, "throttle": 0.0, "brake": 0.0}
        self.gps = {"latitude": 0.0, "longitude": 0.0}

        self.routing_info = {"length": 0}

        self.hmi = {
            "map": "",
            "car": "",
            "mode": ""
        }

        self.time = time()
        self.start_auto_time = None

    def set_start_time(self):
        self.start_auto_time = cyber_time.Time.now().to_sec()

    def _localization_callback(self, localization_msg):
        self.loc["x"] = localization_msg.pose.position.x
        self.loc["y"] = localization_msg.pose.position.y
        self.loc["z"] = localization_msg.pose.position.z
        self.loc["heading"] = localization_msg.pose.heading / pi * 180.0
        self.loc["time"] = time()

    def _canbus_callback(self, canbus_msg):
        self.veh["speed"] = canbus_msg.speed_mps
        self.veh["steering"] = canbus_msg.steering_percentage
        self.veh["throttle"] = canbus_msg.throttle_percentage
        self.veh["brake"] = canbus_msg.brake_percentage

    def _gnss_callback(self, gnss_msg):
        self.gps["longitude"] = gnss_msg.longitude
        self.gps["latitude"] = gnss_msg.latitude

    def _routing_callback(self, routing_msg):
        self.routing_info["length"] = routing_msg.measurement.distance
        lanes_count = len(routing_msg.road)
        lanes = [routing_msg.road[i].passage[0].segment[0].id for i in range(lanes_count)]
        first_point = [routing_msg.road[i].passage[0].segment[0].start_s for i in range(lanes_count)]
        last_point = [routing_msg.road[i].passage[0].segment[0].end_s for i in range(lanes_count)]
        roads = OrderedDict()

        for i in range(lanes_count):
            roads[lanes[i]] = [first_point[i], last_point[i]]

        self.routing_response = [
            self.hmi["map"],
            roads,
            [[routing_msg.routing_request.waypoint[0].pose.x, routing_msg.routing_request.waypoint[0].pose.y],
             [routing_msg.routing_request.waypoint[1].pose.x, routing_msg.routing_request.waypoint[1].pose.y]]
        ]
        logging.info(self.routing_response)

    def _hmi_callback(self, hmi_msg):
        self.hmi = {
            "map": hmi_msg.current_map.lower().replace(" ", "_"),
            "car": hmi_msg.current_vehicle,
            "mode": hmi_msg.current_mode
        }
        self.hmi_last_msg = hmi_msg

    @staticmethod
    def _kill():
        cyber.shutdown()
        os.kill(os.getpid(), signal.SIGTERM)


if __name__ == '__main__':
    '''
    def _localization_callback(localization_msg):
        sleep(2)
        logging.info(localization_msg)


    def _hmi_callback(hmi_msg):
        sleep(1)
        logging.info(hmi_msg)


    cyber.init()
    cyber_node = cyber.Node("listener")
    cyber_node.create_reader("/apollo/localization/pose", LocalizationEstimate, _localization_callback)
    cyber_node.create_reader("/apollo/hmi/status", HMIStatus, _hmi_callback)
    try:
        cyber_node.spin()
    except KeyboardInterrupt:
        print(3)
        sys.exit()
    for thread in threading.enumerate():
        if 'Dummy' in thread.name:

            print(thread.name)
    cyber.waitforshutdown()
    print(5)
    '''
    hook = ApolloAutoHook()

    while hook.cyber.ok():
        print(hook.loc)
