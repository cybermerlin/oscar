# #############################################################################
# Copyright 2020 ScPA StarLine Ltd. All Rights Reserved.
#
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# #############################################################################

from mqtt_transport import MQTTTransport
from apollo_contact_layer import ApolloAutoHook
from car_interface import CarInterface
import os
import json


def read_config():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f'{dir_path}/config.json', 'r') as config:
        data = json.load(config)

    return data


if __name__ == '__main__':
    broker_connection_params = read_config()
    broker_host, broker_port, uuid = broker_connection_params.values()

    apollo_hook = ApolloAutoHook()

    car_interface = CarInterface(apollo_hook)

    mqtt_transport = MQTTTransport(broker_host, uuid, car_interface, 0.05)

    mqtt_transport.start()
    mqtt_transport.send_messages()


