#pragma once

#include <memory>
#include <vector>
#include <cstdlib>
#include <thread>

#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"
#include "cyber/time/time.h"
#include "cyber/time/duration.h"
#include "modules/oscar/supervisor/common/supervisor_runner.h"
#include "cyber/common/log.h"
#include "modules/common/adapters/adapter_gflags.h"
#include "cyber/component/timer_component.h"
#include "yaml-cpp/yaml.h"

#include "modules/oscar/supervisor/submodules/proto/sv_prc_msg.pb.h"
#include "modules/common_msgs/perception_msgs/perception_obstacle.pb.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
namespace apollo {
namespace supervisor {

class PRCSupervisor : public SupervisorRunner {
 public:
  PRCSupervisor();
  void RunOnce(const double current_time) override;
  void GetStatus(std::string* submodule_name, int* status, std::string* debug_msg);
 private:
  int status_;
  std::string debug_msg_;
  double vehicle_x_, vehicle_y_;
  YAML::Node lidar_config_;
  int lidar_rays_quantity_;
  std::shared_ptr<apollo::cyber::Node> sv_prc_node_;
  std::shared_ptr<apollo::cyber::Reader<apollo::perception::PerceptionObstacles>> prc_reader_;
  std::shared_ptr<apollo::cyber::Reader<apollo::drivers::PointCloud>> lidar_reader_;
  std::shared_ptr<cyber::Writer<sv_prc_msg>> prc_status_writer_;
};

}
}