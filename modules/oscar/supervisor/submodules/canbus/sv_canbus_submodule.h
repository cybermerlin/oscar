#pragma once

#include <memory>
#include <vector>
#include <cstdlib>
#include <thread>

#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"
#include "cyber/time/time.h"
#include "cyber/time/duration.h"
#include "modules/oscar/supervisor/common/supervisor_runner.h"
#include "cyber/common/log.h"
#include "modules/common/adapters/adapter_gflags.h"
#include "cyber/component/timer_component.h"

#include "modules/oscar/supervisor/submodules/proto/sv_canbus_msg.pb.h"
#include "modules/common_msgs/chassis_msgs/chassis.pb.h"
#include "modules/common_msgs/chassis_msgs/chassis_detail.pb.h"
namespace apollo {
namespace supervisor {

class CANBUSSupervisor : public SupervisorRunner {
 public:
  CANBUSSupervisor();
  void RunOnce(const double current_time) override;
  void GetStatus(std::string* submodule_name, int* status, std::string* debug_msg);
 private:
  int status_;
  std::string debug_msg_;
  std::shared_ptr<apollo::cyber::Node> sv_canbus_node_;
  std::shared_ptr<apollo::cyber::Reader<apollo::canbus::Chassis>> canbus_chassis_reader_;
  std::shared_ptr<apollo::cyber::Reader<apollo::canbus::ChassisDetail>> canbus_chassis_detail_reader_;
  std::shared_ptr<cyber::Writer<sv_canbus_msg>> canbus_status_writer_;
};

}
}