#!/usr/bin/env bash

CONTAINER_NAME=tracy-capture
TRACY_VERSION=0.9

[ -f /.dockerenv ] && echo "Capturing should be run outside docker container. Exiting." && exit

docker container kill $CONTAINER_NAME >/dev/null 2>&1 && sleep 0.1

docker run -ti \
    --rm \
    --ipc=host \
    --network host \
    --name $CONTAINER_NAME \
    -v $(pwd):/workspace/save_dir \
    -u "$(id -u):$(id -g)" \
    registry.gitlab.com/starline/oscar_utils/tracy/tracy_docker/capture_v$TRACY_VERSION:latest "$@"
