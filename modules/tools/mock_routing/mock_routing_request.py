#!/usr/bin/env python3

###############################################################################
# Copyright 2017 The Apollo Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
"""
Insert routing request
Usage:
    mock_routing_request.py
"""
import argparse
import os
import sys
import time

from cyber.python.cyber_py3 import cyber
from cyber.python.cyber_py3 import cyber_time
from modules.common_msgs.routing_msgs import routing_pb2


def main():
    """
    Main rosnode
    """
    cyber.init()
    node = cyber.Node("mock_routing_requester")
    sequence_num = 0

    routing_request = routing_pb2.RoutingRequest()

    routing_request.header.timestamp_sec = cyber_time.Time.now().to_sec()
    routing_request.header.module_name = 'routing_request'
    routing_request.header.sequence_num = sequence_num
    sequence_num = sequence_num + 1

    # # CubeTown parking
    # waypoint = routing_request.waypoint.add()
    # waypoint.pose.x = 592748.760861910
    # waypoint.pose.y = 4134482.618303716
    # waypoint.id = 'lane_12'
    # waypoint.s = 14.082816205

    # waypoint = routing_request.waypoint.add()
    # waypoint.pose.x = 592701.953367526
    # waypoint.pose.y = 4134482.213870728
    # waypoint.id = 'lane_12'
    # waypoint.s = 60.892195944

    # routing_request.parking_info.parking_space_id = "parking_space_0"
    # routing_request.parking_info.parking_point.x = 592715.303712195
    # routing_request.parking_info.parking_point.y = 4134487.854777624

    # AutonomouStuff parking
    waypoint = routing_request.waypoint.add()
    waypoint.pose.x = 596546.17
    waypoint.pose.y = 4137697.84
    waypoint.id = 'lane_6'
    waypoint.s = 12.198746816

    waypoint = routing_request.waypoint.add()
    waypoint.pose.x = 596582.48
    waypoint.pose.y = 4137761.17
    waypoint.id = 'lane_6'
    waypoint.s = 96.811980830

    routing_request.parking_info.parking_space_id = "PS_66"
    routing_request.parking_info.parking_point.x = 596582.48
    routing_request.parking_info.parking_point.y = 4137761.17

    writer = node.create_writer('/apollo/routing_request',
                                routing_pb2.RoutingRequest)
    time.sleep(2.0)
    print("routing_request", routing_request)
    writer.write(routing_request)


if __name__ == '__main__':
    main()
