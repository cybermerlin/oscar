#!/usr/bin/env bash

###############################################################################
# Copyright 2019 Nikolay Dema. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

OSCAR_ROOT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../.." && pwd)"
OSCAR_TOOLS_DIR=$OSCAR_ROOT_DIR/modules/oscar/tools

function oscarcd() {

    local USAGE_HINT="Usage: oscarcd: {''|tools|bag|log|car-cfg|help}\n \
    change directory according to used option: \n\
    [empty] - /apollo                           - project root \n\
    tools   - /apollo/modules/oscar/tools - utilities for calibration, visualization and extraction\n\
    bag     - /apollo/data/bag                  - records from data recorder (bag is inherited naming from ROS terminology)\n\
    log     - /apollo/data/log                  - logs from modules output\n\
    car-cfg - /apollo/modules/calibration/data  - car configs\n\
    help    - show this help"

    TARGET_FOLDER="placeholder"
    case $1 in
    "")
        TARGET_FOLDER=$OSCAR_ROOT_DIR
        ;;
    tools)
        TARGET_FOLDER=$OSCAR_ROOT_DIR/modules/oscar/tools
        ;;
    car-cfg)
        TARGET_FOLDER=$OSCAR_ROOT_DIR/modules/calibration/data
        ;;
    bag)
        TARGET_FOLDER=$OSCAR_ROOT_DIR/data/bag
        ;;
    log)
        TARGET_FOLDER=$OSCAR_ROOT_DIR/data/log
        ;;
    help)
        echo -e "$USAGE_HINT"
        return
        ;;
    \?)
        echo -e "$USAGE_HINT"
        return
        ;;
    esac

    [ -d $TARGET_FOLDER ] && cd $TARGET_FOLDER || echo -e "$USAGE_HINT"
}

_oscar_completions() {

    OSCAR_COMPL_LIST="map mapcreator docker alpha trajectory calibration records help foxglove tracy carla"

    if [ "${#COMP_WORDS[@]}" == "2" ]; then
        COMPREPLY=($(compgen -W "${OSCAR_COMPL_LIST}" -- "${COMP_WORDS[1]}"))
    fi

    if [ "${COMP_WORDS[1]}" == "docker" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "start stop enter pull_images rerun_containers" -- "${COMP_WORDS[2]}"))
        fi

    elif [ "${COMP_WORDS[1]}" == "mapcreator" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "install update start" -- "${COMP_WORDS[2]}"))
        else
            if [ "${COMP_WORDS[2]}" == "install" ] && [ ! -f /.dockerenv ]; then
                if [ "${#COMP_WORDS[@]}" == "4" ]; then
                    COMPREPLY=($(compgen -W "--docker" -- "${COMP_WORDS[3]}"))
                fi
            fi
        fi

    elif [ "${COMP_WORDS[1]}" == "map" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "generate_bin_and_route" -- "${COMP_WORDS[2]}"))
        elif [ "${COMP_WORDS[2]}" == "generate_bin_and_route" ]; then
            local map_path=("$OSCAR_ROOT_DIR/modules/map/data/")
            COMPREPLY=($(cd $map_path && compgen -d -- "${COMP_WORDS[3]}"))

            if [ "${COMP_WORDS[3]}" == "" ]; then
                COMPREPLY=("${COMPREPLY[@]/#/$map_path}")
            fi
        fi

    elif [ "${COMP_WORDS[1]}" == "alpha" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "stop joy params takeover periphery" -- "${COMP_WORDS[2]}"))
        fi

    elif [[ "${COMP_WORDS[1]}" == "records" ]]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "extraction checking" -- "${COMP_WORDS[2]}"))
        elif [ "${#COMP_WORDS[@]}" == "4" ]; then
            local cmd=$1 cur=$2 pre=$3
            local arr i file
            arr=($(cd "$OSCAR_TOOLS_DIR/${COMP_WORDS[2]}/${COMP_WORDS[2]}" && compgen -f -- "$cur"))
            COMPREPLY=()
            j=0

            [[ $cur == "" ]] && COMPREPLY[j++]="help"
            [[ $cur == "" ]] && COMPREPLY[j++]="test"

            for ((i = 0; i < ${#arr[@]}; ++i)); do
                file=${arr[i]}
                ([[ $file == *.py ]] && [[ $file != utils* ]] && [[ $file != *__* ]]) && COMPREPLY[j++]=$file
            done
        elif [ "${#COMP_WORDS[@]}" == "5" ]; then

            if [[ ${COMP_WORDS[3]} == *.py ]]; then
                COMPREPLY="--help"
            fi
        fi

    elif [[ "${COMP_WORDS[1]}" == "calibration" ]]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            local cmd=$1 cur=$2 pre=$3
            local arr i file
            arr=($(cd "$OSCAR_TOOLS_DIR/${COMP_WORDS[1]}" && compgen -f -- "$cur"))
            COMPREPLY=()
            j=0

            for ((i = 0; i < ${#arr[@]}; ++i)); do
                file=${arr[i]}
                [[ -d $OSCAR_TOOLS_DIR/${COMP_WORDS[1]}/$file ]] &&
                    [[ $file != "third_party" ]] && [[ $file != "common" ]] &&
                    [[ $file != *__* ]] && [[ $file != *.* ]] && COMPREPLY[j++]=$file

            done
        elif [ "${#COMP_WORDS[@]}" == "4" ]; then
            local cmd=$1 cur=$2 pre=$3
            local arr i file
            arr=($(cd "$OSCAR_TOOLS_DIR/${COMP_WORDS[1]}/${COMP_WORDS[2]}" && compgen -f -- "$cur"))
            COMPREPLY=()
            j=0

            [[ $cur == "" ]] && COMPREPLY[j++]="test"
            [[ $cur == "" ]] && COMPREPLY[j++]="help"

            for ((i = 0; i < ${#arr[@]}; ++i)); do
                file=${arr[i]}
                ([[ $file == *.py ]] && [[ $file != utils* ]] && [[ $file != *__* ]]) && COMPREPLY[j++]=$file
            done
        fi

    elif [ "${COMP_WORDS[1]}" == "trajectory" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "generate play plot stop" -- "${COMP_WORDS[2]}"))

        else

            if [ "${COMP_WORDS[2]}" == "generate" ]; then

                if [ "${#COMP_WORDS[@]}" == "4" ]; then
                    COMPREPLY=($(compgen -W "8type 0type" -- "${COMP_WORDS[3]}"))

                else
                    if [ "${COMP_WORDS[3]}" == "8type" ] || [ "${COMP_WORDS[3]}" == "0type" ]; then
                        COMP_COUNT=$((${#COMP_WORDS[@]} - 1))
                        COMPREPLY=($(compgen -W "-r -a -d -v -n --rtk-player -h" -- "${COMP_WORDS[${COMP_COUNT}]}"))
                    fi
                fi

            elif [ "${COMP_WORDS[2]}" == "play" ]; then
                COMP_COUNT=$((${#COMP_WORDS[@]} - 1))
                COMPREPLY=($(compgen -W "-r -n -l -h --loop --rtk-recorder" -- "${COMP_WORDS[${COMP_COUNT}]}"))

            elif [ "${COMP_WORDS[2]}" == "plot" ]; then
                COMP_COUNT=$((${#COMP_WORDS[@]} - 1))
                COMPREPLY=($(compgen -W "-r --rate --car-path --traj-frames --traj-car-footprints -n -h" -- "${COMP_WORDS[${COMP_COUNT}]}"))

            elif [ "${COMP_WORDS[2]}" == "stop" ]; then

                if [ "${#COMP_WORDS[@]}" == "4" ]; then
                    COMPREPLY=($(compgen -W "plotting playing" -- "${COMP_WORDS[3]}"))
                fi
            else
                return
            fi
        fi

    elif [ "${COMP_WORDS[1]}" == "foxglove" ]; then
        if [ "${#COMP_WORDS[@]}" == "3" ]; then

            COMPREPLY=($(compgen -W "convert bridge studio web" -- "${COMP_WORDS[2]}"))
        elif [ "${#COMP_WORDS[@]}" == "4" ]; then
            if [ "${COMP_WORDS[2]}" == "bridge" ] || [ "${COMP_WORDS[2]}" == "studio" ] || [ "${COMP_WORDS[2]}" == "web" ]; then
                COMPREPLY=($(compgen -W "--debug stop" -- "${COMP_WORDS[3]}"))
            fi
        fi

    elif [ "${COMP_WORDS[1]}" == "tracy" ]; then
        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "gui capture" -- "${COMP_WORDS[2]}"))
        fi

    elif [ "${COMP_WORDS[1]}" == "carla" ]; then

        if [ "${#COMP_WORDS[@]}" == "3" ]; then
            COMPREPLY=($(compgen -W "stop bridge" -- "${COMP_WORDS[2]}"))
        fi

    else
        return
    fi
}

_oscarcd_completions() {
    if [ "${#COMP_WORDS[@]}" == "2" ]; then
        COMPREPLY=($(compgen -W "tools bag log car-cfg help" -- "${COMP_WORDS[1]}"))
    fi
    return
}

complete -F _oscar_completions -o default oscar
complete -F _oscarcd_completions oscarcd
