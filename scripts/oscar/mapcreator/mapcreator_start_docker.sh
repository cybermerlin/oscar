#!/bin/bash
OSCAR_ROOT_DIR=/apollo
if [ -d  $OSCAR_ROOT_DIR/mapcreator ]; then
  cd $OSCAR_ROOT_DIR/mapcreator
  source myenv/bin/activate
  export QT_QPA_PLATFORM_PLUGIN_PATH=/apollo/mapcreator/myenv/lib/python3.6/site-packages/PyQt5/Qt5/plugins/platforms/
  bazel run //mapcreator:MapCreator True /apollo/mapcreator
else
  echo "MapCreator is not installed. You should execute 'oscar mapcreator install' first"
fi

