# Скрипты для настройки host системы

В данной директории содержатся скрипты по настройке системы, которые нельзя сделать в docker контейнере.


## create_gnss_device_symlink.sh

По умолчанию, для GNSS приемника создаются файлы /dev/ttyUSBx. Данный скрипт создает для приемника уникальные имена /dev/ttyNovatel{0, 1, 2} и перезагружает сервис udev. Необходимо выполнить только перед первым запуском проекта.

##### Использование

```
sudo ./novatel/create_gnss_device_symlink.sh
```

## add_setup.sh

Скрипт добавляет в ```~/.bashrc``` строку с подгрузкой утилит oscar:``` "source {current_path_to_project}/scripts/oscar/oscar_tools/stup.sh"```.

