#!/usr/bin/env bash


SETUP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../oscar_tools" && pwd -P )"

ADD_LINE="source $SETUP_SCRIPT_DIR/setup.sh"
grep -qxF "$ADD_LINE" ~/.bashrc || echo $ADD_LINE >> ~/.bashrc

source ~/.bashrc