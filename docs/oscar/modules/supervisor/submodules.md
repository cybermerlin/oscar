# Инструкция по созданию подмодулей

Для организации подмодулей в */apollo/modules/supervisor/common/* реализован класс **SupervisorRunner**, который имеет несколько полезных функций:
    - void RunOnce(const double current_time) - главная функция подмодуля, выполняется через каждые interval секунд. interval определяется в конструкторе класса;
    - void GetStatus(int* status, std::string* debug_msg) - сбор последних даннных отработки подмодуля. status - число от 0 до 39 - приоритет передаваемого сообщения, debug_msg - сообщение, которое должен увидеть оператор.

## Пример создания подмодуля

Рассмотрим пример подмодуля **GNSS** в */apollo/modules/supervisor/submodules*:

**sv_gnss_submodule.h**:

```c++
#pragma once
  
// C++ библиотеки
#include <memory>
#include <vector>
#include <cstdlib>
#include <thread>
  
// Файлы Cyber, нужные для работы
#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"
#include "cyber/time/time.h"
#include "cyber/time/duration.h"
#include "cyber/common/log.h"
#include "cyber/component/timer_component.h"
  
// GFlags для работы конфигов
#include "modules/common/adapters/adapter_gflags.h"
  
// Файл с наследуемым классом подмодуля
#include "modules/supervisor/common/supervisor_runner.h"
  
// Прото-файлы GNSS от Apollo и для Supervisor
#include "modules/supervisor/submodules/proto/gnss_conf.pb.h"
#include "modules/drivers/gnss/proto/gnss_best_pose.pb.h"
  
  
namespace apollo {
namespace supervisor {
  
class GNSSSupervisor : public SupervisorRunner {
 public:
  GNSSSupervisor();
  void RunOnce(const double current_time) override;
  void GetStatus(int* status, std::string* debug_msg);
 private:
  int status_;
  std::string debug_msg_;
  std::shared_ptr<apollo::cyber::Node> sv_gnss_node_;
  std::shared_ptr<apollo::cyber::Reader<apollo::drivers::gnss::GnssBestPose>> best_pose_reader_;
  sv_gnss_conf gnss_conf_;
};
  
}
}
```

**sv_gnss_submodule.сс**:

```c++
#include "modules/supervisor/submodules/gnss/sv_gnss_submodule.h"
  
// Интервал проверки модуля GNSS
DEFINE_double(gnss_check_period, 0.3,
            "Interval in seconds.");
// Путь к файлу конфигов Supervisor для GNSS
DEFINE_string(sv_gnss_conf_file, "/apollo/modules/supervisor/submodules/conf/gnss_conf.pb.txt",
            "Path to GNSS conf file");
  
// Дозволительные типы решений
std::vector <apollo::drivers::gnss::SolutionType> sol_types_allowed = {
  apollo::drivers::gnss::SolutionType::FIXEDPOS,
  apollo::drivers::gnss::SolutionType::FIXEDHEIGHT,
  apollo::drivers::gnss::SolutionType::NARROW_INT,
  apollo::drivers::gnss::SolutionType::NARROW_FLOAT,
  apollo::drivers::gnss::SolutionType::PSRDIFF,
};
  
namespace apollo {
namespace supervisor {
  
struct compare {
  int key;
  compare(int const &i): key(i) {}
  bool operator()(int const &i){
    return (i == key);
  }
};
  
// Инициализация класса подмодуля
GNSSSupervisor::GNSSSupervisor()
    : SupervisorRunner("GNSSSupervisor", FLAGS_gnss_check_period) {
  // Инициализация ноды
  sv_gnss_node_ = apollo::cyber::CreateNode("sv_gnss_node");
  // Инициализация чтения канала GNSS без коллбэка
  best_pose_reader_ = sv_gnss_node_->CreateReader<apollo::drivers::gnss::GnssBestPose>("/apollo/sensor/gnss/best_pose", nullptr);
  ACHECK(best_pose_reader_ != nullptr);
  ACHECK(
    cyber::common::GetProtoFromFile(FLAGS_sv_gnss_conf_file, &gnss_conf_))
    << "Unable to load gnss conf file: " + FLAGS_sv_gnss_conf_file;
}
  
// Процедура проверки
void GNSSSupervisor::RunOnce(const double current_time) {
  status_ = 0;
    
  // Получение последнего сообщения, лежащего в канале GNSS
  best_pose_reader_->Observe();
  const auto &best_pose_msg = best_pose_reader_->GetLatestObserved();
  
  if(best_pose_msg == nullptr) {
    AERROR << "GNSS message is not ready";
    status_ = (int)gnss_conf_.not_ready_priority();
    debug_msg_ = "GNSS is not ready. Check that GNSS module is on.";
    return void();
  }
  
  // Check timeout
  if(current_time - best_pose_msg->header().timestamp_sec() > gnss_conf_.max_timeout_s()){
    // Присваивание численной метрики статуса и сообщения при условии, что найденная ошибка является более приоритетной
    if(status_ < (int)gnss_conf_.timeout_priority()){
      status_ = (int)gnss_conf_.timeout_priority();
      debug_msg_ = "GNSS message timeout";
    }
  }
  
  // Check differential age
  if(best_pose_msg->differential_age() > gnss_conf_.max_differential_age()){
    if(status_ < (int)gnss_conf_.differential_age_timeout_priority()){
      status_ = (int)gnss_conf_.differential_age_timeout_priority();
      debug_msg_ = "GNSS differential age timeout";
    }
  }
  
  // Check solution status
  if(best_pose_msg->sol_status() != apollo::drivers::gnss::SolutionStatus::SOL_COMPUTED){
    if(status_ < (int)gnss_conf_.bad_sol_status_priority()){
      status_ = (int)gnss_conf_.bad_sol_status_priority();
      debug_msg_ = "GNSS invalid solution status";
    }
  }
  
  // Check solution type
  if(!std::any_of(sol_types_allowed.begin(), sol_types_allowed.end(), compare(best_pose_msg->sol_type()))){
    if(status_ < (int)gnss_conf_.bad_sol_type_priority()){
      status_ = (int)gnss_conf_.bad_sol_type_priority();
      debug_msg_ = "current GNSS solution type is not in white-list";
    }
  }
  
  // Check standart deviation
  double lat_std_dev = best_pose_msg->latitude_std_dev();
  double lon_std_dev = best_pose_msg->longitude_std_dev();
  double linear_dev = std::sqrt(lat_std_dev * lat_std_dev + lon_std_dev * lon_std_dev);
  if(linear_dev > gnss_conf_.max_deviation_allowed()){
    if(status_ < (int)gnss_conf_.bad_deviation_priority()){
      status_ = (int)gnss_conf_.bad_deviation_priority();
      debug_msg_ = "Standart GNSS deviation is above threshold";
    }
  }
}
  
// Процедура передачи переменных с результатами проверки
void GNSSSupervisor::GetStatus(int* status, std::string* debug_msg) {
  *status = status_;
  *debug_msg = debug_msg_;
}
  
}
}
```

После определения класса нужно настроить вызов его методов в **sv_component**. В инициализации класса **Supervisor** в файле **sv_component.cc** дополняем вектор супервизоров:

```c++
bool Supervisor::Init() {
  supervisors_.emplace_back(new GNSSSupervisor()); //добавляем в список подмодулей созданный класс
  ...
}
```

Теперь нужно прописать зависимости сборки для Bazel:

**/apollo/modules/supervisor/submodules/BUILD:**

```c++
...
cc_library(
    name = "gnss_submodule",
    srcs = ["sv_gnss_submodule.cc"],
    hdrs = ["sv_gnss_submodule.h"],
    deps = [
        "//modules/supervisor/common:supervisor_runner",
        "//modules/drivers/gnss/proto:gnss_proto",
        "//modules/common/adapters:adapter_gflags",
        "//modules/supervisor/submodules/proto:sv_submodule_proto"
    ],
)
...
```

**/apollo/modules/supervisor/BUILD:**

```c++
...
cc_library(
    name = "supervisor_lib",
    srcs = ["sv_component.cc"],
    hdrs = ["sv_component.h"],
    deps = [
        "//cyber",
        "//modules/common/util:message_util",
        "//modules/supervisor/common:supervisor_runner",
        "//modules/supervisor/proto:supervisor_proto",
        "//modules/canbus/proto:canbus_proto",
        "//modules/supervisor/submodules/gnss:gnss_submodule", //Добавляем новый супервизор
    ],
)
...
```

После этого нужно пересобрать **apollo** и запустить **supervisor**. Ожидаемое поведение - компонент **supervisor** опрашивает новый подмодуль и учитывает его сообщения при формировании конечного предупреждения.

## Приоритет сообщения

Приоритет сообщения **status** - двузначное число, численно отображающее важность текста для оператора. Индикация и торможение будут действовать в соответствии с ошибкой максимального приоритета, которая пришла в текущей итерации всех подмодулей. Первая цифра status от 0 до 3 указывает на характер сообщения: 0 - **OK**, 1 - **WARNING**, 2 - **ERROR**, 3 - **FATAL**. Вторая цифра числа указывает на приоритет сообщения в своем подклассе.