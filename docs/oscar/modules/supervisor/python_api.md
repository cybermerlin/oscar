# Описание Supervisor Python API

Файлы, связанные с **Supervisor Python API** находятся в */apollo/modules/supervisor/api/*, импорт производится следующим образом:


```python
import supervisor_api
```

Для дальнейшей работы создается экземпляр класса *SupervisorPreferences()*. **Методы класса**:

| <p align ="center">Метод</p> | <p align ="center">Входные данные</p> | <p align ="center">Назначение</p> | <p align ="center">Возвращаемое значение</p> |
|------|----------|-----------------------|-------|
| DefineSVSoundState(state)  | bool state: True - Включить, False - Выключить | Включить/выключить звуковые оповещения от всех подмодулей | <p align = "center">-</p> |
| DefineSVDebugState(state)  | bool state: True - Включить, False - Выключить | Включить/выключить дебаг-режим для всех подмодулей одновременно | <p align = "center">-</p> |
| GetSVParameters()  | <p align = "center">-</p> | Получить текущие настройки модуля Supervisor | Словарь params: "sound_on": bool sound_on,"debug_mode": bool debug_mode |
| DefineGNSSSoundState(state) | bool state: True - Включить, False - Выключить | Включить/выключить звуковые оповещения от подмодуля GNSS | <p align = "center">-</p> |
| DefineGNSSDebugState(state) | bool state: True - Включить, False - Выключить | Включить/выключить дебаг-режим для подмодуля GNSS | <p align = "center">-</p> |
| GetGNSSParameters() | <p align = "center">-</p> | Получить текущие настройки подмодуля GNSS | Словарь params: "sound_on": bool sound_on,"debug_mode": bool debug_mode |
| DefineIMUSoundState(state) | bool state: True - Включить, False - Выключить | Включить/выключить звуковые оповещения от подмодуля IMU | <p align = "center">-</p> |
| DefineIMUDebugState(state) | bool state: True - Включить, False - Выключить | Включить/выключить дебаг-режим для подмодуля IMU | <p align = "center">-</p> |
| GetIMUParameters() | <p align = "center">-</p>  | Получить текущие настройки подмодуля IMU | Словарь params: "sound_on": bool sound_on,"debug_mode": bool debug_mode |
| SaveCurrentParameters() | <p align = "center">-</p> | Сохранить текущие настройки всех модулей. Все настройки автоматически перезаписываются в конфиг-файл conf/preferences.yaml | <p align = "center">-</p> |

