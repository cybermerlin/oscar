# Конфигурации модуля Control

Конфигурации модуля находятся в нескольких файлах:
1. oscar/modules/control/conf/control_common_conf.pb.txt
2. oscar/modules/control/conf/control_conf.pb.txt
3. oscar/modules/control/conf/lateral_controller_conf.pb.txt
4. oscar/modules/control/conf/longitudinal_controller_conf.pb.txt
5. oscar/modules/control/conf/mpc_controller_conf.pb.txt

Многие конфигурации дублируются. В первом файле содержатся общие настройки. Они повторяются в 3,4,5 файлах (в том числе и значения).

Таблица параметров:

| <p align ="center">Параметр</p> | <p align ="center">Описание</p> | <p align ="center">Значение по умолчанию</p> |
|----------|----------|-----------------------|
| control_test_duration  |  | -1   |
| enable_csv_debug |  | false |
| enable_speed_station_preview | Предпросмотр предсказанного положения | false |
| is_control_test_mode | Запуск в тестовом режиме | false |
| use_preview_speed_for_table | | false |
| enable_input_timestamp_check | | false |
| max_localization_miss_num | Максимальное количество запросов в модуль локализации без ответа (с некорректным ответом). Если число превышено, то модуль отваливается. | 20 |
| max_planning_miss_num | Максимальное количество запросов в модуль планирования без ответа (с некорректным ответом). Если число превышено, то модуль отваливается. | 20 |
| max_acceleration_when_stopped | Максимальное ускорение при остановке | 0.01 |
| max_path_remain_when_stopped | Максимальное расстояние до цели при остановке | 0.3 |
| steer_angle_rate | | 100 |
| enable_gain_scheduler | | true |
| set_steer_limit | | true |
| enable_slope_offset |  | false |
| control_period | Период обновления данных управления | 0.01 |
| trajectory_period | Период обновления траектории | 0.1 |
| chassis_period | Период обновления данных шасси | 0.01 |
| localization_period | Период обновления данных локализации | 0.01 |
| max_status_interval_sec | Максимальное время ответа на запрос о статусе | 0.1 |
| max_planning_interval_sec | Максимальное время планирования | 0.2 |
| max_planning_delay_threshold | | 4.0 |
| soft_estop_brake | | 50.0 |
| max_steering_percentage_allowed | | 100 |
| minimum_speed_resolution | Минимальное разрешение скорости | 0.2 |
| query_relative_time | Промежуток времени между запросами | 0.8 |
| minimum_speed_protection | Если скорость авто равна или больше этой, то контроллер начнает регулировать её | 0.1 |

Во втором файле прописаны конфигурации трёх контроллеров: lateral, longitudinal и mpc. Для каждого из этих контроллеров есть отдельный файл с настройками (3,4,5 соответственно). 3 и 4 файлы полностью идентичны, там прописаны настройки для обоих контроллеров, в 5 настройки отличаются.