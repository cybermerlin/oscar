## **RTK\_planner**

![rtk_planner](images/planning/RTK_planner.jpeg)

Некоторые пояснения: Если в траектории нет точек или их меньше двух, то мы на месте.
Так как алгоритм должен работать для разного количества опорных точек, существует число (в виде флага) необходимого количества точек для запуска и, при их недостатке, алгоритм заполняет траекторию, дублируя последнюю точку, пока траектория не достигнет нужного размера.
Значение _forward\_buffer_ задаётся соответствующим флагом в .config файле.
