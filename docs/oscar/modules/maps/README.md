# Карта

Каждая карта представляет из себя папку со следующей структурой:

```txt
<map_dir>                        # Определяется FLAGS_map_dir
  |-- base_map.xml               # Определяется FLAGS_base_map_filename
  |-- routing_map.bin            # Определяется FLAGS_routing_map_filename
  |-- sim_map.bin                # Определяется FLAGS_sim_map_filename
  |-- default_end_way_point.txt  # Определяется FLAGS_end_way_point_filename
```

Имена файлов карты можно указать списком кандидатов:

```txt
--base_map_filename="base.xml|base.bin|base.txt"
```

Тогда будет использоваться первый доступный для загрузки файл. Обычно используется следующее:

```txt
x.xml  # An OpenDrive formatted map.
x.bin  # A binary pb map.
x.txt  # A text pb map.
```

## Отличия между base\_map, routing\_map и sim\_map
* `base_map` - наиболее полная карта, которая содержит всю информацию о дорогах, полосах, знаках и т.д. Остальные карты генерируются из `base_map`.

* `routing_map` содержит топологию полос движения из `base_map`. Чтобы сгенерировать её необходимо запустить следующие команды в терминале:
 ```
   dir_name=modules/map/data/demo # example map directory
   ./scripts/generate_routing_topo_graph.sh --map_dir ${dir_name}
  ```
  
* `sim_map` - это облегчённая версия `base_map`, необходимая для визуализации в dreamview. За счёт исключения не нужных для пилотирования данных повышается производительность. `sim_map` может быть сгенерирована выполнением следующих команд:
  ```
  dir_name=modules/map/data/demo # example map directory
  bazel-bin/modules/map/tools/sim_map_generator --map_dir=${dir_name} --output_dir=${dir_name}
  ```
  
## Использование другой (новой) карты


1. [Предпочтительно] Изменить файл с глобальными настройками (которые задаются флагами): *modules/common/data/global_flagfile.txt*

*Без выполнения этого пункта новые карты работают!*

![Add_map_path](images/Add_map_path.png)

Делать это нужно внимательно, потому что в этом файле находятся настройки всей системы.

2. Определение карты как флаг, который влияет только на один отдельный процесс, выглядит следующим образом:
	
   ```bash
   <binary> --map_dir=/path/to/your/map
   ```
3. Переопределение в собственном файле флагов модуля, который обычно находится в:
	*modules/<module_name>/conf/<module_name>.conf*

  Это будет влиять тоже только на один модуль.

   ```txt
   --flagfile=modules/common/data/global_flagfile.txt

   # Переопределение в файле с флагами
   --map_dir=/path/to/your/map
   ```
   
  **Можно использовать!**
   
  Процесс создания карты по пройденному маршруту описан [здесь](map_recorder.md).
