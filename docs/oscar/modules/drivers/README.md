# Модуль Drivers

Данный модуль обеспечивает связь периферийных устройств с остальными модулями проекта.

- [GNSS](gnss/README.md)
- [V4L Камеры](camera/README.md)
- [Lucid Камеры](../../../../modules/oscar/drivers/camera)
