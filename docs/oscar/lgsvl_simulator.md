В качестве основного симулятора в рамках проекта OSCAR используется [LGSVL](https://www.lgsvlsimulator.com).

Для работы с стеком Apollo предварительно выполните [инcтрукции по установке и сборке проекта OSCAR](README.md) если не сделали этого ранее.

### Установка и работа с симулятором

1. Скачайте и распакуйте последний релиз (не *-rc* версию) lgsvl с [оффициального репозитория на github](https://github.com/lgsvl/simulator/releases).

2. Войдите в контейнер apollo, запустите bootstrap и мост

    ```
    ./scripts/bootstrap.sh
    ./scripts/bridge.sh
    ```

3. Для запуска симулятора перейдите в соответствующую директорию и выполните

    ```
    ./simulator
    ```
   ![lgsvl_config](images/lgsvl/SVLSimulator_main_window.png)

    В появившемся окне необходимо удостовериться, что выбран режим "Online" (в выпадающем меню в правом верхнем углу необходимо выбрать "Go online").
    Появится кнопка "Link to cloud", ее и нужно нажать. В браузере откроется окно входа. Необходимо зарегистрироваться и войти.

   ![lgsvl_config](images/lgsvl/SVLSimulator_registration_page.png)

   После прохождения регистрации и входа должно открыться следующее окно с предложением создать "Cluster" из вашей рабочей машины
   (если этого не произошло, стоит еще раз нажать кнопку "Link to cloud" в окне симулятора).
   В поле "New Cluster" вводим имя кластера и создаем его.

   ![lgsvl_config](images/lgsvl/SVLSimulator_cluster_page.png)

   ![lgsvl_config](images/lgsvl/SVLSimulator_cluster_page2.png)

   ![lgsvl_config](images/lgsvl/SVLSimulator_cluster_page3.png)



4. Настройте и запустите симуляцию

    Перед созданием симуляции необходимо выбрать карту и автомобиль с поддержкой Apollo 6.0. Для этого перейдите во вкладку **Library-Maps**, и в представленном каталоге определите на какой карте будет проводиться симуляция.

    ![lgsvl_config](images/lgsvl/SVLSimulator_choice_of_map.png)

    Во вкладке **Library-Vehicles** определите какой автомобиль будет использоваться в симуляции.

    ![lgsvl_config](images/lgsvl/SVLSimulator_choise_of_car.png)

    Перейдите во вкладку **Simulations** и создайте симуляцию нажатием на **Add new**.

    ![lgsvl_config](images/lgsvl/SVLSimulator_simulation_page.png)

    В появившемся окне назовите симуляцию любым именем, добавьте описание и ключевые слова при необходимости. Выберите кластер для симуляций (например тот, что был создан немного ранее). Также, для активации доступны три функции: **Create test report**, **Headless mode**, **Interactive mode**.
    Функция **Create test report** отвечает за создание отчёта о симуляции, который, при активации функции, находится в **Test results**.
    Функция **Headless mode** убирает главный вид в симуляции для повышения производительности.
    Функция **Interactive mode** разрешает управление потоком времени, снимки симуляции, взаимодействие с окружающим миром, ручное управление автомобилем. Должна быть **обязательно** активирована.

    ![lgsvl_config](images/lgsvl/SVLSimulator_simulation_page2.png)

    После заполнения полей и активации функций переходим на следующую страницу нажав кнопку **Next**.

    Во вкладке **Test case** необходимо выбрать шаблон симуляции, доступны следующие: **Random traffic**, **Visual scenario editor**, **Python API**, **API Only**.

    ![lgsvl_config](images/lgsvl/SVLSimulator_test_case_page1.png)

    В **Random traffic** пешеходы, автомобили, велосипедисты генерируются случайным образом, время суток и погода задаются вручную. За генерацию автомобилей отвечает функция *Random Traffic*, пешеходов - *Random Pedestrians*, велосипедистов - *Random Bicyclists*. Функция *Use Pre-defined Seed* повышает "связанность" симуляции. Если она отключена, то симуляция генерирует случайное количество объектов случайным образом в случайное время (работает *Random seed*). Активация этой функции позволяет задать конкретную частоту, исходя из которой будет происходить генерация объектов. Дата и погода настраиваются 6-ю параметрами: *Simulation Date* - дата, *Time Of Day* - время суток, *Rain* - дождь (влияет на работу датчиков), *Fog* - туман, *Wetness* - влажность дороги (влияет на скольжение), *Cloudiness* - облачность (влияет на освещённость и, соответственно, на работу датчиков).

    ![lgsvl_config](images/lgsvl/SVLSimulator_test_case_page2.png)

    ![lgsvl_config](images/lgsvl/SVLSimulator_test_case_page3.png)

    **Visual scenario editor** позволяет загрузить сценарий из .json файла.
    **Python API** и **API Only** позволяют управлять объектами в окружающей среде обновляя их состояние через *API*.
    В данном гиде мы остановимся на **Random traffic**.  После выбора и настройки шаблона жмём **Next**.
    В **Autopilot** выбираем *Apollo 6.0* и в поле **Bridge connection** вводим *127.0.0.1:9090*.

    ![lgsvl_config](images/lgsvl/SVLSimulator_autopilot_page.png)

    Переходим на последнюю страничку нажатием **Next**.
    Симуляция готова, жмём **Publish** и видим окно только что созданной нами симуляции.

    ![lgsvl_config](images/lgsvl/SVLSimulator_publish_page.png)

    ![lgsvl_config](images/lgsvl/SVLSimulator_done.png)

	Для запуска используем кнопку **Run Simulation**.

    В основном окне симулятора появится созданная симуляция, для ее запуска требуется нажать кнопку *Play* в нижнем левом углу.

    ![lgsvl_sim](images/lgsvl/SVLSimulator_simulation_window2.png)


5. Запустите стек Apollo для управления автомобилем в симуляции

    Откройте Dreamview введя в браузере localhost:8888 и выберите используемую в симуляции карту и автомобиль, а также установите мод **Simulation** в верхнем правом углу и перейдите в панель **Module Controller**.

    ![lgsvl_sim](images/lgsvl/SVLSimulator_dreamview1.png)

    Запустите модули *Planning*, *Localization*, *Routing*, *Control* и *Prediction*.

    ![lgsvl_sim](images/lgsvl/SVLSimulator_dreamview2.png)

    Перейдите в панель **Route Editing** и назначте цель маршрута щелкнув левой кнопкой мыши по достижимой для автомобиля точке на карте (достижимые траектории идут вдоль зеленых линий, во вкладке Route Editing). Для отправки команды на перемещение в назначенную точку нажмите **Send Routing Request**.

    ![lgsvl_sim](images/lgsvl/SVLSimulator_dreamview3.png)

     Если автомобиль в симуляции начал движение, то подготовку к работе с симулятором можно считать оконченной.

    ![lgsvl_sim](images/lgsvl/SVLSimulator_dreamview4.png)


6. Добавление новых карт и автомобилей

    В симулятор можно добавлять новые карты, для этого нужно в соответствующей вкладке **Store** / **Maps** нажать кнопку **"+"**

   ![lgsvl_apollo](images/lgsvl/SVLSimulator_map_window.png)

    После добавления, карта попадает во вкладку **Library** / **Maps**. Далее карту можно использовать при настройке симуляции (пункт 4).

    Для добавления карты в dreamview/appolo необходимо перейти на страницу конкретной карты и скачать .bin файл.

    ![lgsvl_apollo](images/lgsvl/SVLSimulator_san_fracisco.png)

    В папке `/modules/map/data/` необходимо создать папку с именем карты и добавить в нее полученный .bin файл.

    Далее необходимо создать `sim_map` и `routing_map`, это можно сделать
    воспользовавшись [инструкциями на русском](/docs/oscar/modules/maps/README.md) и
    [анлийском](/modules/map/data/README.md) языках.

    Теперь карту можно использовать в dreamview.

	[Инструкция, как создать HD карту по пройденному маршруту](/docs/oscar/modules/maps/map_recorder.md)


### Решение проблем

1. При первом запуске симуляции на шаге **4** и прошлого раздела симулятор должен
скачать дополнительные компоненты с серверов svl. Вероятно ввиду прекращения
поддержки проекта они могу не скачиваться и начало симуляции зависает:

    ![](images/lgsvl/lgsvl_dont_load_modules_err.png)

    В таком случае, для запуска симуляции [можно воспользоваться сборкой с
    кэшированными модулями](https://drive.google.com/file/d/124ZimkMcFvgfbvZZ7lcNumsqeDbbVpmY/view?usp=sharing).
